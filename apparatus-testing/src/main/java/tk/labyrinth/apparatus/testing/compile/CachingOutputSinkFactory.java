package tk.labyrinth.apparatus.testing.compile;

import lombok.Getter;
import org.benf.cfr.reader.api.OutputSinkFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * https://www.benf.org/other/cfr/api/index.html > Override where the output goes
 */
public class CachingOutputSinkFactory implements OutputSinkFactory {

	@Getter
	private final List<String> cache = new ArrayList<>();

	@Override
	public <T> Sink<T> getSink(SinkType sinkType, SinkClass sinkClass) {
		return sinkType == SinkType.JAVA
				? sinkable -> cache.add(sinkable.toString())
				: sinkable -> {
			// TODO: Replace with some constant of Consumer#doNothing().
			// no-op
		};
	}

	@Override
	public List<SinkClass> getSupportedSinks(SinkType sinkType, Collection<SinkClass> available) {
		return List.of(SinkClass.STRING);
	}
}
