package tk.labyrinth.apparatus.testing.compile;

import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.benf.cfr.reader.api.CfrDriver;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.misc4j2.java.io.IoUtils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public abstract class CompilerAwareTestBase {

	@Getter
	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	public List<String> decompile(List<String> sourceFileNames) {
		CachingOutputSinkFactory cachingOutputSinkFactory = new CachingOutputSinkFactory();
		CfrDriver cfrDriver = new CfrDriver.Builder()
				.withOutputSink(cachingOutputSinkFactory)
				.build();
		cfrDriver.analyse(sourceFileNames.stream()
				.map(sourceFileName -> testCompiler.getOutputDirectory().resolve(sourceFileName.replace(".java", ".class")))
				.map(Path::toString)
				.collect(Collectors.toList()));
		return cachingOutputSinkFactory.getCache();
	}

	public List<String> readExpectedDecompilerOutput(List<String> sourceFileNames) {
		return sourceFileNames.stream()
				.map(CompilerAwareTestBase::readSingleExpectedDecompilerOutput)
				.collect(Collectors.toList());
	}

	public static String readSingleExpectedDecompilerOutput(String sourceFileName) {
		String className;
		String path;
		{
			int lastIndexOfSlash = sourceFileName.lastIndexOf('/');
			className = sourceFileName.substring(lastIndexOfSlash + 1).replace(".java", "");
			path = lastIndexOfSlash != -1
					? sourceFileName.substring(0, lastIndexOfSlash) + "/"
					: "";
		}
		return IoUtils.unchecked(() -> {
			String result;
			{
				InputStream resourceAsStream = ClassLoader.getSystemClassLoader().getResourceAsStream(
						path + "decompiled/" + className + ".txt");
				if (resourceAsStream != null) {
					result = String.join("\n", IOUtils.readLines(
							resourceAsStream,
							StandardCharsets.UTF_8));
				} else {
					result = null;
				}
			}
			return result;
		});
	}
}
