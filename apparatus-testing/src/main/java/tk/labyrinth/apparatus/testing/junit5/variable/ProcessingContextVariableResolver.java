package tk.labyrinth.apparatus.testing.junit5.variable;

import com.google.auto.service.AutoService;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.parameter.JaapVariableResolver;

@AutoService(JaapVariableResolver.class)
public class ProcessingContextVariableResolver implements JaapVariableResolver<ProcessingContext> {

	@Override
	public Class<ProcessingContext> getParameterType() {
		return ProcessingContext.class;
	}

	@Override
	public boolean isInternal() {
		return true;
	}

	@Override
	public ProcessingContext resolveVariable(AnnotationProcessingRound round) {
		return ProcessingContext.of(round.getProcessingEnvironment());
	}
}
