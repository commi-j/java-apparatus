@file:Suppress("UNCHECKED_CAST")

dependencies {
	//
	// Provides @AutoService.
	compileOnly(libraries["com.google.auto.service:auto-service"])
	annotationProcessor(libraries["com.google.auto.service:auto-service"])
	//
	// Provides decompiler.
	api(libraries["org.benf:cfr"])
	//
	// Provides test compiler.
	api(libraries["tk.labyrinth:jaap-testing"])
	//
	api(project(":apparatus-core"))
}
//
(extra["addJavacExportsToCompiler"] as () -> Unit).invoke()
(extra["configurePublishingToBintray"] as () -> Unit).invoke()
