package tk.labyrinth.apparatus.misc4j.java.lang.reflect;

import org.apache.commons.lang3.reflect.TypeUtils;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ParameterUtils {

	public static Type getActualParameter(Type actualType, Class<?> declaringType, int index) {
		return getActualParameters(actualType, declaringType).skip(index).findFirst().orElseThrow();
	}

	public static Stream<Type> getActualParameters(Type actualType, Class<?> declaringType) {
		Map<TypeVariable<?>, Type> typeArguments = TypeUtils.getTypeArguments(actualType, declaringType);
		return Stream.of(declaringType.getTypeParameters()).map(typeArguments::get);
	}

	public static Type getFirstActualParameter(Type actualType, Class<?> declaringType) {
		return getActualParameter(actualType, declaringType, 0);
	}

	public static Type getSecondActualParameter(Type actualType, Class<?> declaringType) {
		return getActualParameter(actualType, declaringType, 1);
	}
}
