package tk.labyrinth.apparatus.misc4j.java.lang.reflect;

import tk.labyrinth.apparatus.misc4j.java.lang.exception.ExceptionUtils;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;

import javax.annotation.Nullable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.2
 */
public class TypeUtils {

	/**
	 * @param value non-null
	 * @param <T>   Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static <T> T cast(Object value) {
		return castNullable(Objects.requireNonNull(value, "value"));
	}

	/**
	 * @param value nullable
	 * @param <T>   Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.1
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <T> T castNullable(@Nullable Object value) {
		return (T) value;
	}

	/**
	 * @param type non-null
	 *
	 * @return nullable
	 *
	 * @see #getClass(Type)
	 * @since 1.0.2
	 */
	@Nullable
	public static Class<?> findClass(Type type) {
		Class<?> result;
		if (type instanceof Class) {
			result = (Class<?>) type;
		} else if (type instanceof ParameterizedType) {
			result = (Class<?>) ((ParameterizedType) type).getRawType();
		} else if (type instanceof TypeVariable<?>) {
			result = null;
		} else {
			throw new NotImplementedException(ExceptionUtils.render(type));
		}
		return result;
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 *
	 * @see #findClass(Type)
	 * @since 1.0.2
	 */
	public static Class<?> getClass(Type type) {
		Class<?> result = findClass(type);
		if (result == null) {
			throw new IllegalArgumentException("No class found for type: " + type);
		}
		return result;
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String getSimpleName(Type type) {
		String result;
		if (type instanceof Class) {
			result = ((Class<?>) type).getSimpleName();
		} else {
			result = type.toString();
		}
		return result;
	}
}
