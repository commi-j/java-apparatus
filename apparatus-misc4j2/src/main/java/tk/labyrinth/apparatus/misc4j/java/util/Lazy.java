package tk.labyrinth.apparatus.misc4j.java.util;

import lombok.RequiredArgsConstructor;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public abstract class Lazy<T> {

	private boolean computed = false;

	@Nullable
	private T value = null;

	/**
	 * Expected to be invoked zero or one time.
	 *
	 * @return nullable
	 */
	@Nullable
	protected abstract T compute();

	@Nullable
	public T get() {
		if (!computed) {
			value = compute();
			computed = true;
		}
		return value;
	}

	public T getOrFail() {
		return Objects.requireNonNull(get(), "value");
	}

	// TODO: Implicize this method by replacing it with default lambda resolver (Apparatus feature).
	public static <T> Lazy<T> of(Supplier<T> valueSupplier) {
		return new SimpleLazy<>(valueSupplier);
	}

	@RequiredArgsConstructor
	public static class SimpleLazy<T> extends Lazy<T> {

		private final Supplier<T> valueSupplier;

		@Nullable
		@Override
		protected T compute() {
			return valueSupplier.get();
		}
	}
}
