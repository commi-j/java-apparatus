package tk.labyrinth.apparatus.misc4j.lib.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ObjectMapperUtils {

	/**
	 * Unchecked variant of {@link ObjectMapper#readValue(String, Class)}.
	 *
	 * @param objectMapper non-null
	 * @param content      non-null
	 * @param valueType    non-null
	 * @param <T>          Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> T readValue(ObjectMapper objectMapper, String content, Class<T> valueType) {
		try {
			return objectMapper.readValue(content, valueType);
		} catch (JsonProcessingException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Unchecked variant of {@link ObjectMapper#writeValueAsString(Object)}.
	 *
	 * @param objectMapper non-null
	 * @param value        non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String writeValueAsString(ObjectMapper objectMapper, Object value) {
		try {
			return objectMapper.writeValueAsString(value);
		} catch (JsonProcessingException ex) {
			throw new RuntimeException(ex);
		}
	}
}
