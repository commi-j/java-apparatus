package tk.labyrinth.apparatus.misc4j.java.lang.exception.wrap;

import javax.annotation.Nullable;

/**
 * Indicates a wrapper for exception that adds no extra value to it.
 *
 * @author Commitman
 * @version 1.0.0
 * @see Throwable#getCause()
 */
public interface ExceptionWrapper {

	/**
	 * Returns {@link Throwable} wrapped by this object. For classes that extend {@link Throwable} this method
	 * is already implemented as {@link Throwable#getCause()};
	 *
	 * @return nullable
	 */
	@Nullable
	Throwable getCause();

	/**
	 * Returns first {@link #getCause() cause} that is not an instance of {@link ExceptionWrapper}.
	 *
	 * @return nullable
	 */
	@Nullable
	default Throwable unwrap() {
		Throwable result;
		{
			Throwable cause = getCause();
			if (cause instanceof ExceptionWrapper) {
				result = ((ExceptionWrapper) cause).unwrap();
			} else {
				result = cause;
			}
		}
		return result;
	}
}
