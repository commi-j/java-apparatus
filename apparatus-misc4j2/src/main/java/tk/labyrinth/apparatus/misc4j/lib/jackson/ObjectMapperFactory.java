package tk.labyrinth.apparatus.misc4j.lib.jackson;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * @author Commitman
 * @version 1.0.2
 */
public class ObjectMapperFactory {

	public static ObjectMapper defaultConfigured() {
		ObjectMapper result = new ObjectMapper();
		{
			// Handle fields and creators.
			// Ignore methods.
			result.setDefaultVisibility(JsonAutoDetect.Value.construct(
					JsonAutoDetect.Visibility.ANY,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.NONE,
					JsonAutoDetect.Visibility.ANY
			));
		}
		{
			// Avoiding jsons being overpopulated with null values.
			result.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		}
		{
			// https://github.com/FasterXML/jackson-modules-java8/tree/master/datetime
			// Must be manually enabled in 2.x <= x < 3.x versions.
			// You can try to remove it if you use 3.x version.
			result.registerModule(new JavaTimeModule());
		}
		return result;
	}
}
