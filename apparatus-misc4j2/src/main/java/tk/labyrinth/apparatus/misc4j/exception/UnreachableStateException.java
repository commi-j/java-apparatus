package tk.labyrinth.apparatus.misc4j.exception;

/**
 * Throw it in cases where you are sure this conditional branch is unreachable but can not guarantee it by syntax means.<br>
 * Whenever this exception is thrown it is an indication of erroneously designed behaviour
 * which should be properly rewritten so you should supply this exception with sufficient information.
 *
 * @author Commitman
 * @version 1.0.0
 */
public class UnreachableStateException extends IllegalStateException {

	public UnreachableStateException() {
		super();
	}

	public UnreachableStateException(String message) {
		super(message);
	}
}
