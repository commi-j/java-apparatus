package tk.labyrinth.apparatus.misc4j.java.lang.reflect;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public interface TypeAware<T> {

	default T createNewInstance() {
		return ReflectionUtils.createNewInstance(getParameterType());
	}

	@SuppressWarnings("unchecked")
	default Class<T> getParameterType() {
		return getParameterType((Class<? extends TypeAware<T>>) getClass());
	}

	@SuppressWarnings("unchecked")
	static <T> Class<T> getParameterType(Class<? extends TypeAware<T>> type) {
		return (Class<T>) getRawParameterType(type);
	}

	@SuppressWarnings("rawtypes")
	// Rawtyping is required for 'type' parameter to avoid unnecessary generic casts when invoked.
	static Class<?> getRawParameterType(Class<? extends TypeAware> type) {
		return TypeUtils.getClass(ParameterUtils.getFirstActualParameter(type, TypeAware.class));
	}
}
