package tk.labyrinth.apparatus.misc4j.lang;

public interface Accessor<T> {

	T get();

	T set(T value);
}
