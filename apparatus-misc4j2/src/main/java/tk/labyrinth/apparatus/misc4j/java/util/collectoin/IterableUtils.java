package tk.labyrinth.apparatus.misc4j.java.util.collectoin;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class IterableUtils {

	public static <E> void forEachWithIndex(@Nonnull Iterable<? extends E> iterable, @Nonnull BiConsumer<Integer, ? super E> consumer) {
		IteratorUtils.forEachWithIndex(iterable.iterator(), consumer);
	}
}
