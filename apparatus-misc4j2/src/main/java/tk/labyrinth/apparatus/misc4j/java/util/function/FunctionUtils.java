package tk.labyrinth.apparatus.misc4j.java.util.function;

import tk.labyrinth.apparatus.misc4j.exception.UnreachableStateException;

public class FunctionUtils {

	public static <T, R> R throwUnreachableStateException(T first, R second) {
		throw new UnreachableStateException();
	}
}
