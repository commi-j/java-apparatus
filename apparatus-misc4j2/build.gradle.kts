@file:Suppress("UNCHECKED_CAST")

dependencies {
	val versionCommonsLang = "3.9"
	//
	api("com.fasterxml.jackson.core", "jackson-databind", "2.11.2")
	api("com.fasterxml.jackson.datatype", "jackson-datatype-jsr310", "2.11.2")
	//
	implementation("org.apache.commons", "commons-lang3", versionCommonsLang)
}
//
(extra["configurePublishingToBintray"] as () -> Unit).invoke()
