package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import tk.labyrinth.jaap.model.declaration.ConstructorModifier;
import tk.labyrinth.jaap.model.declaration.JavaConstructorModifier;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodModifier;

class CoreObjectMapperFactoryTest {

	private final ObjectMapper objectMapper = CoreObjectMapperFactory.create();

	@EnumSource(JavaConstructorModifier.class)
	@ParameterizedTest
	void testSerializeDeserializeJavaConstructorModifiers(JavaConstructorModifier modifier) throws JsonProcessingException {
		String serializedValue = objectMapper.writeValueAsString(modifier);
		Assertions.assertEquals(serializedValue.toLowerCase(), serializedValue);
		Assertions.assertEquals(modifier, objectMapper.readValue(serializedValue, ConstructorModifier.class));
	}

	@EnumSource(JavaMethodModifier.class)
	@ParameterizedTest
	void testSerializeDeserializeJavaMethodModifiers(JavaMethodModifier modifier) throws JsonProcessingException {
		String serializedValue = objectMapper.writeValueAsString(modifier);
		Assertions.assertEquals(serializedValue.toLowerCase(), serializedValue);
		Assertions.assertEquals(modifier, objectMapper.readValue(serializedValue, MethodModifier.class));
	}
}
