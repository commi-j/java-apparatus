package tk.labyrinth.apparatus.core;

import java.math.BigInteger;

@SuppressWarnings({"ArrayHashCode", "ResultOfMethodCallIgnored", "unused"})
class MethodInvocationManipulatingVisitorUtilsTestCompilationWork {

	public static void main(String... args) {
		BigInteger variableDeclaredInMethodBody = BigInteger.ONE;
		{
			// Statement cases
			{
				// Literal
				"a".hashCode();
			}
			{
				// Parentheses with literals
				("a" + "b").hashCode();
			}
			{
				// .class literal
				BigInteger.class.hashCode();
			}
		}
		{
			// Variable cases
			{
				// Variable declared in same block (and hiding class)
				BigInteger String = BigInteger.ONE;
				String.hashCode();
			}
			{
				// Variable declared in outer block
				BigInteger bi = BigInteger.ONE;
				{
					bi.hashCode();
				}
			}
			{
				// Variable declared in method body (same behaviour as for outer block)
				variableDeclaredInMethodBody.hashCode();
			}
			{
				// Variable declared in method parameters
				args.hashCode();
			}
		}
		{
			// Lambda cases
			// TODO
//			{
//				// Variable inside lambda (same behaviour as for same block)
//				Runnable r = () -> {
//					Object tmp = new Object();
//					tmp.hashCode();
//				};
//			}
//			{
//				// Variable declared in lambda parameters
//				Consumer<BigInteger> f = input -> input.hashCode();
//			}
//			{
//				// MethodReference
//				Consumer<BigInteger> f = BigInteger::hashCode;
//			}
		}
	}
}
