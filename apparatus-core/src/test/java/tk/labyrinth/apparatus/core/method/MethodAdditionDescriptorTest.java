package tk.labyrinth.apparatus.core.method;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.core.json.CoreObjectMapperFactory;
import tk.labyrinth.jaap.model.ElementSignature;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import java.io.IOException;
import java.net.URL;
import java.util.List;

class MethodAdditionDescriptorTest {

	private final URL jsonUrl = ClassLoader.getSystemResource("json/methodAdditionDescriptors.json");

	@Test
	void testMapping() throws IOException {
		ObjectMapper objectMapper = CoreObjectMapperFactory.create();
		//
		ArrayNode jsonArray = objectMapper.readValue(jsonUrl, ArrayNode.class);
		//
		{
			MethodAdditionDescriptor javaValue = new MethodAdditionDescriptor(
					null,
					MethodDeclaration.builder()
							.modifiers(List.of(JavaMethodModifier.PUBLIC))
							.name("toLowerString")
							.returnType(TypeDescription.ofNonParameterized(String.class))
							.build(),
					CanonicalTypeSignature.of(Object.class),
					ElementSignature.ofValid("java.lang.String#toLowerCase()"));
			String jsonValue = jsonArray.get(0).toString();
			//
			Assertions.assertEquals(jsonValue, objectMapper.writeValueAsString(javaValue));
			Assertions.assertEquals(javaValue, objectMapper.readValue(jsonValue, MethodAdditionDescriptor.class));
		}
	}
}
