package tk.labyrinth.apparatus.sourcetree.scope;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.test.model.methodinvocation.MethodInvocationsInVariousScopes;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class TreeContextTest {

	@CompilationTarget(sourceTypes = MethodInvocationsInVariousScopes.class)
	@Test
	void testIsStatic(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				true,
				false,
				false,
				false,
				true
		), ListUtils.mapNonnull(nodes, node -> node.getNodeContext().getTreeContext().hasThis()));
	}
}
