package tk.labyrinth.apparatus.sourcetree.scope;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.test.model.NestedTypeNames;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
class ElementNodeUtilsTest {

	@CompilationTarget(sourceTypes = NestedTypeNames.class)
	@Test
	void testFindClass(ProcessingContext context) {
		{
			ClassNode classNode = ElementNodeUtils.findClass(context.getTypeElementTemplate(NestedTypeNames.class));
			Assertions.assertNotNull(classNode);
			Assertions.assertEquals("NestedTypeNames", classNode.toString());
		}
		{
			ClassNode classNode = ElementNodeUtils.findClass(context.getTypeElementTemplate(NestedTypeNames.Nested.class));
			Assertions.assertNotNull(classNode);
			Assertions.assertEquals("Nested", classNode.toString());
		}
		{
			ClassNode classNode = ElementNodeUtils.findClass(context.getTypeElementTemplate(NestedTypeNames.Nested.Nestedmost.class));
			Assertions.assertNotNull(classNode);
			Assertions.assertEquals("Nestedmost", classNode.toString());
		}
		{
			Assertions.assertNull(ElementNodeUtils.findClass(context.getTypeElementTemplate(Object.class)));
		}
	}
}
