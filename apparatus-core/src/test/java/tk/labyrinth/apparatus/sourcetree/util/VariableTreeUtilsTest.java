package tk.labyrinth.apparatus.sourcetree.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.apparatus.test.model.variable.Variables;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.template.element.util.VariableElementUtils;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import javax.annotation.processing.ProcessingEnvironment;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class VariableTreeUtilsTest {

	@CompilationTarget(sourceTypes = Variables.class)
	@Test
	void testFindElement(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
		List<VariableNode> nodes = TestNodeUtils.collectVariables(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(Arrays.asList(
				VariableElementUtils.resolveField(processingEnvironment, Variables.class, "field"),
				VariableElementUtils.resolveParameter(processingEnvironment, Variables.class, "method(java.lang.Number)", 0),
				null,
				null
		), ListUtils.mapNonnull(nodes, node -> VariableTreeUtils.findElement(node.getVariableTree())));
	}

	@CompilationTarget(sourceTypes = Variables.class)
	@Test
	void testFindTypeMirror(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
		List<VariableNode> nodes = TestNodeUtils.collectVariables(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				TypeMirrorUtils.resolve(processingEnvironment, BigDecimal.class),
				TypeMirrorUtils.resolve(processingEnvironment, Number.class),
				TypeMirrorUtils.resolve(processingEnvironment, BigInteger.class),
				TypeMirrorUtils.resolve(processingEnvironment, Serializable.class)
		), ListUtils.mapNonnull(nodes, node -> VariableTreeUtils.findTypeMirror(
				node.getNodeContext().getTreeContext(), node.getVariableTree())));
	}
}
