package tk.labyrinth.apparatus.sourcetree.registry.util;

import java.math.BigDecimal;
import java.util.*;

//import java.util.*;
class SelfClass {

	BigDecimal bigDecimal;

	NestedClass nestedClass;

	SiblingClass siblingClass;

	/**
	 * This is tk.labyrinth.apparatus.sourcetree.registry.util.System and not java.lang.System.
	 */
	System system;

	public static class NestedClass {
		// empty
	}
}

class SiblingClass {

	SelfClass.NestedClass nestedClass;
}
