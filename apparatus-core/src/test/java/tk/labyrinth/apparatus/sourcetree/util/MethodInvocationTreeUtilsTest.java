package tk.labyrinth.apparatus.sourcetree.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.sourcetree.registry.TreeRegistry;
import tk.labyrinth.apparatus.test.model.methodinvocation.MethodInvocationWithThisTarget;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.ChainedMethodInvocations;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.StaticMethodInvocationWithExternalField;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.StaticMethodInvocations;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import javax.annotation.processing.ProcessingEnvironment;
import java.io.Console;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class MethodInvocationTreeUtilsTest {

	@CompilationTarget(sourceTypes = StaticMethodInvocationWithExternalField.class)
	@Test
	void testBuildMethodSelectionContext(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				EntitySelectionContext.forMethod(TypeMirrorUtils.resolve(processingEnvironment, BigDecimal.class))
		), ListUtils.mapNonnull(nodes, node -> MethodInvocationTreeUtils.buildMethodSelectionContext(
				node.getNodeContext().getTreeContext(), node.getMethodInvocationTree())));
	}

	@CompilationTarget(sourceTypes = StaticMethodInvocationWithExternalField.class)
	@Test
	void testFindArgumentTypes(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				List.of(TypeMirrorUtils.resolve(processingEnvironment, BigDecimal.class))
		), ListUtils.mapNonnull(nodes, node -> MethodInvocationTreeUtils.findArgumentTypes(
				node.getNodeContext().getTreeContext(), node.getMethodInvocationTree())));
	}

	@CompilationTarget(sourceTypes = ChainedMethodInvocations.class)
	@Test
	void testFindMethod(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				ExecutableElementUtils.resolve(processingEnvironment, Object.class, "toString()"),
				ExecutableElementUtils.resolve(processingEnvironment, Console.class, "reader()"),
				ExecutableElementUtils.resolve(processingEnvironment, System.class, "console()")
		), ListUtils.mapNonnull(nodes, node -> MethodInvocationTreeUtils.findMethod(
				node.getNodeContext().getTreeContext(), node.getMethodInvocationTree())));
	}

	@CompilationTarget(sourceTypes = StaticMethodInvocationWithExternalField.class)
	@Test
	void testFindReturnType(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				TypeMirrorUtils.resolve(processingEnvironment, String.class)
		), ListUtils.mapNonnull(nodes, node -> MethodInvocationTreeUtils.findReturnType(
				node.getNodeContext().getTreeContext(), node.getMethodInvocationTree())));
	}

	@CompilationTarget(sourceTypes = StaticMethodInvocations.class)
	@Test
	void testGetSimpleName(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(ListUtils.flatten(
				ListUtils.repeat("testMethod", 3)
		), ListUtils.mapNonnull(nodes, node -> MethodInvocationTreeUtils.getSimpleName(
				node.getMethodInvocationTree())));
	}

	@CompilationTarget(sourceTypes = MethodInvocationWithThisTarget.class)
	@Test
	void testGetTargetWithThisTarget(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				"tk.labyrinth.apparatus.test.model.methodinvocation.MethodInvocationWithThisTarget.this",
				"tk.labyrinth.apparatus.test.model.methodinvocation.MethodInvocationWithThisTarget.this"
		), ListUtils.mapNonnull(nodes, node -> MethodInvocationTreeUtils.getTargetEntity(
				node.getNodeContext().getTreeContext(), node.getMethodInvocationTree()).toString()));
	}

	@CompilationTarget(sourceTypes = StaticMethodInvocations.class)
	@Test
	void testResolveTargetType(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(ListUtils.flatten(
				ListUtils.repeat(TypeMirrorUtils.resolve(round.getProcessingEnvironment(), StaticMethodInvocations.class), 3)
		), ListUtils.mapNonnull(nodes, node -> MethodInvocationTreeUtils.resolveTargetType(
				(TreeRegistry) node.getNodeContext().getTreeContext(), node.getMethodInvocationTree())));
	}
}
