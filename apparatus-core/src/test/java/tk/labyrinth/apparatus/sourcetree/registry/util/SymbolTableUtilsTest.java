package tk.labyrinth.apparatus.sourcetree.registry.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.sourcetree.util.CompilationUnitTreeUtils;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.lang.model.util.Elements;

@ExtendWithJaap
class SymbolTableUtilsTest {

	@CompilationTarget(sourceTypes = SelfClass.class)
	@Test
	void testLookup(AnnotationProcessingRound round) {
		Elements elements = round.getProcessingEnvironment().getElementUtils();
		CompilationUnitTreeUtils.streamFrom(round.getProcessingEnvironment(), round.getRoundEnvironment()).forEach(compilationUnitTree -> {
//						Assertions.assertEquals("java.math.BigDecimal",
//								SymbolTableUtils.lookup(compilationUnitTree,
//										elements.getName("SelfClass.NestedClass")).toString());
			//
			// package-level class
//						Assertions.assertEquals("java.math.BigDecimal",
//								SymbolTableUtils.lookup(compilationUnitTree,
//										elements.getName("PackageLevelClass")).toString());
			//
			// Self
//						Assertions.assertEquals("tk.labyrinth.apparatus.sourcetree.registry.util.SelfClass",
//								SymbolTableUtils.lookup(compilationUnitTree,
//										elements.getName("SelfClass")).toString());
			//
			// Sibling
//						Assertions.assertEquals("tk.labyrinth.apparatus.sourcetree.registry.util.SiblingClass",
//								SymbolTableUtils.lookup(compilationUnitTree,
//										elements.getName("SiblingClass")).toString());
			//
			// Nested
//						Assertions.assertEquals("java.math.BigDecimal",
//								SymbolTableUtils.lookup(compilationUnitTree,
//										elements.getName("NestedClass")).toString());
			//
			// 7.5.1. Single-Type-Import
			Assertions.assertEquals("java.math.BigDecimal",
					SymbolTableUtils.lookup(compilationUnitTree,
							elements.getName("BigDecimal")).toString());
			//
			// 7.5.2. Type-Import-on-Demand
			Assertions.assertEquals("java.util.Random",
					SymbolTableUtils.lookup(compilationUnitTree,
							elements.getName("Random")).toString());
			//
			// java.lang auto import
			Assertions.assertEquals("java.lang.System",
					SymbolTableUtils.lookup(compilationUnitTree,
							elements.getName("System")).toString());
		});
	}
}
