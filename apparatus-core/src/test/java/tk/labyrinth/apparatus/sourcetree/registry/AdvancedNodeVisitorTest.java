package tk.labyrinth.apparatus.sourcetree.registry;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.test.model.MemberSelects;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class AdvancedNodeVisitorTest {

	@CompilationTarget(sourceTypes = MemberSelects.class)
	@Test
	void testCollectMemberSelects(AnnotationProcessingRound round) {
		List<MemberSelectNode> nodes = TestNodeUtils.collectMemberSelects(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				//
				//           4         3         2    1
				// package tk.labyrinth.apparatus.test.model;
				"tk.labyrinth.apparatus.test.model",
				"tk.labyrinth.apparatus.test",
				"tk.labyrinth.apparatus",
				"tk.labyrinth",
				//
				//            6    5
				// import java.math.BigDecimal;
				"java.math.BigDecimal",
				"java.math",
				//
				//                            7
				// BigDecimal bd0 = BigDecimal.ONE;
				"BigDecimal.ONE",
				//
				// Nothing here
				// BigDecimal bd1 = BigDecimal.valueOf(1);
				//
				//                      9    8
				// BigDecimal bd2 = java.math.BigDecimal.valueOf(1);
				"java.math.BigDecimal",
				"java.math",
				//
				// Nothing here
				// int min = 144 / BigDecimal.valueOf(12).intValue();
				//
				//       10                                   11
				// System.out.println(Math.max(min, BigDecimal.TEN.signum()));
				"System.out",
				"BigDecimal.TEN"
		), ListUtils.mapNonnull(nodes, MemberSelectNode::toString));
	}
}
