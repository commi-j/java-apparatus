package tk.labyrinth.apparatus.test.model.memberselect;

import java.math.BigDecimal;

@SuppressWarnings({"AccessStaticViaInstance", "unused"})
public class SimpleMemberSelects {

	static {
		{
			// Static Method Invocation
			System.out.println();
		}
		{
			// Variable Assignment
			Object o = System.out;
		}
		{
			// Complex Selection
			Object o = BigDecimal.ZERO.negate().ONE.TEN;
		}
	}
}
