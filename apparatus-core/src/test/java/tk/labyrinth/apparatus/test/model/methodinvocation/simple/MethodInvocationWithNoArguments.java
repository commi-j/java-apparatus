package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

@SuppressWarnings({"SameParameterValue", "unused"})
public class MethodInvocationWithNoArguments {

	static {
		methodWithNoArguments();
	}

	static void methodWithNoArguments() {
		// no-op
	}
}
