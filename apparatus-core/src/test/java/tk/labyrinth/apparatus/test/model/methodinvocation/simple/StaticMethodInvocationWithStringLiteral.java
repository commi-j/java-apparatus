package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

@SuppressWarnings({"SameParameterValue", "unused"})
public class StaticMethodInvocationWithStringLiteral {

	static {
		method("stringLiteral");
	}

	static void method(String value) {
		// no-op
	}
}
