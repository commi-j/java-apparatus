package tk.labyrinth.apparatus.test.model.identifier.simple;

import java.math.BigDecimal;
import java.math.BigInteger;

@SuppressWarnings({"ConstantConditions", "unused"})
public class LocalVariableInitializedWithLocalFields {

	static BigDecimal staticField;

	BigInteger instanceField;

	{
		Number staticNumber = staticField;
		Object instanceNumber = instanceField;
	}
}
