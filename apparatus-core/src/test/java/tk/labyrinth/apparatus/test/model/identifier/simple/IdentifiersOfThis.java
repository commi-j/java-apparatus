package tk.labyrinth.apparatus.test.model.identifier.simple;

import java.util.RandomAccess;

/**
 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-15.html#jls-15.8.3
 */
@SuppressWarnings("unused")
public class IdentifiersOfThis implements RandomAccess {

	RandomAccess field = this;

	{
		Object localVariable = this;
	}

	void instanceMethod() {
		RandomAccess localVariable = this;
	}

	interface Interface {

		default void defaultMethod() {
			Object localVariable = this;
		}
	}
}
