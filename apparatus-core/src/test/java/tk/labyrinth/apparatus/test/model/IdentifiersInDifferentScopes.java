package tk.labyrinth.apparatus.test.model;

public class IdentifiersInDifferentScopes {

	private final String foo = "foo";

	{
		foo();
		String bar = foo;
		bar();
	}

	private void bar() {
		// no-op
	}

	private void foo() {
		// no-op
	}
}
