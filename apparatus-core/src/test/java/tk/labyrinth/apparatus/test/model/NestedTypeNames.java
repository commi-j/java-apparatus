package tk.labyrinth.apparatus.test.model;

@SuppressWarnings("unused")
public class NestedTypeNames {

	static {
		// 2/2
		NestedTypeNames t00;
		tk.labyrinth.apparatus.test.model.NestedTypeNames t01;
		//
		// 3/3
		Nested t10;
		NestedTypeNames.Nested t11;
		tk.labyrinth.apparatus.test.model.NestedTypeNames.Nested t12;
		//
		// 3/4
		// Nestedmost t20;
		Nested.Nestedmost t21;
		NestedTypeNames.Nested.Nestedmost t22;
		tk.labyrinth.apparatus.test.model.NestedTypeNames.Nested.Nestedmost t23;
	}

	public static class Nested {

		static {
			// 2/2
			NestedTypeNames t00;
			tk.labyrinth.apparatus.test.model.NestedTypeNames t01;
			//
			// 3/3
			Nested t10;
			NestedTypeNames.Nested t11;
			tk.labyrinth.apparatus.test.model.NestedTypeNames.Nested t12;
			//
			// 4/4
			Nestedmost t20;
			Nested.Nestedmost t21;
			NestedTypeNames.Nested.Nestedmost t22;
			tk.labyrinth.apparatus.test.model.NestedTypeNames.Nested.Nestedmost t23;
		}

		public static class Nestedmost {

			static {
				// 2/2
				NestedTypeNames t00;
				tk.labyrinth.apparatus.test.model.NestedTypeNames t01;
				//
				// 3/3
				Nested t10;
				NestedTypeNames.Nested t11;
				tk.labyrinth.apparatus.test.model.NestedTypeNames.Nested t12;
				//
				// 4/4
				Nestedmost t20;
				Nested.Nestedmost t21;
				NestedTypeNames.Nested.Nestedmost t22;
				tk.labyrinth.apparatus.test.model.NestedTypeNames.Nested.Nestedmost t23;
			}
		}
	}
}
