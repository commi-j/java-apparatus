package tk.labyrinth.apparatus.test.model;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class MemberSelects {

	static int qwe = 12;

	static {
		{
			// Static Field
			BigDecimal bd0 = BigDecimal.ONE;
			//
			// Static Method
			BigDecimal bd1 = BigDecimal.valueOf(1);
			//
			// Static Method with qualified name
			BigDecimal bd2 = java.math.BigDecimal.valueOf(1);
		}
		{
			int min = 144 / BigDecimal.valueOf(12).intValue();
			System.out.println(Math.max(min, BigDecimal.TEN.signum()));
		}
	}

	static long qwe() {
		return 14;
	}
}
