package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

@SuppressWarnings("UnusedReturnValue")
public class StaticMethodInvocationFromSelf {

	static {
		testMethod();
	}

	static Integer testMethod() {
		throw new UnsupportedOperationException();
	}
}
