package tk.labyrinth.apparatus.test.model.memberselect;

/**
 * If we create a nested class named java javac will fail the compilation althou IDEA thinks code is valid.
 */
public class FieldOrNestedClassHardcoreCase {

	{
//		BigInteger bi0 = null;
		java.math.BigInteger.class.hashCode();
	}
//	private static class java {
////		private static math math;
////
////		private static class math {
////
////			private static Object BigInteger = new Object();
////		}
//	}
}
