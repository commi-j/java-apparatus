package tk.labyrinth.apparatus.test.model.memberselect;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class FieldOrNestedClassCase {

	static {
		ClassWithField.Member.length();
		//
		System.out.println(ClassWithFieldAndNestedClass.Member);
		ClassWithFieldAndNestedClass.Member.length();
		ClassWithFieldAndNestedClass.Member mem = new ClassWithFieldAndNestedClass.Member();
		//
		ClassWithNestedClass.Member.length();
	}

	public static class Member {
		// empty
	}

	public static class ClassWithField {

		public static String Member;
	}

	public static class ClassWithFieldAndNestedClass {

		public static String Member;

		public static class Member {

			@SuppressWarnings("UnusedReturnValue")
			public static long length() {
				throw new UnsupportedOperationException();
			}
		}
	}

	public static class ClassWithNestedClass {

		public static class Member {

			@SuppressWarnings("UnusedReturnValue")
			public static long length() {
				throw new UnsupportedOperationException();
			}
		}
	}
}
