package tk.labyrinth.apparatus.test.model.identifier;

@SuppressWarnings({"ConstantConditions", "ResultOfMethodCallIgnored", "unused", "UnusedReturnValue"})
public class IdentifiersInMethodInvocations {

	static Object field;

	static Object method() {
		return null;
	}

	static Object method(Object bar) {
		return null;
	}

	static void withDirectStaticImport() {
		// TODO
	}

	static void withField() {
		//
		method(field);
	}

	static void withLocalVariable() {
		Object localVariable = null;
		//
		method(localVariable);
	}

	static void withMethodInvocation() {
		//
		method(method());
	}

	static void withParameter(Object parameter) {
		//
		method(parameter);
	}

	static void withStarStaticImport() {
		// TODO
	}
}
