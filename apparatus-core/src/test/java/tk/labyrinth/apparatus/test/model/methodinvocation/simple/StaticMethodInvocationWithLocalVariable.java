package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

import java.math.BigDecimal;

@SuppressWarnings({"ConstantConditions", "SameParameterValue", "unused"})
public class StaticMethodInvocationWithLocalVariable {

	static {
		BigDecimal localVariable = null;
		//
		method(localVariable);
	}

	static void method(Object value) {
		// no-op
	}
}
