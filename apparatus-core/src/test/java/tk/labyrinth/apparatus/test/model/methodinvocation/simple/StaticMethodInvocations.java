package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

@SuppressWarnings("UnusedReturnValue")
public class StaticMethodInvocations {

	static {
		testMethod();
		StaticMethodInvocations.testMethod();
		tk.labyrinth.apparatus.test.model.methodinvocation.simple.StaticMethodInvocations.testMethod();
	}

	static Integer testMethod() {
		throw new UnsupportedOperationException();
	}
}
