package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

public class MethodInvocationsWithNoTarget {

	static void parentMethod() {
		// no-op
	}

	public static class Child {

		static {
			childMethod();
			parentMethod();
		}

		static void childMethod() {
			// no-op
		}
	}
}
