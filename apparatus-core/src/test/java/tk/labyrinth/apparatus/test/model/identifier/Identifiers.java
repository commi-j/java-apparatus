package tk.labyrinth.apparatus.test.model.identifier;

//@SuppressWarnings({"ConstantConditions", "UnnecessaryLocalVariable", "unused"})
public class Identifiers {

	static Object foo;

	static Object foo(Object bar) {
		return null;
	}

	static void withField() {
		//
		// Field as VariableInitializer
		Object obj = foo;
	}

	static void withLocalVariable() {
		Object foo = null;
		//
		// LocalVariable as VariableInitializer
		Object obj = foo;
	}

	static void withMethodInvocation() {
		//
		// Field as MethodArgument
		// MethodInvocation as VariableInitializer
		Object obj = foo(foo);
	}

	static void withParameter(Object foo) {
		//
		// Parameter as VariableInitializer
		Object obj = foo;
	}
}
