package tk.labyrinth.apparatus.test.model.memberselect;

import java.math.BigDecimal;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class MethodSelection {

	{
		// Static Methods
		BigDecimal.valueOf(1);
		BigDecimal.valueOf(1d);
		//
		// Static Methods with Qualified Names
		java.math.BigDecimal.valueOf(1);
		java.math.BigDecimal.valueOf(1d);
	}
}
