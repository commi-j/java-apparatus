package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

import java.math.BigDecimal;

@SuppressWarnings({"ConstantConditions", "SameParameterValue", "unused"})
public class StaticMethodInvocationWithLocalField {

	static BigDecimal field;

	static {
		method(field);
	}

	static void method(Object value) {
		// no-op
	}
}
