package tk.labyrinth.apparatus.test.util;

import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.IdentifierNode;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.model.node.MethodNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.PackageNode;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.apparatus.sourcetree.registry.AdvancedNodeVisitor;
import tk.labyrinth.apparatus.sourcetree.registry.CompilationBundleTreeRegistry;
import tk.labyrinth.apparatus.sourcetree.registry.TreeRegistry;
import tk.labyrinth.apparatus.sourcetree.util.CompilationUnitTreeUtils;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;

import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class TestNodeUtils {

	private static <T> Stream<T> doCollect(AnnotationProcessingRound round, UnaryOperator<AdvancedNodeVisitor<T>> visitorConfigurer) {
		AdvancedNodeVisitor<T> visitor = visitorConfigurer.apply(new AdvancedNodeVisitor<>());
		TreeRegistry treeRegistry = new CompilationBundleTreeRegistry(RoundContext.of(round));
		return CompilationUnitTreeUtils.streamFrom(round)
				.flatMap(compilationUnitTree -> compilationUnitTree.accept(
						visitor.getTreeVisitor(), treeRegistry));
	}

	private static <T> Stream<T> doCollect(Node node, UnaryOperator<AdvancedNodeVisitor<T>> visitorConfigurer) {
		AdvancedNodeVisitor<T> visitor = visitorConfigurer.apply(new AdvancedNodeVisitor<>());
		return node.getTree().accept(visitor.getTreeVisitor(), null);
	}

	private static <T> Stream<T> doCollect(RoundContext roundContext, UnaryOperator<AdvancedNodeVisitor<T>> visitorConfigurer) {
		AdvancedNodeVisitor<T> visitor = visitorConfigurer.apply(new AdvancedNodeVisitor<>());
		TreeRegistry treeRegistry = new CompilationBundleTreeRegistry(roundContext);
		return CompilationUnitTreeUtils.streamFrom(roundContext.getRound())
				.flatMap(compilationUnitTree -> compilationUnitTree.accept(
						visitor.getTreeVisitor(), treeRegistry));
	}

	public static Stream<BlockNode> collectBlocks(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitBlock(Stream::of));
	}

	public static Stream<BlockNode> collectBlocks(Node node) {
		return doCollect(node, visitor -> visitor.visitBlock(Stream::of));
	}

	public static Stream<ClassNode> collectClasses(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitClass(Stream::of));
	}

	public static Stream<ExpressionNode> collectExpressions(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitExpression(Stream::of));
	}

	public static Stream<IdentifierNode> collectIdentifiers(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitIdentifier(Stream::of));
	}

	public static Stream<IdentifierNode> collectIdentifiers(Node node) {
		return doCollect(node, visitor -> visitor.visitIdentifier(Stream::of));
	}

	public static Stream<MemberReferenceNode> collectMemberReferences(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitMemberReference(Stream::of));
	}

	public static Stream<MemberReferenceNode> collectMemberReferences(RoundContext roundContext) {
		return doCollect(roundContext, visitor -> visitor.visitMemberReference(Stream::of));
	}

	public static Stream<MemberSelectNode> collectMemberSelects(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitMemberSelect(Stream::of));
	}

	public static Stream<MemberSelectNode> collectMemberSelects(Node node) {
		return doCollect(node, visitor -> visitor.visitMemberSelect(Stream::of));
	}

	public static Stream<MethodInvocationNode> collectMethodInvocations(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitMethodInvocation(Stream::of));
	}

	public static Stream<MethodInvocationNode> collectMethodInvocations(RoundContext roundContext) {
		return doCollect(roundContext, visitor -> visitor.visitMethodInvocation(Stream::of));
	}

	public static Stream<MethodNode> collectMethods(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitMethod(Stream::of));
	}

	public static Stream<Node> collectNodes(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitNode(Stream::of));
	}

	public static Stream<PackageNode> collectPackages(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitPackage(Stream::of));
	}

	public static Stream<VariableNode> collectVariables(AnnotationProcessingRound round) {
		return doCollect(round, visitor -> visitor.visitVariable(Stream::of));
	}

	public static Stream<VariableNode> collectVariables(Node node) {
		return doCollect(node, visitor -> visitor.visitVariable(Stream::of));
	}

	public static void forEachNode(AnnotationProcessingRound round, Consumer<Node> callback) {
		AdvancedNodeVisitor<?> visitor = new AdvancedNodeVisitor<>()
				.visitNode(node -> {
					callback.accept(node);
					return null;
				});
		TreeRegistry treeRegistry = new CompilationBundleTreeRegistry(RoundContext.of(round));
		CompilationUnitTreeUtils.streamFrom(round)
				.forEach(compilationUnitTree -> compilationUnitTree.accept(
						visitor.getTreeVisitor(), treeRegistry));
	}

	public static void printAllNodes(AnnotationProcessingRound round) {
		forEachNode(round, node -> {
			System.out.println("NODE: " + node + " / " + node.getClass().getSimpleName());
		});
	}
}
