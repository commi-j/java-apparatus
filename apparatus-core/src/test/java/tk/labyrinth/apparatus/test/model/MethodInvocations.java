package tk.labyrinth.apparatus.test.model;

public class MethodInvocations {

	private void parentInstanceMethod() {
		// no-op
	}

	private static void parentStaticMethod() {
		// no-op
	}

	private class Inner {

		{
			parentInstanceMethod();
			childInstanceMethod();
			//
			parentStaticMethod();
			// childStaticMethod();
		}

		private void childInstanceMethod() {
			// no-op
		}
	}

	private static class StaticNested {

		{
			// parentInstanceMethod();
			childInstanceMethod();
			//
			parentStaticMethod();
			childStaticMethod();
		}

		private void childInstanceMethod() {
			// no-op
		}

		private static void childStaticMethod() {
			// no-op
		}
	}
}
