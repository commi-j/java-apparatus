package tk.labyrinth.apparatus.test.model.methodinvocation;

@SuppressWarnings({"ResultOfMethodCallIgnored", "unused"})
public class MethodInvocationOnVariableIdentifiers {

	public static Object field = new Object();

	static void method(Object parameter) {
		Object localVariable = 14;
		//
		localVariable.hashCode();
		parameter.hashCode();
		field.hashCode();
	}
}
