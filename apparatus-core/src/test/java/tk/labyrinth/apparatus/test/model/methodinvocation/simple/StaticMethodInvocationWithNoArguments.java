package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

@SuppressWarnings({"SameParameterValue", "unused"})
public class StaticMethodInvocationWithNoArguments {

	static {
		method();
	}

	static void method() {
		// no-op
	}
}
