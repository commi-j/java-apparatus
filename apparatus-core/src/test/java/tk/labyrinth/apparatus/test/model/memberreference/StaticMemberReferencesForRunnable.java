package tk.labyrinth.apparatus.test.model.memberreference;

@SuppressWarnings("unused")
public class StaticMemberReferencesForRunnable {

	{
		{
			process(System::currentTimeMillis);
		}
		{
			Runnable r = System::console;
		}
		{
			Object o = (Runnable) System::runFinalization;
		}
	}

	void process(Runnable runnable) {
		runnable.run();
	}
}
