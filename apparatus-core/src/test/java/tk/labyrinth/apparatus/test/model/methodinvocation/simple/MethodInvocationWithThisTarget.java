package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

public class MethodInvocationWithThisTarget {

	{
		this.thisMethod();
	}

	void thisMethod() {
		// no-op
	}
}
