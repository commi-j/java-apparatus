package tk.labyrinth.apparatus.test.model.declaration;

import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version 1.0.0
 */
public class TestDeclarations {

	public static MethodDeclaration bigDecimalGreaterThan() {
		return MethodDeclaration.builder()
				.formalParameters(List.of(FormalParameterDeclaration.builder()
						.name("other")
						.type(TypeDescription.ofNonParameterized(BigDecimal.class))
						.build()))
				.modifiers(List.of(JavaMethodModifier.PUBLIC))
				.name("greaterThan")
				.returnType(TypeDescription.ofNonParameterized(boolean.class))
				.build();
	}
}
