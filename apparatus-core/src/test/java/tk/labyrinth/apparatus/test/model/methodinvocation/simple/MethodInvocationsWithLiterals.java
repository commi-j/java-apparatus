package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

@SuppressWarnings({"SameParameterValue", "unused"})
public class MethodInvocationsWithLiterals {

	static {
		methodWithLiterals(true);
		methodWithLiterals(12);
		methodWithLiterals(12L);
		methodWithLiterals("twelve");
		methodWithLiterals(null);
	}

	static void methodWithLiterals(Object value) {
		// no-op
	}
}
