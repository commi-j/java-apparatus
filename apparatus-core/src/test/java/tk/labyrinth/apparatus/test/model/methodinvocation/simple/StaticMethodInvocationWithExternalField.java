package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

import java.math.BigDecimal;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class StaticMethodInvocationWithExternalField {

	static {
		String.valueOf(BigDecimal.ZERO);
	}
}
