package tk.labyrinth.apparatus.test.model.methodinvocation;

@SuppressWarnings({"ClassInitializerMayBeStatic", "unused"})
public class MethodInvocationsInVariousScopes {

	static {
		System.currentTimeMillis();
	}

	{
		System.currentTimeMillis();
	}

	public MethodInvocationsInVariousScopes() {
		System.currentTimeMillis();
	}

	void instanceMethod() {
		System.currentTimeMillis();
	}

	static void staticMethod() {
		System.currentTimeMillis();
	}
}
