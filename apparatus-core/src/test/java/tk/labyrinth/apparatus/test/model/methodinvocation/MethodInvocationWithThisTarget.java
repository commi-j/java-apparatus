package tk.labyrinth.apparatus.test.model.methodinvocation;

@SuppressWarnings("unused")
public class MethodInvocationWithThisTarget {

	{
		//
		// Explicit this target
		this.instanceMethod();
		//
		// Implicit this target
		instanceMethod();
	}

	void instanceMethod() {
		// no-op
	}
}
