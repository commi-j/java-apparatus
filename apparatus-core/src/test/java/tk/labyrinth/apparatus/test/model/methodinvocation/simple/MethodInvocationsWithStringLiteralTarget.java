package tk.labyrinth.apparatus.test.model.methodinvocation.simple;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class MethodInvocationsWithStringLiteralTarget {

	static {
		"hello".length();
	}
}
