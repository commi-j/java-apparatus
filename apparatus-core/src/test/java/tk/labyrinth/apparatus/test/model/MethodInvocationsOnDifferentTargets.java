package tk.labyrinth.apparatus.test.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

@SuppressWarnings({"rawtypes", "ResultOfMethodCallIgnored"})
public interface MethodInvocationsOnDifferentTargets {

	Random field = new Random();

	static void method(BigDecimal methodArgument) {
		{
			BigInteger variable = BigInteger.ONE;
			variable.hashCode();
		}
		{
			methodArgument.hashCode();
		}
		{
			field.hashCode();
		}
		{
			// String literal
			"foo".hashCode();
		}
		{
			// Plain constructor
			new Date().hashCode();
		}
		{
			// Generic constructor
			new ArrayList().hashCode();
			new ArrayList<>().hashCode();
			new ArrayList<String>().hashCode();
		}
//		{
//			// Plain anonymous
//			new Date() {
//				// empty
//			}.toInstant();
//		}
	}
}
