package tk.labyrinth.apparatus.study;

public class MethodSelections {

	static {
		A a = new A();
		{
			// A-D - interface
			// A-B-C - class
			// Class method always wins.
			a.classVsInterfaceMethod();
		}
	}

	interface D {

		default void classVsInterfaceMethod() {
			// no-op
		}
	}

	interface E {

	}

	static class A extends B implements D {

	}

	static class B extends C {

	}

	static class C {

		public void classVsInterfaceMethod() {
			// no-op
		}
	}
}
