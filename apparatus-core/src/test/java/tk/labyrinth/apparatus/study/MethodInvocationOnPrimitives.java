package tk.labyrinth.apparatus.study;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
public class MethodInvocationOnPrimitives {

	@CompilationTarget(sourceResources = "MethodInvocationOnPrimitiveLiteral.java")
	@Disabled
	@Test
	void testOnLiteral(AnnotationProcessingRound round) {
		List<Node> nodes = TestNodeUtils.collectNodes(round)
				.collect(Collectors.toList());
		//
		System.out.println();
	}

	@CompilationTarget(sourceResources = "MethodInvocationOnPrimitiveLocalVariable.java")
	@Disabled
	@Test
	void testOnLocalVariable(AnnotationProcessingRound round) {
		List<Node> nodes = TestNodeUtils.collectNodes(round)
				.collect(Collectors.toList());
		//
		System.out.println();
	}
}
