package tk.labyrinth.apparatus.model.node.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.test.model.memberreference.StaticMemberReferencesForRunnable;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class MemberReferenceNodeImplTest {

	@CompilationTarget(sourceTypes = {
			StaticMemberReferencesForRunnable.class
	})
	@Disabled
	@Test
	void testFindFunctionalInterface(AnnotationProcessingRound round) {
		ProcessingContext context = ProcessingContext.of(round);
		//
		List<MemberReferenceNode> nodes = TestNodeUtils.collectMemberReferences(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				context.getTypeTemplate(Runnable.class),
				context.getTypeTemplate(Runnable.class),
				context.getTypeTemplate(Runnable.class)
		), ListUtils.mapNonnull(nodes, MemberReferenceNode::findFunctionalInterface));
	}

	@CompilationTarget(sourceTypes = {
			StaticMemberReferencesForRunnable.class
	})
	@Test
	void testFindMethod(AnnotationProcessingRound round) {
		ProcessingContext context = ProcessingContext.of(round);
		//
		List<MemberReferenceNode> nodes = TestNodeUtils.collectMemberReferences(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				context.getMethodElementTemplateByName(System.class, "currentTimeMillis"),
				context.getMethodElementTemplateByName(System.class, "console"),
				context.getMethodElementTemplateByName(System.class, "runFinalization")
		), ListUtils.mapNonnull(nodes, MemberReferenceNode::findMethod));
	}

	@CompilationTarget(sourceTypes = {
			StaticMemberReferencesForRunnable.class
	})
	@Test
	void testGetParentNode(AnnotationProcessingRound round) {
		List<MemberReferenceNode> nodes = TestNodeUtils.collectMemberReferences(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				"process(System::currentTimeMillis)",
				"Runnable r = System::console",
				"(Runnable)System::runFinalization"
		), ListUtils.mapNonnull(nodes, node -> node.getParentNode().toString()));
	}
}
