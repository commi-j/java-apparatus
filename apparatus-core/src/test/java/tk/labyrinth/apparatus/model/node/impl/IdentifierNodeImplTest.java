package tk.labyrinth.apparatus.model.node.impl;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.misc4j.java.util.collectoin.ListUtils;
import tk.labyrinth.apparatus.model.node.IdentifierNode;
import tk.labyrinth.apparatus.test.model.identifier.Identifiers;
import tk.labyrinth.apparatus.test.model.identifier.simple.IdentifiersOfThis;
import tk.labyrinth.apparatus.test.model.identifier.simple.LocalVariableInitializedWithLocalFields;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class IdentifierNodeImplTest {

	@CompilationTarget(sourceTypes = Identifiers.class)
	@Test
	void testFindType(AnnotationProcessingRound round, ProcessingContext context) {
		List<IdentifierNode> nodes = TestNodeUtils.collectIdentifiers(round)
				.collect(Collectors.toList());
		TypeHandle objectType = context.getTypeHandle(Object.class);
		//
		Assertions.assertEquals(List.of(
				Pair.of("tk", null),
				Pair.of("Object", objectType),
				Pair.of("Object", objectType),
				Pair.of("Object", objectType),
				Pair.of("Object", objectType),
				Pair.of("foo", objectType),
				Pair.of("Object", objectType),
				Pair.of("Object", objectType),
				Pair.of("foo", objectType),
				Pair.of("Object", objectType),
				Pair.of("foo", objectType),
				Pair.of("Object", objectType),
				Pair.of("Object", objectType),
				Pair.of("foo", objectType)
		), ListUtils.mapNonnull(nodes, node -> Pair.of(node.toString(), node.findType())));
	}

	@Disabled
	@CompilationTarget(sourceTypes = LocalVariableInitializedWithLocalFields.class)
	@Test
	void testGetEntityWithLocalFields(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
//		List<IdentifierNode> nodes = TestNodeUtils.collectBlocks(round)
//				.skip(1)
//				.flatMap(TestNodeUtils::collectIdentifiers)
//				.collect(Collectors.toList());
//		//
//		Assertions.assertEquals(List.of(
//				"java.lang.Number",
//				"static BigDecimal staticField",
//				"java.lang.Object",
//				"BigInteger instanceField"
//		), ListUtils.mapNonnull(nodes, node -> IdentifierTreeUtils.getEntity(
//				node.getTreeRegistry(), node.toSelector()).toString()));
	}

	@Disabled
	@CompilationTarget(sourceTypes = IdentifiersOfThis.class)
	@Test
	void testGetEntityWithThis(AnnotationProcessingRound round, ProcessingEnvironment processingEnvironment) {
//		List<IdentifierNode> nodes = TestNodeUtils.collectClasses(round)
//				.limit(1) // Ignore nested Types.
//				.flatMap(TestNodeUtils::collectIdentifiers)
//				.skip(4) // Skipping SuppressWarning, value, RandomAccess, super.
//				.collect(Collectors.toList());
//		//
//		Assertions.assertEquals(List.of(
//				"java.util.RandomAccess",
//				"tk.labyrinth.apparatus.test.model.identifier.simple.IdentifiersOfThis.this",
//				"java.lang.Object",
//				"tk.labyrinth.apparatus.test.model.identifier.simple.IdentifiersOfThis.this",
//				"java.util.RandomAccess",
//				"tk.labyrinth.apparatus.test.model.identifier.simple.IdentifiersOfThis.this",
//				"java.lang.Object",
//				"tk.labyrinth.apparatus.test.model.identifier.simple.IdentifiersOfThis.Interface.this"
//		), ListUtils.mapNonnull(nodes, node -> IdentifierTreeUtils.getEntity(
//				node.getTreeRegistry(), node.toSelector()).toString()));
	}
}
