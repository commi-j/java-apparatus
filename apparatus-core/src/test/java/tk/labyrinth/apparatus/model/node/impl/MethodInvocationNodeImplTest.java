package tk.labyrinth.apparatus.model.node.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.test.model.MethodInvocations;
import tk.labyrinth.apparatus.test.model.MethodInvocationsOnDifferentTargets;
import tk.labyrinth.apparatus.test.model.declaration.TestDeclarations;
import tk.labyrinth.apparatus.test.model.methodinvocation.MethodInvocationOnVariableIdentifiers;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.MethodInvocationWithNoArguments;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.MethodInvocationWithThisTarget;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.MethodInvocationsWithLiterals;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.MethodInvocationsWithNoTarget;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.MethodInvocationsWithStringLiteralTarget;
import tk.labyrinth.apparatus.test.model.methodinvocation.simple.MethodInvocationsWithTypeTarget;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.enhanced.EnhancedProcessing;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExpectCompilationFailure;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@ExtendWithJaap
class MethodInvocationNodeImplTest {

	@CompilationTarget(sourceTypes = {
			MethodInvocationsWithNoTarget.class,
			MethodInvocationsWithStringLiteralTarget.class,
			MethodInvocationsWithTypeTarget.class,
			MethodInvocationWithThisTarget.class,
	})
	@Test
	void testFindMethod(AnnotationProcessingRound round) {
		ProcessingContext context = ProcessingContext.of(round);
		//
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !Objects.equals(node.getMethodName(), "super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				context.getMethodElementTemplate(MethodInvocationsWithNoTarget.Child.class, "childMethod()"),
				context.getMethodElementTemplate(MethodInvocationsWithNoTarget.class, "parentMethod()"),
				context.getMethodElementTemplate(String.class, "length()"),
				context.getMethodElementTemplate(System.class, "currentTimeMillis()"),
				context.getMethodElementTemplate(MethodInvocationWithThisTarget.class, "thisMethod()")
		), ListUtils.mapNonnull(nodes, MethodInvocationNode::findMethod));
	}

	@CompilationTarget(sourceTypes = {
			MethodInvocationWithNoArguments.class,
			MethodInvocationsWithLiterals.class,
	})
	@Test
	void testFindMethodSignature(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !Objects.equals(node.getMethodName(), "super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				MethodSimpleSignature.of("methodWithNoArguments()"),
				MethodSimpleSignature.of("methodWithLiterals(boolean)"),
				MethodSimpleSignature.of("methodWithLiterals(int)"),
				MethodSimpleSignature.of("methodWithLiterals(long)"),
				MethodSimpleSignature.of("methodWithLiterals(java.lang.String)"),
				MethodSimpleSignature.of("methodWithLiterals(java.lang.Object)")
		), ListUtils.mapNonnull(nodes, MethodInvocationNode::findMethodSignature));
	}

	@CompilationTarget(sourceResources = "BigDecimalGreaterThan.java")
	@ExpectCompilationFailure
	@Test
	void testFindMethodWithSyntheticMethod(AnnotationProcessingRound round) {
		RoundContext roundContext;
		{
			SyntheticElementTemplateRegistry syntheticElementTemplateRegistry = new SyntheticElementTemplateRegistry();
			roundContext = RoundContext.of(
					round,
					EnhancedProcessing.createContext(
							round.getProcessingEnvironment(),
							syntheticElementTemplateRegistry));
			//
			syntheticElementTemplateRegistry.registerMethodDeclaration(
					CanonicalTypeSignature.of(BigDecimal.class),
					TestDeclarations.bigDecimalGreaterThan());
		}
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(roundContext)
				.filter(node -> !Objects.equals(node.getMethodName(), "super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(
				List.of(
						roundContext.getProcessingContext().getMethodElementTemplate(
								"java.math.BigDecimal#greaterThan(java.math.BigDecimal)")),
				ListUtils.mapNonnull(nodes, MethodInvocationNode::findMethod));
	}

	@CompilationTarget(sourceTypes = MethodInvocationsOnDifferentTargets.class)
	@Test
	void testGetTargetDeclarationOnDifferentTargets(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				"BigInteger variable = BigInteger.ONE",
				"BigDecimal methodArgument",
				"Random field = new Random()",
				"\"foo\"",
				"new Date()",
				"new ArrayList()",
				"new ArrayList<>()",
				"new ArrayList<String>()"
		), ListUtils.mapNonnull(nodes, node -> node.getTargetDeclaration().toString()));
	}

	@CompilationTarget(sourceTypes = MethodInvocationOnVariableIdentifiers.class)
	@Test
	void testGetTargetDeclarationOnVariableIdentifiers(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				"Object localVariable = 14",
				"Object parameter",
				"public static Object field = new Object()"
		), ListUtils.mapNonnull(nodes, node -> node.getTargetDeclaration().toString()));
	}

	// TODO: Make this work when we decide how to treat no target and this cases.
	@CompilationTarget(sourceTypes = MethodInvocations.class)
	@Disabled
	@Test
	void testGetTargetDeclarationWithDifferentScopes(AnnotationProcessingRound round) {
		List<MethodInvocationNode> nodes = TestNodeUtils.collectMethodInvocations(round)
				.filter(node -> !node.getMethodName().contentEquals("super"))
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(List.of(
				"tk.labyrinth.apparatus.test.model.MethodInvocations.Inner.this",
				"tk.labyrinth.apparatus.test.model.MethodInvocations.Inner.this",
				"tk.labyrinth.apparatus.test.model.MethodInvocations.Inner",
				"tk.labyrinth.apparatus.test.model.MethodInvocations.StaticNested.this",
				"tk.labyrinth.apparatus.test.model.MethodInvocations.StaticNested",
				"tk.labyrinth.apparatus.test.model.MethodInvocations.StaticNested"
		), ListUtils.mapNonnull(nodes, node -> node.getTargetDeclaration().toString()));
	}
}
