package tk.labyrinth.apparatus.model.node.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.test.model.memberselect.SimpleMemberSelects;
import tk.labyrinth.apparatus.test.util.TestNodeUtils;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWithJaap
class MemberSelectNodeImplTest {

	@CompilationTarget(sourceTypes = SimpleMemberSelects.class)
	@Test
	void testFindElement(AnnotationProcessingRound round) {
		// FIXME: Move to method parameter.
		ProcessingContext processingContext = ProcessingContext.of(round);
		//
		List<MemberSelectNode> nodes = TestNodeUtils.collectMemberSelects(round)
				.collect(Collectors.toList());
		//
		Assertions.assertEquals(Arrays.asList(
				//
				// package tk.labyrinth.apparatus.test.model.memberselect;
				processingContext.getPackageElementTemplate("tk.labyrinth.apparatus.test.model.memberselect"),
				processingContext.getPackageElementTemplate("tk.labyrinth.apparatus.test.model"),
//
				// TODO: Think about synth packages.
//				processingContext.getPackageElementTemplate("tk.labyrinth.apparatus.test", true),
//				processingContext.getPackageElementTemplate("tk.labyrinth.apparatus", true),
//				processingContext.getPackageElementTemplate("tk.labyrinth", true),
				null,
				null,
				null,
				//
				// import java.math.BigDecimal;
				processingContext.getTypeTemplate("java.math.BigDecimal"),
				processingContext.getPackageElementTemplate("java.math"),
				//
				// System.out.println();
				processingContext.getFieldElementTemplate("java.lang.System#out"),
				//
				// Object o = System.out;
				processingContext.getFieldElementTemplate("java.lang.System#out"),
				//
				// Object o = BigDecimal.ZERO.negate().ONE.TEN;
				processingContext.getFieldElementTemplate("java.math.BigDecimal#TEN"),
				processingContext.getFieldElementTemplate("java.math.BigDecimal#ONE"),
				processingContext.getFieldElementTemplate("java.math.BigDecimal#ZERO")
		), ListUtils.mapNonnull(nodes, MemberSelectNode::findElement));
	}
}
