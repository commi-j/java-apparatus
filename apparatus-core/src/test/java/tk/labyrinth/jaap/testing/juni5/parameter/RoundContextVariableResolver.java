package tk.labyrinth.jaap.testing.juni5.parameter;

import com.google.auto.service.AutoService;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.parameter.JaapVariableResolver;

@AutoService(JaapVariableResolver.class)
public class RoundContextVariableResolver implements JaapVariableResolver<RoundContext> {

	@Override
	public Class<RoundContext> getParameterType() {
		return RoundContext.class;
	}

	@Override
	public boolean isInternal() {
		return true;
	}

	@Override
	public RoundContext resolveVariable(AnnotationProcessingRound round) {
		return RoundContext.of(round);
	}
}
