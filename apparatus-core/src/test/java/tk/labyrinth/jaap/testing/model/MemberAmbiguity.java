package tk.labyrinth.jaap.testing.model;

@SuppressWarnings({"unused", "UnusedReturnValue"})
public class MemberAmbiguity {

	public static Integer Member;

	static {
		{
			// Field or NestedType?
			WithField.Member.intValue();
			WithNestedType.Member.intValue();
		}
		{
			// Field or MethodInvocation or NewClass?
			Object n0 = WithField.Member;
			Object n1 = WithMethods.Member();
			Object n2 = WithMethods.Member("");
			Object n3 = new WithNestedType.Member();
			Object n4 = new WithNestedType.Member("");
		}
	}

	public static class WithField {

		public static Number Member;
	}

	public static class WithMethods {

		public static Number Member() {
			throw new UnsupportedOperationException();
		}

		public static Number Member(String value) {
			throw new UnsupportedOperationException();
		}
	}

	public static class WithNestedType {

		public static class Member {

			public Member() {
				throw new UnsupportedOperationException();
			}

			public Member(String value) {
				throw new UnsupportedOperationException();
			}

			public static long intValue() {
				throw new UnsupportedOperationException();
			}
		}
	}
}
