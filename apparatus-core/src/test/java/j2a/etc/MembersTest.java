package j2a.etc;

import com.sun.tools.javac.tree.JCTree;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.sourcetree.scope.ElementNodeUtils;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;

@ExtendWithJaap
class MembersTest {

	@Disabled("We have different positions even for the same Java version. These are for 11.0.5 but they are different for 11.0.9.1")
	@CompilationTarget(sourceTypes = Members.class)
	@Test
	void testPos(ProcessingContext processingContext) {
		JCTree.JCClassDecl jcClassDecl = (JCTree.JCClassDecl) ElementNodeUtils.getClass(
				processingContext.getTypeElementTemplate(Members.class)).getClassTree();
		//
		ContribAssertions.assertEquals(List.of(
				Pair.of("private static final int psfi = 0", 72),
				Pair.of("private static int psi = 0", 105),
				Pair.of("private final int pfi", 136),
				Pair.of("private int pi", 157),
				Pair.of("public <init>() {", 172),
				Pair.of("public <init>(int pfi, int pi) {", 222),
				Pair.of("private void noOp() {", 306)
		), jcClassDecl.defs.stream().map(jcTree -> Pair.of(getFirstLine(jcTree.toString().trim()), jcTree.pos)));
	}

	private static String getFirstLine(String value) {
		int indexOfLineBreak = value.indexOf('\n');
		return indexOfLineBreak != -1 ? value.substring(0, indexOfLineBreak).trim() : value;
	}
}
