package j2a.etc;

public class Members {

	private static final int psfi = 0;

	private static int psi = 0;

	private final int pfi;

	private int pi;

	public Members() {
		pfi = 0;
		pi = 0;
	}

	public Members(int pfi, int pi) {
		this.pfi = pfi;
		this.pi = pi;
	}

	private void noOp() {
		//
	}
}
