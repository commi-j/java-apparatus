package tk.labyrinth.apparatus.core.resource;

public class MethodInvocationOnPrimitiveLiteral {

	{
		// This will most probably never work as we can not distinguish between dot as invocation
		// and dot as floating-point number declaration.
		int result = 12.pow(2);
	}
}
