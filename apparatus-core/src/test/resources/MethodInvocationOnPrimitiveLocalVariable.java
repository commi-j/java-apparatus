package tk.labyrinth.apparatus.core.resource;

public class MethodInvocationOnPrimitiveLocalVariable {

	{
		// This will most probably never work as we can not distinguish between dot as invocation
		// and dot as floating-point number declaration.
		int localVariable = 12;
		int result = localVariable.pow(2);
	}
}
