package tk.labyrinth.apparatus.index;

import lombok.Value;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import javax.annotation.Nullable;
import java.net.URL;

@Value
public class OldIndexEntry {

	String key;

	String module;

	/**
	 * Null if created from {@link NewIndexEntry}.
	 */
	@Nullable
	URL location;

	/**
	 * CompilationUnit.
	 */
	CanonicalTypeSignature origin;

	String value;

	public NewIndexEntry toNewIndexEntry() {
		return new NewIndexEntry(key, module, origin, value);
	}
}
