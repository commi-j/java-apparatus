package tk.labyrinth.apparatus.index;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Value;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import javax.annotation.Nullable;
import java.net.URL;

@Value
public class NewIndexEntry {

	// FIXME: Move this annotation to mixin.
	@JsonIgnore
	String key;

	String module;

	/**
	 * TopLevelType. If null, module is responsible for invalidating this entry.
	 */
	@Nullable
	CanonicalTypeSignature origin;

	String value;

	public OldIndexEntry toOldIndexEntry() {
		return toOldIndexEntry(null);
	}

	public OldIndexEntry toOldIndexEntry(@Nullable URL location) {
		return new OldIndexEntry(key, module, location, origin, value);
	}
}
