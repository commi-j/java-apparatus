package tk.labyrinth.apparatus.index;

import java.util.stream.Stream;

public interface IndexRegistry {

	void add(NewIndexEntry newIndexEntry);

	void flush();

	default Stream<OldIndexEntry> get(Class<?> key) {
		return get(key.getCanonicalName());
	}

	Stream<OldIndexEntry> get(String key);
}
