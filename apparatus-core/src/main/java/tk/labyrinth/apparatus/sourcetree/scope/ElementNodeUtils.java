package tk.labyrinth.apparatus.sourcetree.scope;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;

public class ElementNodeUtils {

	@Nullable
	public static ClassNode findClass(TypeElementTemplate typeElementTemplate) {
		ClassNode result;
		{
			Trees trees = Trees.instance(typeElementTemplate.getProcessingContext().getProcessingEnvironment());
			TreePath path = trees.getPath(typeElementTemplate.getElement());
			if (path != null) {
				NodeContext nodeContext = new TreeAwareNodeContext(path.getCompilationUnit(), typeElementTemplate.getProcessingContext(), trees);
				result = nodeContext.getNodeRegistry().getClass((ClassTree) path.getLeaf());
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static VariableNode findField(FieldElementTemplate fieldElementTemplate) {
		VariableNode result;
		{
			Trees trees = Trees.instance(fieldElementTemplate.getProcessingContext().getProcessingEnvironment());
			TreePath path = trees.getPath(fieldElementTemplate.getElement());
			if (path != null) {
				NodeContext nodeContext = new TreeAwareNodeContext(path.getCompilationUnit(), fieldElementTemplate.getProcessingContext(), trees);
				result = nodeContext.getNodeRegistry().getVariable((VariableTree) path.getLeaf());
			} else {
				result = null;
			}
		}
		return result;
	}

	public static ClassNode getClass(TypeElementTemplate typeElementTemplate) {
		ClassNode result = findClass(typeElementTemplate);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeElementTemplate = " + typeElementTemplate);
		}
		return result;
	}

	public static VariableNode getField(FieldElementTemplate fieldElementTemplate) {
		VariableNode result = findField(fieldElementTemplate);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fieldElementTemplate = " + fieldElementTemplate);
		}
		return result;
	}
}
