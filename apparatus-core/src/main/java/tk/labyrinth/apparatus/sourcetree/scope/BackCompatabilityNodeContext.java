package tk.labyrinth.apparatus.sourcetree.scope;

import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

@RequiredArgsConstructor
public class BackCompatabilityNodeContext implements NodeContext {

	@Getter
	private final NodeRegistry nodeRegistry = new NodeRegistryImpl(this);

	@Getter
	private final TreeContext treeContext;

	private TreePath getPath(Tree tree) {
		return getTrees().getPath(treeContext.getCompilationUnitTree(), tree);
	}

	private Trees getTrees() {
		return Trees.instance(getTreeContext().getProcessingEnvironment());
	}

	@Nullable
	@Override
	public TypeElementTemplate findEnclosingTypeElement(Tree tree) {
		// FIXME: Fails if invalid, need to return null instead.
		return getProcessingContext().getTypeElementTemplate(treeContext.getScopeTypeElement());
	}

	@Nullable
	@Override
	public Entity findEntity(IdentifierTree identifierTree) {
		return treeContext.findEntity(EntitySelector.forVariableOrType(identifierTree));
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod(Tree scopeTree, MethodSimpleSignature methodSimpleSignature) {
		return getEnclosingTypeElement(scopeTree).getTypeChain()
				.map(typeElementTemplate -> typeElementTemplate.selectMethodElement(methodSimpleSignature))
				.filter(Objects::nonNull)
				.findFirst().orElse(null);
	}

	@Nullable
	@Override
	public Tree findParent(Tree tree) {
		TreePath parentPath = getPath(tree).getParentPath();
		return parentPath != null ? parentPath.getLeaf() : null;
	}

	@Nullable
	@Override
	public TypeHandle findType(Tree tree) {
		TypeMirror typeMirror = getTrees().getTypeMirror(getPath(tree));
		return typeMirror != null ? getProcessingContext().getTypeHandle(GenericContext.empty(), typeMirror) : null;
	}

	@Override
	public ProcessingContext getProcessingContext() {
		return treeContext.getProcessingContext();
	}
}
