package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.tools.javac.tree.JCTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContextUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionType;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

public class VariableTreeUtils {

	private static EntitySelector buildVariableTypeSelector(Tree tree) {
		EntitySelector result;
		if (tree instanceof IdentifierTree) {
			result = EntitySelector.forVariableType((IdentifierTree) tree);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(tree));
		}
		return result;
	}

	@Nullable
	private static TypeMirror resolveType(TreeContext treeContext, EntitySelectorChain selectorChain) {
		if (selectorChain.getContext().getType() != EntitySelectionType.VARIABLE_TYPE) {
			throw new IllegalArgumentException("False selectorChain: " + selectorChain);
		}
		//
		Entity entity = TreeContextUtils.findEntity(treeContext, selectorChain);
		return entity != null
				? TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), entity.asElement())
				: null;
	}

	@Nullable
	private static TypeMirror resolveType(TreeContext treeContext, Tree variableTypeTree) {
		TypeMirror result;
		if (variableTypeTree instanceof IdentifierTree) {
			result = resolveType(treeContext, buildVariableTypeSelector(variableTypeTree).asChain());
		} else if (variableTypeTree instanceof MemberSelectTree) {
			result = resolveType(treeContext, EntitySelectorChain.forVariableType((MemberSelectTree) variableTypeTree));
		} else if (variableTypeTree instanceof ParameterizedTypeTree) {
			result = ParameterizedTypeTreeUtils.resolveType(treeContext, (ParameterizedTypeTree) variableTypeTree);
		} else if (variableTypeTree instanceof PrimitiveTypeTree) {
			result = PrimitiveTypeTreeUtils.resolveType(treeContext.getProcessingEnvironment(),
					(PrimitiveTypeTree) variableTypeTree);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(variableTypeTree));
		}
		return result;
	}

	@Nullable
	public static VariableElement findElement(VariableTree variableTree) {
		return ((JCTree.JCVariableDecl) variableTree).sym;
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, VariableTree variableTree) {
		TypeMirror result;
		{
			VariableElement variableElement = findElement(variableTree);
			if (variableElement != null) {
				result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), variableElement);
			} else {
				result = resolveType(treeContext, variableTree.getType());
			}
		}
		return result;
	}

	public static VariableElement getElement(VariableTree variableTree) {
		return Objects.requireNonNull(findElement(variableTree), "variableTree = " + variableTree);
	}

	public static boolean isField(NodeContext nodeContext, VariableTree variableTree) {
		return nodeContext.getParent(variableTree) instanceof ClassTree;
	}

	public static VariableTree requireField(NodeContext nodeContext, VariableTree variableTree) {
		if (!isField(nodeContext, variableTree)) {
			throw new IllegalArgumentException("Require field: " + variableTree);
		}
		return variableTree;
	}

	@Deprecated
	@Nullable
	public static TypeMirror resolveType(TreeContext treeContext, VariableTree variableTree) {
		TypeMirror result;
		{
			VariableElement variableElement = findElement(variableTree);
			if (variableElement != null) {
				result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), variableElement);
			} else {
				result = resolveType(treeContext, variableTree.getType());
			}
		}
		return result;
	}
}
