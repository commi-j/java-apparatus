package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.VariableTree;
import com.sun.tools.javac.tree.JCTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.model.ThisEntity;
import tk.labyrinth.apparatus.sourcetree.util.ClassTreeUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import java.util.List;

public class ClassTreeRegistry extends TreeRegistryBase {

	private final ClassTree classTree;

	private final TypeElement typeElement;

	public ClassTreeRegistry(TreeRegistry parent, ClassTree classTree) {
		super(parent);
		this.classTree = classTree;
		this.typeElement = ((JCTree.JCClassDecl) classTree).sym;
	}

	private Entity doFindEnclosedType(String simpleName) {
		Entity result;
		{
			ClassTree classTreeMember = classTree.getMembers().stream()
					.filter(ClassTree.class::isInstance)
					.map(ClassTree.class::cast)
					.filter(member -> member.getSimpleName().contentEquals(simpleName))
					.collect(CollectorUtils.findOnly(true));
			if (classTreeMember != null) {
				result = Entity.ofElement(ClassTreeUtils.getTypeElement(classTreeMember));
			} else {
				result = null;
			}
		}
		return result;
	}

	private Entity doFindField(String simpleName) {
		Entity result;
		{
			VariableTree variableTreeMember = classTree.getMembers().stream()
					.filter(VariableTree.class::isInstance)
					.map(VariableTree.class::cast)
					.filter(member -> member.getName().contentEquals(simpleName))
					.collect(CollectorUtils.findOnly(true));
			if (variableTreeMember != null) {
				result = Entity.ofVariableTree(variableTreeMember);
			} else {
				// TODO: inherited & enclosed fields
				VariableElement variableElement = TypeElementUtils.findDeclaredField(typeElement, simpleName);
				result = variableElement != null ? Entity.ofElement(variableElement) : null;
			}
		}
		return result;
	}

	private Entity doFindMethod(EntitySelector selector) {
		Entity result;
		{
			MethodTree methodTreeMember = ClassTreeUtils.getHierarchy(getProcessingEnvironment(), classTree)
					.map(ClassTree::getMembers)
					.flatMap(List::stream)
					.filter(MethodTree.class::isInstance)
					.map(MethodTree.class::cast)
					.filter(member -> member.getName().contentEquals(selector.getSimpleName()))
					// TODO: Check parameters.
					.collect(CollectorUtils.findOnly(true));
			if (methodTreeMember != null) {
				result = Entity.ofMethodTree(methodTreeMember);
			} else {
				// TODO: inherited & enclosed methods
				//VariableElement variableElement = TypeElementUtils.findDeclaredField(typeElement, simpleName);
				//result = variableElement != null ? Entity.ofVariableElement(variableElement) : null;
				throw new NotImplementedException(ExceptionUtils.render(selector));
			}
		}
		return result;
	}

	@Nullable
	@Override
	protected Entity doFindEntity(EntitySelector selector) {
		Entity result;
		if (selector.getSimpleName().equals("this")) {
			// Special case of 'this'.
			// TODO: Check if the scope supports 'this' context.
			result = Entity.ofThisEntity(ThisEntity.of(typeElement));
		} else if (selector.canBeMethod()) {
			// When we are looking for method we are not looking for anything else.
			result = doFindMethod(selector);
		} else {
			// Variable or enclosed type or this type.
			// TODO: Probably all types that does not require import, i.e. from the same package.
			Entity variableTreeEntity;
			//
			if (selector.canBeVariable()) {
				variableTreeEntity = doFindField(selector.getSimpleName());
			} else {
				variableTreeEntity = null;
			}
			//
			if (variableTreeEntity != null) {
				result = variableTreeEntity;
			} else if (selector.canBeType()) {
				// TODO: Enclosed types
				result = doFindEnclosedType(selector.getSimpleName());
			} else {
				result = null;
			}
		}
		return result;
	}
//	@Nullable
//	@Override
//	protected VariableTree doGetVariable(Name name) {
//		// TODO: inherited fields
//		return classTree.getMembers().stream()
//				.filter(VariableTree.class::isInstance)
//				.map(VariableTree.class::cast)
//				.filter(variableTree -> Objects.equals(variableTree.getName(), name))
//				.findFirst().orElse(null);
//	}

	@Nullable
	@Override
	public ClassTree findScopeClassTree() {
		return classTree;
	}

	@Override
	public TypeElement findScopeTypeElement() {
		return typeElement;
	}
}
