package tk.labyrinth.apparatus.sourcetree.scope;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.util.VariableTreeUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.UnreachableStateException;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;

public class TreeContextUtils {

	@Nullable
	public static Entity findEntity(TreeContext treeContext, EntitySelectorChain selectorChain) {
		Entity result;
		{
			Pair<EntitySelector, EntitySelectorChain> headAndTail = selectorChain.split();
			EntitySelector head = headAndTail.getLeft();
			EntitySelectorChain tail = headAndTail.getRight();
			//
			Entity headEntity = treeContext.findEntity(head);
			if (headEntity != null) {
				if (tail != null) {
					Element headElement;
					if (headEntity.isElement()) {
						headElement = headEntity.asElement();
					} else if (headEntity.isVariableTree()) {
						headElement = VariableTreeUtils.findElement(headEntity.asVariableTree());
					} else {
						throw new UnreachableStateException(ExceptionUtils.render(selectorChain));
					}
					if (headElement != null) {
						Element tailElement = ElementUtils.navigate(treeContext.getProcessingEnvironment(), headElement, tail);
						result = tailElement != null ? Entity.ofElement(tailElement) : null;
					} else {
						result = null;
					}
				} else {
					result = headEntity;
				}
			} else {
				if (tail != null) {
					Element element = treeContext.findElement(selectorChain);
					result = element != null ? Entity.ofElement(element) : null;
				} else {
					result = null;
				}
			}
		}
		return result;
	}
}
