package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.VariableTree;
import com.sun.tools.javac.tree.JCTree;

import javax.annotation.Nullable;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;

public class MethodTreeUtils {

	@Nullable
	public static VariableTree findDeclaration(MethodTree methodTree, Name name) {
		//
		return null;
	}

	public static ExecutableElement findMethod() {
		return null;
	}

	// TODO: Make complex lookup (class/package/hierarchy)
	public static boolean hasAnnotation(MethodTree methodTree, String annotationName) {
		return methodTree.getModifiers().getAnnotations().stream()
				.map(AnnotationTree::getAnnotationType)
				.map(JCTree.JCIdent.class::cast)
				.anyMatch(jcIdent -> jcIdent.sym.getQualifiedName().contentEquals(annotationName));
	}

	public static boolean isNotStatic(MethodTree methodTree) {
		return !isStatic(methodTree);
	}

	public static boolean isStatic(MethodTree methodTree) {
		return methodTree.getModifiers().getFlags().contains(Modifier.STATIC);
	}

	public static ExecutableElement resolveExecutableElement(MethodTree methodTree) {
		return ((JCTree.JCMethodDecl) methodTree).sym;
	}
}
