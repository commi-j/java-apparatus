package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;
import tk.labyrinth.misc4j2.exception.UnreachableStateException;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

public class TreeUtils {

	@Nullable
	public static Element findElement(TreeContext treeContext, Tree tree) {
		Element result;
		if (tree instanceof MethodInvocationTree) {
			result = MethodInvocationTreeUtils.findMethod(treeContext, (MethodInvocationTree) tree);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(tree));
		}
		return result;
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, Tree tree) {
		TypeMirror result;
		if (tree instanceof ExpressionTree) {
			result = ExpressionTreeUtils.findTypeMirror(treeContext, null, (ExpressionTree) tree);
		} else if (tree instanceof VariableTree) {
			result = VariableTreeUtils.findTypeMirror(treeContext, (VariableTree) tree);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(tree));
		}
		return result;
	}

	public static TypeMirror resolveType(TreeContext treeRegistry, Tree tree) {
		TypeMirror result;
		if (tree instanceof ExpressionTree) {
			result = ExpressionTreeUtils.resolveType(treeRegistry, (ExpressionTree) tree);
		} else {
			throw new UnreachableStateException(ExceptionUtils.render(tree));
		}
		return result;
	}
}
