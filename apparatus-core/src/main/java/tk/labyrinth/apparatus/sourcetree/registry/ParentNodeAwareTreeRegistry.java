package tk.labyrinth.apparatus.sourcetree.registry;

import lombok.Getter;
import tk.labyrinth.apparatus.model.node.Node;

public class ParentNodeAwareTreeRegistry extends TreeRegistryBase {

	@Getter
	private final Node parentNode;

	public ParentNodeAwareTreeRegistry(TreeRegistry parent, Node parentNode) {
		super(parent);
		this.parentNode = parentNode;
	}
}
