package tk.labyrinth.apparatus.sourcetree.registry;

import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import java.util.Objects;

public class ChildEntitySelectionTreeRegistry extends TreeRegistryBase {

	TreeRegistry parent;

	public ChildEntitySelectionTreeRegistry(TreeRegistry parent) {
		super(Objects.requireNonNull(parent, "parent"));
		this.parent = parent;
	}

	@Override
	public EntitySelectionContext getEntitySelectionContext() {
		return parent.getEntitySelectionContext().getParent();
	}
}
