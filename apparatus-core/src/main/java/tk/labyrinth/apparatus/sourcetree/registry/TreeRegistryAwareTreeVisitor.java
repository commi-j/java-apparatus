package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.PackageTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ProvidesTree;
import com.sun.source.tree.RequiresTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreeScanner;
import tk.labyrinth.apparatus.model.node.AssignmentNode;
import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.IdentifierNode;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.model.node.MethodNode;
import tk.labyrinth.apparatus.model.node.PackageNode;
import tk.labyrinth.apparatus.model.node.TypeCastNode;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.apparatus.sourcetree.scope.BackCompatabilityNodeContext;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.MethodInvocationTreeUtils;

import javax.annotation.Nullable;

public abstract class TreeRegistryAwareTreeVisitor<R> extends TreeScanner<R, TreeRegistry> {

	@Nullable
	protected R onAssignment(AssignmentNode node) {
		return null;
	}

	@Nullable
	protected R onBlock(BlockNode node) {
		return null;
	}

	@Nullable
	protected R onClass(ClassNode node) {
		return null;
	}

	@Nullable
	protected R onIdentifier(IdentifierNode node) {
		return null;
	}

	@Nullable
	protected R onMemberReference(MemberReferenceNode node) {
		return null;
	}

	@Nullable
	protected R onMemberSelect(MemberSelectNode node) {
		return null;
	}

	@Nullable
	protected R onMethod(MethodNode node) {
		return null;
	}

	@Nullable
	protected R onMethodInvocation(MethodInvocationNode node) {
		return null;
	}

	@Nullable
	protected R onPackage(PackageNode node) {
		return null;
	}

	@Nullable
	protected R onTypeCast(TypeCastNode node) {
		return null;
	}

	@Nullable
	protected R onVariable(VariableNode node) {
		return null;
	}

	protected abstract R reduceDirect(R first, R second);

	protected abstract R reduceReverted(R first, R second);

	protected R scanAndReduce(Tree node, TreeRegistry p, R r) {
		// Copy of private super method.
		return reduce(scan(node, p), r);
	}

	protected R scanAndReduce(Iterable<? extends Tree> nodes, TreeRegistry p, R r) {
		// Copy of private super method.
		return reduce(scan(nodes, p), r);
	}

	@Override
	public final R reduce(R r1, R r2) {
		return reduceReverted(r1, r2);
	}

	@Override
	public R visitAssignment(AssignmentTree node, TreeRegistry treeRegistry) {
		NodeContext nodeContext = new BackCompatabilityNodeContext(treeRegistry);
		AssignmentNode newNode = nodeContext.getNodeRegistry().getAssignment(node);
		TreeRegistry newTreeRegistry = new ParentNodeAwareTreeRegistry(treeRegistry, newNode.asNode());
		//
		return reduceDirect(onAssignment(newNode),
				super.visitAssignment(node, newTreeRegistry));
	}

	@Override
	public R visitBlock(BlockTree node, TreeRegistry treeRegistry) {
		TreeRegistry newTreeRegistry = new BlockTreeRegistry(treeRegistry, node);
		NodeContext nodeContext = new BackCompatabilityNodeContext(newTreeRegistry);
		return reduceDirect(onBlock(nodeContext.getNodeRegistry().getBlock(node)),
				super.visitBlock(node, newTreeRegistry));
	}

	@Override
	public R visitClass(ClassTree node, TreeRegistry treeRegistry) {
		TreeRegistry newTreeRegistry = new ClassTreeRegistry(treeRegistry, node);
		NodeContext nodeContext = new BackCompatabilityNodeContext(newTreeRegistry);
		return reduceDirect(onClass(nodeContext.getNodeRegistry().getClass(node)),
				super.visitClass(node, newTreeRegistry));
	}

	@Override
	public R visitCompilationUnit(CompilationUnitTree node, TreeRegistry treeRegistry) {
		return super.visitCompilationUnit(node, new CompilationUnitTreeRegistry(treeRegistry, node));
	}

	@Override
	public R visitCompoundAssignment(CompoundAssignmentTree node, TreeRegistry treeRegistry) {
		return super.visitCompoundAssignment(node, treeRegistry);
	}

	@Override
	public R visitIdentifier(IdentifierTree node, TreeRegistry treeRegistry) {
		TreeRegistry newTreeRegistry = TreeRegistryFactory.forIdentifier(treeRegistry);
		NodeContext nodeContext = new BackCompatabilityNodeContext(newTreeRegistry);
		return reduceDirect(onIdentifier(nodeContext.getNodeRegistry().getIdentifier(node)),
				super.visitIdentifier(node, newTreeRegistry));
	}

	@Override
	public R visitImport(ImportTree node, TreeRegistry treeRegistry) {
		TreeRegistry newTreeRegistry = new ImportTreeRegistry(treeRegistry);
		return super.visitImport(node, newTreeRegistry);
	}

	@Override
	public R visitLambdaExpression(LambdaExpressionTree node, TreeRegistry treeRegistry) {
		return super.visitLambdaExpression(node, new LambdaExpressionTreeRegistry(treeRegistry, node));
	}

	@Override
	public R visitMemberReference(MemberReferenceTree node, TreeRegistry treeRegistry) {
		NodeContext nodeContext = new BackCompatabilityNodeContext(treeRegistry);
		return reduceDirect(onMemberReference(nodeContext.getNodeRegistry().getMemberReference(treeRegistry.getParentNode(), node)),
				super.visitMemberReference(node, treeRegistry));
	}

	@Override
	public final R visitMemberSelect(MemberSelectTree node, TreeRegistry treeRegistry) {
		TreeRegistry newTreeRegistry = new MemberSelectTreeRegistry(treeRegistry);
		NodeContext nodeContext = new BackCompatabilityNodeContext(newTreeRegistry);
		return reduceDirect(onMemberSelect(nodeContext.getNodeRegistry().getMemberSelect(node)),
				super.visitMemberSelect(node, newTreeRegistry));
	}

	@Override
	public R visitMethod(MethodTree node, TreeRegistry treeRegistry) {
		TreeRegistry newTreeRegistry = new MethodTreeRegistry(treeRegistry, node);
		NodeContext nodeContext = new BackCompatabilityNodeContext(newTreeRegistry);
		return reduceDirect(onMethod(nodeContext.getNodeRegistry().getMethod(node)),
				super.visitMethod(node, newTreeRegistry));
	}

	@Override
	public final R visitMethodInvocation(MethodInvocationTree node, TreeRegistry treeRegistry) {
		NodeContext nodeContext = new BackCompatabilityNodeContext(treeRegistry);
		MethodInvocationNode newNode = nodeContext.getNodeRegistry().getMethodInvocation(node);
		TreeRegistry newTreeRegistry = new ParentNodeAwareTreeRegistry(treeRegistry, newNode.asNode());
		//
		R result = onMethodInvocation(newNode);
		result = scanAndReduce(node.getTypeArguments(), newTreeRegistry, result);
		{
			// Ignoring MethodInvocationTree#getMethodSelect() and visiting its child instead.
			// This is done to simplify presentation - when you see foo.bar() you expect
			//   MethodInvocation bar() on Identifier foo
			// and not
			//   MethodInvocation bar() on MemberSelect foo.bar on Identifier foo
			ExpressionTree targetExpressionTree = MethodInvocationTreeUtils.findTargetExpressionTree(newTreeRegistry, node);
			if (targetExpressionTree != null) {
				result = scanAndReduce(targetExpressionTree, newTreeRegistry, result);
			}
		}
		{
//			AtomicReference<R> resultReference = new AtomicReference<>(result);
//			IterableUtils.forEachWithIndex(node.getArguments(), (index, argument) -> {
//				resultReference.set(scanAndReduce(argument, newTreeRegistry, resultReference.get()));
//			});
			result = scanAndReduce(node.getArguments(), newTreeRegistry, result);
		}
		return result;
	}

	@Override
	public R visitPackage(PackageTree node, TreeRegistry treeRegistry) {
		TreeRegistry newTreeRegistry = new PackageTreeRegistry(treeRegistry);
		NodeContext nodeContext = new BackCompatabilityNodeContext(newTreeRegistry);
		return reduceDirect(onPackage(nodeContext.getNodeRegistry().getPackage(node)),
				super.visitPackage(node, newTreeRegistry));
	}

	@Override
	public R visitPrimitiveType(PrimitiveTypeTree node, TreeRegistry treeRegistry) {
		return super.visitPrimitiveType(node, treeRegistry);
	}

	@Override
	public R visitProvides(ProvidesTree node, TreeRegistry treeRegistry) {
		return super.visitProvides(node, treeRegistry);
	}

	@Override
	public R visitRequires(RequiresTree node, TreeRegistry treeRegistry) {
		return super.visitRequires(node, treeRegistry);
	}

	@Override
	public R visitTypeCast(TypeCastTree node, TreeRegistry treeRegistry) {
		NodeContext nodeContext = new BackCompatabilityNodeContext(treeRegistry);
		TypeCastNode newNode = nodeContext.getNodeRegistry().getTypeCast(node);
		TreeRegistry newTreeRegistry = new ParentNodeAwareTreeRegistry(treeRegistry, newNode.asNode());
		//
		return reduceDirect(onTypeCast(newNode),
				super.visitTypeCast(node, newTreeRegistry));
	}

	@Override
	public R visitVariable(VariableTree node, TreeRegistry treeRegistry) {
		NodeContext nodeContext = new BackCompatabilityNodeContext(treeRegistry);
		VariableNode newNode = nodeContext.getNodeRegistry().getVariable(node);
		TreeRegistry newTreeRegistry = new ParentNodeAwareTreeRegistry(treeRegistry, newNode.asNode());
		//
		R result = onVariable(newNode);
		result = scanAndReduce(node.getModifiers(), newTreeRegistry, result);
		result = scanAndReduce(node.getType(), newTreeRegistry, result);
		result = scanAndReduce(node.getNameExpression(), newTreeRegistry, result);
		result = scanAndReduce(node.getInitializer(), newTreeRegistry, result);
		return result;
	}
}
