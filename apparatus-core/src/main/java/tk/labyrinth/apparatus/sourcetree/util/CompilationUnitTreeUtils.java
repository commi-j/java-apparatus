package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import java.util.Objects;
import java.util.stream.Stream;

public class CompilationUnitTreeUtils {

	public static Stream<CompilationUnitTree> streamFrom(AnnotationProcessingRound round) {
		return streamFrom(round.getProcessingEnvironment(), round.getRoundEnvironment());
	}

	public static Stream<CompilationUnitTree> streamFrom(ProcessingEnvironment processingEnvironment, RoundEnvironment roundEnvironment) {
		Trees trees = Trees.instance(processingEnvironment);
		return roundEnvironment.getRootElements().stream()
				.map(trees::getPath)
				.filter(Objects::nonNull)
				.map(TreePath::getCompilationUnit);
	}
}
