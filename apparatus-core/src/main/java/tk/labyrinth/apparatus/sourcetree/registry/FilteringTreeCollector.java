package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreeScanner;
import tk.labyrinth.apparatus.misc4j.java.util.collectoin.StreamUtils;

import java.util.stream.Stream;

@SuppressWarnings("unchecked")
public class FilteringTreeCollector<T extends Tree> extends TreeScanner<Stream<T>, Class<T>> {

	@Override
	public Stream<T> visitAssignment(AssignmentTree node, Class<T> filterType) {
		return filterType.isInstance(node)
				? StreamUtils.concatNullable(Stream.of((T) node), super.visitAssignment(node, filterType))
				: super.visitAssignment(node, filterType);
	}
}
