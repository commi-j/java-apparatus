package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.VariableTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;

public class BlockTreeRegistry extends TreeRegistryBase {

	private final BlockTree blockTree;

	public BlockTreeRegistry(TreeRegistry parent, BlockTree blockTree) {
		super(parent);
		this.blockTree = blockTree;
	}

	@Nullable
	@Override
	protected Entity doFindEntity(EntitySelector selector) {
		Entity result;
		if (selector.canBeVariable()) {
			VariableTree variableTree = blockTree.getStatements().stream()
					.filter(VariableTree.class::isInstance)
					.map(VariableTree.class::cast)
					.filter(internalVariableTree -> internalVariableTree.getName().contentEquals(selector.getSimpleName()))
					.collect(CollectorUtils.findOnly(true));
			result = variableTree != null ? Entity.ofVariableTree(variableTree) : null;
		} else {
			result = null;
		}
		return result;
	}

	@Override
	public boolean hasThis() {
		boolean result;
		{
			TreeRegistry parent = getParent();
			if (parent == null) {
				throw new IllegalStateException("Can not detect staticity without parent: " + this);
			}
			if (getParent() instanceof ClassTreeRegistry) {
				result = blockTree.isStatic();
			} else {
				result = getParent().hasThis();
			}
		}
		return result;
	}
}
