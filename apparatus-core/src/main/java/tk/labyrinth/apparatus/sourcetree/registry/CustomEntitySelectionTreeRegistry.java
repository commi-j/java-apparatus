package tk.labyrinth.apparatus.sourcetree.registry;

import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

import java.util.Objects;

public class CustomEntitySelectionTreeRegistry extends TreeRegistryBase {

	private final EntitySelectionContext entitySelectionContext;

	public CustomEntitySelectionTreeRegistry(TreeRegistry parent, EntitySelectionContext entitySelectionContext) {
		super(parent);
		this.entitySelectionContext = Objects.requireNonNull(entitySelectionContext, "entitySelectionContext");
	}

	@Override
	public EntitySelectionContext getEntitySelectionContext() {
		return entitySelectionContext;
	}
}
