package tk.labyrinth.apparatus.sourcetree.scope;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

public interface TreeContext {

	@Nullable
	Element findElement(EntitySelectorChain selectorChain);

	@Nullable
	Entity findEntity(EntitySelector selector);

	@Nullable
	ClassTree findScopeClassTree();

	@Nullable
	TypeElement findScopeTypeElement();

	CompilationUnitTree getCompilationUnitTree();

	default Node getParentNode() {
		throw new NotImplementedException();
	}

	ProcessingContext getProcessingContext();

	@Deprecated
	default ProcessingEnvironment getProcessingEnvironment() {
		return getProcessingContext().getProcessingEnvironment();
	}

	default TypeElement getScopeTypeElement() {
		TypeElement result = findScopeTypeElement();
		if (result == null) {
			throw new IllegalArgumentException("No ScopeElement: " + this);
		}
		return result;
	}

	/**
	 * https://docs.oracle.com/javase/specs/jls/se8/html/jls-15.html#jls-15.8.3
	 *
	 * @return true if <b>this</b> is available in this context
	 */
	boolean hasThis();
}
