package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.MethodTree;
import com.sun.source.tree.VariableTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.util.MethodTreeUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.misc4j2.collectoin.CollectorUtils;

import javax.annotation.Nullable;

public class MethodTreeRegistry extends TreeRegistryBase {

	private final MethodTree methodTree;

	public MethodTreeRegistry(TreeRegistry parent, MethodTree methodTree) {
		super(parent);
		this.methodTree = methodTree;
	}

	@Nullable
	@Override
	protected Entity doFindEntity(EntitySelector selector) {
		Entity result;
		if (selector.canBeVariable()) {
			VariableTree variableTree = methodTree.getParameters().stream()
					.filter(parameter -> parameter.getName().contentEquals(selector.getSimpleName()))
					.collect(CollectorUtils.findOnly(true));
			result = variableTree != null ? Entity.ofVariableTree(variableTree) : null;
		} else {
			result = null;
		}
		return result;
	}

	@Override
	public boolean hasThis() {
		return MethodTreeUtils.isStatic(methodTree);
	}
}
