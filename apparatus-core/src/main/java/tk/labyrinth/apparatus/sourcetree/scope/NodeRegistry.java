package tk.labyrinth.apparatus.sourcetree.scope;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.PackageTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.VariableTree;
import tk.labyrinth.apparatus.model.node.AssignmentNode;
import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.CompilationUnitNode;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.FieldNode;
import tk.labyrinth.apparatus.model.node.IdentifierNode;
import tk.labyrinth.apparatus.model.node.LiteralNode;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.model.node.MethodNode;
import tk.labyrinth.apparatus.model.node.NewClassNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.PackageNode;
import tk.labyrinth.apparatus.model.node.TypeCastNode;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

public interface NodeRegistry {

	AssignmentNode getAssignment(AssignmentTree assignmentTree);

	BlockNode getBlock(BlockTree blockTree);

	ClassNode getClass(ClassTree classTree);

	CompilationUnitNode getCompilationUnit(CompilationUnitTree compilationUnitTree);

	default ExpressionNode getExpression(ExpressionTree expressionTree) {
		ExpressionNode result;
		if (expressionTree instanceof IdentifierTree) {
			result = getIdentifier((IdentifierTree) expressionTree).asExpression();
		} else if (expressionTree instanceof LiteralTree) {
			result = getLiteral((LiteralTree) expressionTree).asExpression();
		} else if (expressionTree instanceof MemberSelectTree) {
			result = getMemberSelect((MemberSelectTree) expressionTree).asExpression();
		} else if (expressionTree instanceof MethodInvocationTree) {
			result = getMethodInvocation((MethodInvocationTree) expressionTree).asExpression();
		} else if (expressionTree instanceof NewClassTree) {
			result = getNewClass((NewClassTree) expressionTree).asExpression();
		} else {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		}
		return result;
	}

	FieldNode getField(VariableTree variableTree);

	IdentifierNode getIdentifier(IdentifierTree identifierTree);

	LiteralNode getLiteral(LiteralTree literalTree);

	MemberReferenceNode getMemberReference(Node parentNode, MemberReferenceTree memberReferenceTree);

	MemberSelectNode getMemberSelect(MemberSelectTree memberSelectTree);

	MethodNode getMethod(MethodTree methodTree);

	MethodInvocationNode getMethodInvocation(MethodInvocationTree methodInvocationTree);

	NewClassNode getNewClass(NewClassTree newClassTree);

	default Node getNode(Tree tree) {
		Node result;
		if (tree instanceof BlockTree) {
			result = getBlock((BlockTree) tree).asNode();
		} else if (tree instanceof ClassTree) {
			result = getClass((ClassTree) tree).asNode();
		} else if (tree instanceof CompilationUnitTree) {
			result = getCompilationUnit((CompilationUnitTree) tree).asNode();
		} else if (tree instanceof ExpressionTree) {
			result = getExpression((ExpressionTree) tree).asNode();
		} else if (tree instanceof MethodTree) {
			result = getMethod((MethodTree) tree).asNode();
		} else if (tree instanceof PackageTree) {
			result = getPackage((PackageTree) tree).asNode();
		} else if (tree instanceof VariableTree) {
			result = getVariable((VariableTree) tree).asNode();
		} else {
			throw new NotImplementedException(ExceptionUtils.render(tree));
		}
		return result;
	}

	PackageNode getPackage(PackageTree packageTree);

	TypeCastNode getTypeCast(TypeCastTree typeCastTree);

	VariableNode getVariable(VariableTree variableTree);
}
