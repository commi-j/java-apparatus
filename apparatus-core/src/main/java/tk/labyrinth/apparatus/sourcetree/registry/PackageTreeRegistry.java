package tk.labyrinth.apparatus.sourcetree.registry;

import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

public class PackageTreeRegistry extends TreeRegistryBase {

	public PackageTreeRegistry(TreeRegistry parent) {
		super(parent);
	}

	@Override
	public EntitySelectionContext getEntitySelectionContext() {
		return EntitySelectionContext.forPackageDeclaration();
	}
}
