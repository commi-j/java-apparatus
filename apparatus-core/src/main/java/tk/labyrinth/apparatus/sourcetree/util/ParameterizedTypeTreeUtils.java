package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ParameterizedTypeTree;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public class ParameterizedTypeTreeUtils {

	public static TypeMirror resolveType(TreeContext treeRegistry, ParameterizedTypeTree parameterizedTypeTree) {
		TypeMirror result;
		{
			// FIXME: There may be an option of obtaining TypeElement, but we downgrade it to Mirror
			//  and them Upgrade again. Need to fix with direct usage of Element.
			TypeMirror type = TreeUtils.resolveType(treeRegistry, parameterizedTypeTree.getType());
			TypeElement element = TypeElementUtils.get(treeRegistry.getProcessingEnvironment(), type);
			// FIXME: Replace with DeclaredTypeUtils.resolve (need to create it before).
			result = treeRegistry.getProcessingEnvironment().getTypeUtils().getDeclaredType(
					element,
					parameterizedTypeTree.getTypeArguments().stream()
							.map(typeArgument -> TreeUtils.resolveType(treeRegistry, typeArgument))
							.toArray(TypeMirror[]::new));
		}
		return result;
	}
}
