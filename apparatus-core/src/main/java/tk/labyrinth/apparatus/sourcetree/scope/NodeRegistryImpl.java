package tk.labyrinth.apparatus.sourcetree.scope;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.PackageTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.VariableTree;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.AssignmentNode;
import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.CompilationUnitNode;
import tk.labyrinth.apparatus.model.node.FieldNode;
import tk.labyrinth.apparatus.model.node.IdentifierNode;
import tk.labyrinth.apparatus.model.node.LiteralNode;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.model.node.MethodNode;
import tk.labyrinth.apparatus.model.node.NewClassNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.PackageNode;
import tk.labyrinth.apparatus.model.node.TypeCastNode;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.apparatus.model.node.impl.AssignmentNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.BlockNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.ClassNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.CompilationUnitNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.FieldNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.IdentifierNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.LiteralNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.MemberReferenceNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.MemberSelectNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.MethodInvocationNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.MethodNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.NewClassNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.PackageNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.TypeCastNodeImpl;
import tk.labyrinth.apparatus.model.node.impl.VariableNodeImpl;
import tk.labyrinth.apparatus.sourcetree.util.VariableTreeUtils;

@RequiredArgsConstructor
public class NodeRegistryImpl implements NodeRegistry {

	private final NodeContext nodeContext;

	@Override
	public AssignmentNode getAssignment(AssignmentTree assignmentTree) {
		return new AssignmentNodeImpl(assignmentTree, nodeContext);
	}

	@Override
	public BlockNode getBlock(BlockTree blockTree) {
		return new BlockNodeImpl(blockTree, nodeContext);
	}

	@Override
	public ClassNode getClass(ClassTree classTree) {
		return new ClassNodeImpl(classTree, nodeContext);
	}

	@Override
	public CompilationUnitNode getCompilationUnit(CompilationUnitTree compilationUnitTree) {
		return new CompilationUnitNodeImpl(compilationUnitTree, nodeContext);
	}

	@Override
	public FieldNode getField(VariableTree variableTree) {
		return new FieldNodeImpl(nodeContext, VariableTreeUtils.requireField(nodeContext, variableTree));
	}

	@Override
	public IdentifierNode getIdentifier(IdentifierTree identifierTree) {
		return new IdentifierNodeImpl(identifierTree, nodeContext);
	}

	@Override
	public LiteralNode getLiteral(LiteralTree literalTree) {
		return new LiteralNodeImpl(literalTree, nodeContext);
	}

	@Override
	public MemberReferenceNode getMemberReference(Node parentNode, MemberReferenceTree memberReferenceTree) {
		return new MemberReferenceNodeImpl(memberReferenceTree, nodeContext, parentNode);
	}

	@Override
	public MemberSelectNode getMemberSelect(MemberSelectTree memberSelectTree) {
		return new MemberSelectNodeImpl(memberSelectTree, nodeContext);
	}

	@Override
	public MethodNode getMethod(MethodTree methodTree) {
		return new MethodNodeImpl(methodTree, nodeContext);
	}

	@Override
	public MethodInvocationNode getMethodInvocation(MethodInvocationTree methodInvocationTree) {
		return new MethodInvocationNodeImpl(methodInvocationTree, nodeContext);
	}

	@Override
	public NewClassNode getNewClass(NewClassTree newClassTree) {
		return new NewClassNodeImpl(newClassTree, nodeContext);
	}

	@Override
	public PackageNode getPackage(PackageTree packageTree) {
		return new PackageNodeImpl(nodeContext, packageTree);
	}

	@Override
	public TypeCastNode getTypeCast(TypeCastTree typeCastTree) {
		return new TypeCastNodeImpl(nodeContext, typeCastTree);
	}

	@Override
	public VariableNode getVariable(VariableTree variableTree) {
		VariableNode result;
		if (VariableTreeUtils.isField(nodeContext, variableTree)) {
			result = getField(variableTree).asVariableNode();
		} else {
			result = new VariableNodeImpl(nodeContext, variableTree);
		}
		return result;
	}
}
