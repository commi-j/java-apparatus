package tk.labyrinth.apparatus.sourcetree.scope;

import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.Tree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;

public interface NodeContext {

	@Nullable
	TypeElementTemplate findEnclosingTypeElement(Tree tree);

	@Nullable
	Entity findEntity(IdentifierTree identifierTree);

	/**
	 * Look for method with provided <b>ArgumentSignature</b> in this and nesting classes.
	 *
	 * @param scopeTree             non-null
	 * @param methodSimpleSignature non-null
	 *
	 * @return nullable
	 */
	@Nullable
	MethodElementTemplate findMethod(Tree scopeTree, MethodSimpleSignature methodSimpleSignature);

	@Nullable
	Tree findParent(Tree tree);

	@Nullable
	TypeHandle findType(Tree tree);

	default TypeElementTemplate getEnclosingTypeElement(Tree tree) {
		TypeElementTemplate result = findEnclosingTypeElement(tree);
		if (result == null) {
			throw new IllegalArgumentException("Not found: tree = " + tree);
		}
		return result;
	}

	NodeRegistry getNodeRegistry();

	default Tree getParent(Tree tree) {
		Tree result = findParent(tree);
		if (result == null) {
			throw new IllegalArgumentException("Not found: tree = " + tree);
		}
		return result;
	}

	ProcessingContext getProcessingContext();

	@Deprecated
	TreeContext getTreeContext();

	interface Provider {

		NodeContext getNodeContext();
	}
}
