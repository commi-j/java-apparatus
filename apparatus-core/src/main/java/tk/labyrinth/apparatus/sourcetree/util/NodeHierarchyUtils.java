package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class NodeHierarchyUtils {

	private static Stream<ClassTree> getClassNodeHierarchy(Supplier<List<? extends Tree>> childrenSupplier) {
		return childrenSupplier.get().stream()
				.filter(ClassTree.class::isInstance)
				.map(ClassTree.class::cast)
				.flatMap(classNode -> Stream.concat(Stream.of(classNode), getClassNodeHierarchy(classNode)));
	}

	public static Stream<ClassTree> getClassNodeHierarchy(ClassTree classNode) {
		return getClassNodeHierarchy(classNode::getMembers);
	}

	public static Stream<ClassTree> getClassNodeHierarchy(CompilationUnitTree compilationUnitNode) {
		return getClassNodeHierarchy(compilationUnitNode::getTypeDecls);
	}
}
