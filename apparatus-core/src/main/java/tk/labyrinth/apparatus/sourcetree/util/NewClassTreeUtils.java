package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.NewClassTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;

public class NewClassTreeUtils {

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, NewClassTree newClassTree) {
		TypeMirror result;
		if (newClassTree.getClassBody() == null) {
			result = ExpressionTreeUtils.findTypeMirror(treeContext, EntitySelectionContext.forType(), newClassTree.getIdentifier());
		} else {
			throw new NotImplementedException(ExceptionUtils.render(newClassTree));
		}
		return result;
	}

	public static Entity getEntity(NewClassTree newClassTree) {
		return Entity.ofExpressionTree(newClassTree);
	}

	@Deprecated
	public static TypeMirror resolveType(TreeContext treeContext, NewClassTree newClassTree) {
		TypeMirror result;
		if (newClassTree.getClassBody() == null) {
			result = ExpressionTreeUtils.resolveType(treeContext, newClassTree.getIdentifier());
		} else {
			throw new NotImplementedException(ExceptionUtils.render(newClassTree));
		}
		return result;
	}
}
