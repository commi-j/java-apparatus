package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.LambdaExpressionTree;

public class LambdaExpressionTreeRegistry extends TreeRegistryBase {

	private final LambdaExpressionTree lambdaExpressionTree;

	public LambdaExpressionTreeRegistry(TreeRegistry parent, LambdaExpressionTree lambdaExpressionTree) {
		super(parent);
		this.lambdaExpressionTree = lambdaExpressionTree;
	}
}
