package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.template.element.ElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
public abstract class TreeRegistryBase implements TreeRegistry {

	private final Map<EntitySelector, Optional<ElementTemplate>> elementCache = new HashMap<>();

	private final Map<EntitySelectorChain, Optional<Element>> elementCache2 = new HashMap<>();

	private final Map<EntitySelector, Optional<Entity>> entityCache = new HashMap<>();

	@Getter(AccessLevel.PROTECTED)
	@Nullable
	private final TreeRegistry parent;

	private final Map<Name, Optional<TypeMirror>> typeCache = new HashMap<>();

	@Nullable
	protected Element doFindElement(EntitySelectorChain selectorChain) {
		return null;
	}

	@Nullable
	protected ElementTemplate doFindElement(EntitySelector selector) {
		return null;
	}

	@Nullable
	protected Entity doFindEntity(EntitySelector selector) {
		return null;
	}

	@Nullable
	protected TypeMirror doGetType(Name name) {
		return null;
	}

	@Nullable
	@Override
	public Element findElement(EntitySelectorChain selectorChain) {
		// TODO: Decide whether we want to cache parent's computations.
		return elementCache2.computeIfAbsent(selectorChain, key -> {
			Element result;
			{
				Element ownResult = doFindElement(key);
				if (ownResult != null) {
					result = ownResult;
				} else if (parent != null) {
					result = parent.findElement(key);
				} else {
					result = null;
				}
			}
			return Optional.ofNullable(result);
		}).orElse(null);
	}

	@Nullable
	@Override
	public ElementTemplate findElement(EntitySelector selector) {
		// TODO: Decide whether we want to cache parent's computations.
		return elementCache.computeIfAbsent(selector, key -> {
			ElementTemplate result;
			{
				ElementTemplate ownResult = doFindElement(key);
				if (ownResult != null) {
					result = ownResult;
				} else if (parent != null) {
					result = parent.findElement(key);
				} else {
					result = null;
				}
			}
			return Optional.ofNullable(result);
		}).orElse(null);
	}

	@Nullable
	@Override
	public Entity findEntity(EntitySelector selector) {
		// TODO: Decide whether we want to cache parent's computations.
		return entityCache.computeIfAbsent(selector, key -> {
			Entity result;
			{
				Entity ownResult = doFindEntity(key);
				if (ownResult != null) {
					result = ownResult;
				} else if (parent != null) {
					result = parent.findEntity(key);
				} else {
					result = null;
				}
			}
			return Optional.ofNullable(result);
		}).orElse(null);
	}

	@Nullable
	@Override
	public ClassTree findScopeClassTree() {
		return parent != null ? parent.findScopeClassTree() : null;
	}

	@Nullable
	@Override
	public TypeElement findScopeTypeElement() {
		return parent != null ? parent.findScopeTypeElement() : null;
	}

	@Override
	public CompilationUnitTree getCompilationUnitTree() {
		return parent.getCompilationUnitTree();
	}

	@Override
	public EntitySelectionContext getEntitySelectionContext() {
		return EntitySelectionContext.forUndefined();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		Objects.requireNonNull(parent, "parent");
		return parent.getProcessingContext();
	}

	@Nullable
	@Override
	public TypeMirror getType(Name name) {
		// TODO: Decide whether we want to cache parent's computations.
		return typeCache.computeIfAbsent(name, key -> {
			TypeMirror result;
			{
				TypeMirror ownResult = doGetType(key);
				if (ownResult != null) {
					result = ownResult;
				} else if (parent != null) {
					result = parent.getType(key);
				} else {
					result = null;
				}
			}
			return Optional.ofNullable(result);
		}).orElse(null);
	}

	@Override
	public boolean hasThis() {
		if (parent == null) {
			throw new IllegalStateException("Can not detect staticity without parent: " + this);
		}
		return parent.hasThis();
	}
}
