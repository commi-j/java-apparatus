package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.PrimitiveTypeTree;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;

public class PrimitiveTypeTreeUtils {

	public static TypeMirror resolveType(ProcessingEnvironment processingEnvironment, PrimitiveTypeTree primitiveTypeTree) {
		return processingEnvironment.getTypeUtils().getPrimitiveType(primitiveTypeTree.getPrimitiveTypeKind());
	}
}
