package tk.labyrinth.apparatus.sourcetree.scope;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.ExceptionUtils;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

@RequiredArgsConstructor
public class TreeAwareNodeContext implements NodeContext {

	private final CompilationUnitTree compilationUnitTree;

	@Getter
	private final NodeRegistry nodeRegistry = new NodeRegistryImpl(this);

	@Getter
	private final ProcessingContext processingContext;

	private final Trees trees;

	private TreePath getPath(Tree tree) {
		return trees.getPath(compilationUnitTree, tree);
	}

	@Nullable
	@Override
	public TypeElementTemplate findEnclosingTypeElement(Tree tree) {
		TreePath path = getPath(tree);
		// FIXME
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public Entity findEntity(IdentifierTree identifierTree) {
		Entity result = null;
		{
			String name = identifierTree.getName().toString();
			TreePath path = getPath(identifierTree);
			while ((path = path.getParentPath()) != null) {
				// TODO
			}
		}
		return result;
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod(Tree scopeTree, MethodSimpleSignature methodSimpleSignature) {
		return getEnclosingTypeElement(scopeTree).getTypeChain()
				.map(typeElementTemplate -> typeElementTemplate.selectMethodElement(methodSimpleSignature))
				.filter(Objects::nonNull)
				.findFirst().orElse(null);
	}

	@Nullable
	@Override
	public Tree findParent(Tree tree) {
		TreePath parentPath = getPath(tree).getParentPath();
		return parentPath != null ? parentPath.getLeaf() : null;
	}

	@Nullable
	@Override
	public TypeHandle findType(Tree tree) {
		TypeMirror typeMirror = trees.getTypeMirror(getPath(tree));
		return typeMirror != null ? processingContext.getTypeHandle(GenericContext.empty(), typeMirror) : null;
	}

	@Override
	public TreeContext getTreeContext() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}
}
