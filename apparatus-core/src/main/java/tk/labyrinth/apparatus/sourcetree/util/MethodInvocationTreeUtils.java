package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.model.ThisEntity;
import tk.labyrinth.apparatus.sourcetree.registry.TreeRegistry;
import tk.labyrinth.apparatus.sourcetree.registry.TreeRegistryFactory;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.util.ExecutableElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;
import tk.labyrinth.misc4j2.exception.UnreachableStateException;

import javax.annotation.Nullable;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.stream.Collectors;

public class MethodInvocationTreeUtils {

	public static EntitySelectionContext buildMethodInvocationSelectionContext(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		EntitySelectionContext result;
		{
			List<TypeMirror> argumentTypes = findArgumentTypes(treeContext, methodInvocationTree);
			if (!argumentTypes.contains(null)) {
				result = EntitySelectionContext.forMethodInvocation(argumentTypes.stream());
			} else {
				result = EntitySelectionContext.forUndefined();
			}
		}
		return result;
	}

	public static EntitySelectionContext buildMethodSelectionContext(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		EntitySelectionContext result;
		{
			List<TypeMirror> argumentTypes = findArgumentTypes(treeContext, methodInvocationTree);
			if (!argumentTypes.contains(null)) {
				result = EntitySelectionContext.forMethod(argumentTypes.stream());
			} else {
				result = EntitySelectionContext.forUndefined();
			}
		}
		return result;
	}

	@Nullable
	public static EntitySelector buildMethodSelector(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		EntitySelector result;
		{
			EntitySelectionContext selectionContext = buildMethodSelectionContext(treeContext, methodInvocationTree);
			// FIXME: Find out why it can not be method.
			result = selectionContext.canBeMethod()
					? EntitySelector.build(selectionContext, getSimpleName(methodInvocationTree))
					: null;
		}
		return result;
	}

	public static List<TypeMirror> findArgumentTypes(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		return methodInvocationTree.getArguments().stream()
				.map(ExpressionTree.class::cast)
				.map(argument -> {
					TreeRegistry treeRegistry = TreeRegistryFactory.forMethodInvocationArgument(treeContext, argument);
					return ExpressionTreeUtils.findTypeMirror(treeRegistry, treeRegistry.getEntitySelectionContext(), argument);
				})
				.collect(Collectors.toList());
	}

	@Nullable
	public static ExecutableElement findMethod(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		ExecutableElement result;
		{
			EntitySelector selector = buildMethodSelector(treeContext, methodInvocationTree);
			if (selector != null) {
				if (hasExplicitTarget(methodInvocationTree)) {
					Entity targetEntity = findTargetEntity(treeContext, methodInvocationTree);
					if (targetEntity != null) {
						TypeMirror targetTypeMirror = EntityUtils.findTypeMirror(treeContext, targetEntity);
						if (targetTypeMirror != null) {
							result = (ExecutableElement) TypeElementUtils.selectMember(treeContext.getProcessingEnvironment(),
									TypeElementUtils.get(treeContext.getProcessingEnvironment(), targetTypeMirror), selector);
						} else {
							result = null;
						}
					} else {
						result = null;
					}
				} else {
					Entity entity = treeContext.findEntity(selector);
					if (entity != null) {
						result = EntityUtils.resolveExecutableElement(entity);
					} else {
						result = null;
					}
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static TypeMirror findReturnType(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		ExecutableElement method = findMethod(treeContext, methodInvocationTree);
		return method != null
				? ExecutableElementUtils.getReturnTypeMirrorErasure(treeContext.getProcessingEnvironment(), method)
				: null;
	}

	@Nullable
	public static Entity findTargetEntity(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		Entity result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				ExecutableElement method = findMethod(treeContext, methodInvocationTree);
				if (method != null) {
					TypeElement scopeElement = treeContext.getScopeTypeElement();
					if (ExecutableElementUtils.isStatic(method)) {
						result = Entity.ofElement(scopeElement);
					} else {
						result = Entity.ofThisEntity(ThisEntity.of(scopeElement));
					}
				} else {
					result = null;
				}
			} else if (methodSelect instanceof MemberSelectTree) {
				result = ExpressionTreeUtils.findEntity(treeContext, EntitySelectionContext.forMethodInvocationTarget(),
						((MemberSelectTree) methodSelect).getExpression());
			} else {
				throw new NotImplementedException(ExceptionUtils.render(methodSelect));
			}
		}
		return result;
	}

	@Nullable
	public static ExpressionTree findTargetExpressionTree(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		ExpressionTree result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				result = null;
			} else if (methodSelect instanceof MemberSelectTree) {
				result = ((MemberSelectTree) methodSelect).getExpression();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(methodSelect));
			}
		}
		return result;
	}

	public static Entity getEntity(MethodInvocationTree methodInvocationTree) {
		return Entity.ofExpressionTree(methodInvocationTree);
	}

	public static String getSimpleName(MethodInvocationTree methodInvocationTree) {
		String result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				result = ((IdentifierTree) methodSelect).getName().toString();
			} else if (methodSelect instanceof MemberSelectTree) {
				result = ((MemberSelectTree) methodSelect).getIdentifier().toString();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(methodInvocationTree));
			}
		}
		return result;
	}

	public static Entity getTargetEntity(TreeContext treeContext, MethodInvocationTree methodInvocationTree) {
		Entity result = findTargetEntity(treeContext, methodInvocationTree);
		if (result == null) {
			throw new IllegalArgumentException("No TargetEntity found: methodInvocationTree = " + methodInvocationTree);
		}
		return result;
	}

	public static boolean hasExplicitTarget(MethodInvocationTree methodInvocationTree) {
		return !(methodInvocationTree.getMethodSelect() instanceof IdentifierTree);
	}

	public static TypeMirror resolveTargetType(TreeRegistry treeRegistry, MethodInvocationTree methodInvocationTree) {
		TypeMirror result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				result = TypeMirrorUtils.erasure(treeRegistry.getProcessingEnvironment(),
						treeRegistry.getScopeTypeElement());
			} else if (methodSelect instanceof MemberSelectTree) {
				ExpressionTree expression = ((MemberSelectTree) methodSelect).getExpression();
				result = ExpressionTreeUtils.resolveType(TreeRegistryFactory.build(expression,
						TreeRegistryFactory.forMethodInvocationTarget(treeRegistry)), expression);
			} else {
				throw new UnreachableStateException(ExceptionUtils.render(methodSelect));
			}
		}
		return result;
	}
}
