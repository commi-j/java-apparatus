package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.sourcetree.scope.BackCompatabilityNodeContext;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

public class TreeRegistryFactory {

	private static TreeRegistry custom(TreeRegistry parent, EntitySelectionContext selectionContext) {
		return new CustomEntitySelectionTreeRegistry(parent, selectionContext);
	}

	public static TreeRegistry build(Tree tree, TreeRegistry parent) {
		TreeRegistry result;
		switch (tree.getKind()) {
			case IDENTIFIER:
			case MEMBER_SELECT:
				result = forMemberSelect(parent);
				break;
			default:
				throw new NotImplementedException(ExceptionUtils.render(tree));
		}
		return result;
	}

	public static TreeRegistry forIdentifier(TreeRegistry parent) {
		return new ChildEntitySelectionTreeRegistry(parent);
	}

	public static TreeRegistry forMemberSelect(TreeRegistry parent) {
		return new ChildEntitySelectionTreeRegistry(parent);
	}

	public static TreeRegistry forMethodInvocation(TreeRegistry parent, MethodInvocationTree methodInvocationTree) {
		NodeContext nodeContext = new BackCompatabilityNodeContext(parent);
		MethodInvocationNode newNode = nodeContext.getNodeRegistry().getMethodInvocation(methodInvocationTree);
		return new ParentNodeAwareTreeRegistry(parent, newNode.asNode());
	}

	public static TreeRegistry forMethodInvocationArgument(TreeContext parent, ExpressionTree expressionTree) {
		TreeContext result;
		switch (expressionTree.getKind()) {
			case BOOLEAN_LITERAL:
			case CHAR_LITERAL:
			case DOUBLE_LITERAL:
			case FLOAT_LITERAL:
			case INT_LITERAL:
			case LONG_LITERAL:
			case NULL_LITERAL:
			case STRING_LITERAL:
				result = new CustomEntitySelectionTreeRegistry((TreeRegistry) parent, EntitySelectionContext.forVariable());
				break;
			case IDENTIFIER:
			case MEMBER_SELECT:
				result = custom((TreeRegistry) parent, EntitySelectionContext.forVariable());
				break;
			case METHOD_INVOCATION:
				result = forMethodInvocation((TreeRegistry) parent, (MethodInvocationTree) expressionTree);
				break;
			default:
				throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		}
		return (TreeRegistry) result;
	}

	public static TreeRegistry forMethodInvocationTarget(TreeRegistry parent) {
		return new CustomEntitySelectionTreeRegistry(parent, EntitySelectionContext.forMethod().getParent());
	}

	public static TreeRegistry forUndefined(TreeContext parent) {
		return new CustomEntitySelectionTreeRegistry((TreeRegistry) parent, EntitySelectionContext.forUndefined());
	}
}
