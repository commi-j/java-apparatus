package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MemberSelectTree;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.apparatus.model.java.Keywords;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.registry.ChildEntitySelectionTreeRegistry;
import tk.labyrinth.apparatus.sourcetree.registry.MemberSelectTreeRegistry;
import tk.labyrinth.apparatus.sourcetree.registry.TreeRegistryFactory;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContextUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;
import tk.labyrinth.jaap.util.ElementUtils;
import tk.labyrinth.jaap.util.TypeElementUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.UnreachableStateException;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;

public class MemberSelectTreeUtils extends tk.labyrinth.jaap.trees.util.MemberSelectTreeUtils {

	@Nullable
	public static Element findElement(TreeContext treeContext, EntitySelectionContext selectionContext, MemberSelectTree memberSelectTree) {
		Pair<ExpressionTree, List<String>> targetAndSimpleNames = deconstruct(memberSelectTree);
		ExpressionTree targetTree = targetAndSimpleNames.getLeft();
		List<String> simpleNames = targetAndSimpleNames.getRight();
		//
		Element result;
		if (targetTree != null) {
			TypeMirror targetTypeMirror = ExpressionTreeUtils.findTypeMirror(TreeRegistryFactory.forUndefined(treeContext), targetTree);
			if (targetTypeMirror != null) {
				TypeElement targetTypeElement = TypeElementUtils.find(treeContext.getProcessingEnvironment(), targetTypeMirror);
				if (targetTypeElement != null) {
					result = ElementUtils.navigate(treeContext.getProcessingEnvironment(), targetTypeElement,
							EntitySelectorChain.build(selectionContext, simpleNames.stream()));
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		} else {
			result = findElement(treeContext, EntitySelectorChain.build(selectionContext, simpleNames.stream()));
		}
		return result;
	}

	@Nullable
	public static Element findElement(TreeContext treeContext, EntitySelectorChain selectorChain) {
		Element result;
		{
			Entity entity = TreeContextUtils.findEntity(treeContext, selectorChain);
			if (entity != null) {
				if (entity.isElement()) {
					result = entity.asElement();
				} else if (entity.isVariableTree()) {
					result = VariableTreeUtils.findElement(entity.asVariableTree());
				} else {
					throw new UnreachableStateException(ExceptionUtils.render(selectorChain));
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static Entity findEntity(TreeContext treeContext, EntitySelectionContext selectionContext, MemberSelectTree memberSelectTree) {
		return findEntity(treeContext, EntitySelectorChain.build(selectionContext, memberSelectTree));
	}

	@Nullable
	public static Entity findEntity(TreeContext treeContext, EntitySelectorChain selectorChain) {
		return TreeContextUtils.findEntity(treeContext, selectorChain);
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, EntitySelectionContext selectionContext, MemberSelectTree memberSelectTree) {
		TypeMirror result;
		{
			Element element = findElement(treeContext, selectionContext, memberSelectTree);
			if (element != null) {
				result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), element);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static TypeMirror resolveType(TreeContext treeContext, MemberSelectTree memberSelectTree) {
		if (!(treeContext instanceof MemberSelectTreeRegistry) && !(treeContext instanceof ChildEntitySelectionTreeRegistry)) {
			throw new IllegalArgumentException(ExceptionUtils.renderList(List.of(treeContext, memberSelectTree)));
		}
		//
		TypeMirror result;
		{
			Name name = memberSelectTree.getIdentifier();
			if (name.contentEquals(Keywords.CLASS)) {
				result = TypeMirrorUtils.resolve(treeContext.getProcessingEnvironment(), Class.class);
			} else {
				Entity entity = TreeContextUtils.findEntity(
						treeContext,
						// TODO: EntitySelectorChain#forVariableOrType.
						EntitySelectorChain.build(EntitySelectionContext.forVariableOrType(), memberSelectTree));
				if (entity != null) {
					if (entity.isElement()) {
						result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), entity.asElement());
					} else if (entity.isVariableTree()) {
						result = VariableTreeUtils.findTypeMirror(treeContext, entity.asVariableTree());
					} else {
						throw new UnsupportedOperationException(ExceptionUtils.render(memberSelectTree));
					}
				} else {
					result = null;
				}
			}
		}
		return result;
	}
}
