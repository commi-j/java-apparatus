package tk.labyrinth.apparatus.sourcetree.registry;

import tk.labyrinth.apparatus.model.node.AssignmentNode;
import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.IdentifierNode;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.model.node.MethodNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.PackageNode;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AdvancedNodeVisitor<R> {

	private final TreeVisitor treeVisitor = new TreeVisitor();

	// TODO: Preserve registration and invocation order for non-leaf visitors.
	private final Map<Class<?>, List<Function<?, Stream<R>>>> visitors = new HashMap<>();

	public TreeVisitor getTreeVisitor() {
		return treeVisitor;
	}

	public AdvancedNodeVisitor<R> visitBlock(Function<BlockNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(BlockNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitClass(Function<ClassNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(ClassNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitExpression(Function<ExpressionNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(ExpressionNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitIdentifier(Function<IdentifierNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(IdentifierNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitMemberReference(Function<MemberReferenceNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(MemberReferenceNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitMemberSelect(Function<MemberSelectNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(MemberSelectNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitMethod(Function<MethodNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(MethodNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitMethodInvocation(Function<MethodInvocationNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(MethodInvocationNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitNode(Function<Node, Stream<R>> visitor) {
		visitors.computeIfAbsent(Node.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitPackage(Function<PackageNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(PackageNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	public AdvancedNodeVisitor<R> visitVariable(Function<VariableNode, Stream<R>> visitor) {
		visitors.computeIfAbsent(VariableNode.class, key -> new ArrayList<>()).add(visitor);
		return this;
	}

	private class TreeVisitor extends TreeRegistryAwareTreeVisitor<Stream<R>> {

		@SuppressWarnings("unchecked")
		private <N> Stream<R> applyVisitors(Class<N> nodeType, Object node) {
			N resolvedNode = resolveNode(nodeType, node);
			List<Function<N, Stream<R>>> typeVisitors = (List<Function<N, Stream<R>>>) (List<?>) visitors.getOrDefault(nodeType, List.of());
			return typeVisitors.stream()
					.map(typeVisitor -> typeVisitor.apply(resolvedNode))
					.flatMap(stream -> stream != null ? stream.collect(Collectors.toList()).stream() : Stream.empty());
		}

		private Stream<?> getNodeTypes(Class<?> nodeType) {
			return Stream.of(
					nodeType,
					ExpressionNode.Convertible.class.isAssignableFrom(nodeType) ? ExpressionNode.class : null,
					Node.class
			).filter(Objects::nonNull);
		}

		@SuppressWarnings("unchecked")
		private <N> N resolveNode(Class<N> nodeType, Object node) {
			N result;
			if (nodeType == ExpressionNode.class) {
				result = (N) ((ExpressionNode.Convertible) node).asExpression();
			} else if (nodeType == Node.class) {
				result = (N) ((Node.Convertible) node).asNode();
			} else {
				result = nodeType.cast(node);
			}
			return result;
		}

		private <N> Stream<R> visit(Class<N> nodeType, N node) {
			Stream<?> types = getNodeTypes(nodeType);
			List<R> collected = types.flatMap(type -> applyVisitors((Class<?>) type, node))
					.collect(Collectors.toList());
			return collected.stream();
		}

		@Nullable
		@Override
		protected Stream<R> onAssignment(AssignmentNode node) {
			return visit(AssignmentNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onBlock(BlockNode node) {
			return visit(BlockNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onClass(ClassNode node) {
			return visit(ClassNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onIdentifier(IdentifierNode node) {
			return visit(IdentifierNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onMemberReference(MemberReferenceNode node) {
			return visit(MemberReferenceNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onMemberSelect(MemberSelectNode node) {
			return visit(MemberSelectNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onMethod(MethodNode node) {
			return visit(MethodNode.class, node);
		}

		@Override
		protected Stream<R> onMethodInvocation(MethodInvocationNode node) {
			return visit(MethodInvocationNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onPackage(PackageNode node) {
			return visit(PackageNode.class, node);
		}

		@Nullable
		@Override
		protected Stream<R> onVariable(VariableNode node) {
			return visit(VariableNode.class, node);
		}

		@Override
		protected Stream<R> reduceDirect(Stream<R> first, Stream<R> second) {
			return StreamUtils.concatNullable(first, second);
		}

		@Override
		protected Stream<R> reduceReverted(Stream<R> first, Stream<R> second) {
			return StreamUtils.concatNullable(second, first);
		}
	}
}
