package tk.labyrinth.apparatus.sourcetree.registry;

import lombok.Getter;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectorChain;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public class CompilationBundleTreeRegistry extends TreeRegistryBase {

	@Getter
	private final CompilationBundleCache cache;

	private final ProcessingContext processingContext;

	public CompilationBundleTreeRegistry(RoundContext roundContext) {
		super(null);
		this.cache = new CompilationBundleCache(roundContext);
		processingContext = roundContext.getProcessingContext();
	}

	@Nullable
	@Override
	protected Element doFindElement(EntitySelectorChain selectorChain) {
		Element result;
		{
			String qualifiedName = selectorChain.getLongName();
			//
			TypeElement typeElement;
			if (selectorChain.canBeType()) {
				typeElement = getProcessingEnvironment().getElementUtils().getTypeElement(qualifiedName);
			} else {
				typeElement = null;
			}
			//
			if (typeElement != null) {
				result = typeElement;
			} else if (selectorChain.canBePackage()) {
				result = getProcessingEnvironment().getElementUtils().getPackageElement(qualifiedName);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	protected TypeMirror doGetType(Name name) {
		return processingContext.getTypeElementTemplate(name.toString()).getTypeMirror();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		return processingContext;
	}
}
