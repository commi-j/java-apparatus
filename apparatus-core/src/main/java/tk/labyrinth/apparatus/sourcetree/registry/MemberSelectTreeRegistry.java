package tk.labyrinth.apparatus.sourcetree.registry;

import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

public class MemberSelectTreeRegistry extends TreeRegistryBase {

	private final TreeRegistry parent;

	public MemberSelectTreeRegistry(TreeRegistry parent) {
		super(parent);
		this.parent = parent;
	}

	@Override
	public EntitySelectionContext getEntitySelectionContext() {
		return parent.getEntitySelectionContext().getParent();
	}
}
