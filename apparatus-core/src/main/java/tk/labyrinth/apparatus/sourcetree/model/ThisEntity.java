package tk.labyrinth.apparatus.sourcetree.model;

import lombok.Value;

import javax.lang.model.element.TypeElement;

@Value(staticConstructor = "of")
public class ThisEntity {

	TypeElement typeElement;

	@Override
	public String toString() {
		return typeElement.toString() + ".this";
	}
}
