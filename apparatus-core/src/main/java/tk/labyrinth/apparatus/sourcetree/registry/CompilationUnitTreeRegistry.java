package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.tools.javac.tree.JCTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.registry.util.SymbolTableUtils;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.util.TypeElementUtils;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public class CompilationUnitTreeRegistry extends TreeRegistryBase {

	private final JCTree.JCCompilationUnit compilationUnit;

	private final CompilationBundleTreeRegistry treeRegistry;

	public CompilationUnitTreeRegistry(TreeRegistry parent, CompilationUnitTree compilationUnitTree) {
		super(parent);
		this.treeRegistry = (CompilationBundleTreeRegistry) parent;
		this.compilationUnit = (JCTree.JCCompilationUnit) compilationUnitTree;
	}

	private TypeElementTemplate doFindTypeElement(String simpleName) {
		TypeElementTemplate result;
		{
			TypeElement foundTypeElement;
			{
				Name simpleNameAsName = getProcessingContext().getName(simpleName);
				TypeElement lookupTypeElement = SymbolTableUtils.lookup(compilationUnit, simpleNameAsName);
				if (lookupTypeElement != null) {
					// FIXME: "java.lang" must goes after package lookup.
					foundTypeElement = lookupTypeElement;
				} else {
					String packageName = compilationUnit.getPackageName().toString();
					//
					foundTypeElement = TypeElementUtils.find(getProcessingEnvironment(),
							packageName + "$" + simpleName);
				}
			}
			result = foundTypeElement != null ? getProcessingContext().getTypeElementTemplate(foundTypeElement) : null;
		}
		return result;
	}

	@Nullable
	@Override
	protected ElementTemplate doFindElement(EntitySelector selector) {
		ElementTemplate result;
		{
			TypeElementTemplate typeElement;
			if (selector.canBeType()) {
				typeElement = doFindTypeElement(selector.getSimpleName());
			} else {
				typeElement = null;
			}
			if (typeElement != null) {
				result = typeElement;
			} else if (selector.canBePackage()) {
				result = getProcessingContext().getPackageElementTemplate(selector.getSimpleName(), true);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	protected Entity doFindEntity(EntitySelector selector) {
		Entity result;
		{
			Element element;
			{
				TypeElementTemplate typeElementTemplate;
				if (selector.canBeType()) {
					typeElementTemplate = doFindTypeElement(selector.getSimpleName());
				} else {
					typeElementTemplate = null;
				}
				if (typeElementTemplate != null) {
					element = typeElementTemplate.getTypeElement();
				} else if (selector.canBePackage()) {
					// FIXME: Will fail here as Entity does not support packages.
					element = getProcessingEnvironment().getElementUtils().getPackageElement(selector.getSimpleName());
				} else {
					element = null;
				}
			}
			result = element != null ? Entity.ofElement(element) : null;
		}
		return result;
	}

	@Nullable
	@Override
	protected TypeMirror doGetType(Name name) {
		TypeMirror result;
		{
			TypeElement lookupTypeElement = SymbolTableUtils.lookup(compilationUnit, name);
			if (lookupTypeElement != null) {
				// FIXME: "java.lang" must goes after package lookup.
				result = treeRegistry.getProcessingEnvironment().getTypeUtils().erasure(lookupTypeElement.asType());
			} else {
				result = treeRegistry.getCache().getType(compilationUnit.getPackageName().toString(), name);
			}
		}
		return result;
	}

	@Override
	public CompilationUnitTree getCompilationUnitTree() {
		return compilationUnit;
	}
}
