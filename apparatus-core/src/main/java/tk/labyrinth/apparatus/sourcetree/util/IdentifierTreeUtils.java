package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.VariableTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;
import tk.labyrinth.misc4j2.exception.UnreachableStateException;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;

/**
 * Identifies:<br>
 * - {@link ClassTree}<br>
 * Import - {@link MemberSelectTree}<br>
 * - {@link MethodTree}<br>
 * - {@link VariableTree}<br>
 */
public class IdentifierTreeUtils {

	@Nullable
	public static Entity findEntity(TreeContext treeContext, EntitySelectionContext selectionContext, IdentifierTree identifierTree) {
		return findEntity(treeContext, EntitySelector.build(selectionContext, identifierTree));
	}

	@Nullable
	public static Entity findEntity(TreeContext treeContext, EntitySelector selector) {
		return treeContext.findEntity(selector);
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, EntitySelectionContext selectionContext, IdentifierTree identifierTree) {
		return findTypeMirror(treeContext, EntitySelector.build(selectionContext, identifierTree));
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, EntitySelector selector) {
		TypeMirror result;
		{
			Entity entity = findEntity(treeContext, selector);
			if (entity != null) {
				if (entity.isElement()) {
					result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), entity.asElement());
				} else if (entity.isVariableTree()) {
					result = VariableTreeUtils.findTypeMirror(treeContext, entity.asVariableTree());
				} else {
					throw new NotImplementedException(ExceptionUtils.render(selector));
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static VariableTree findVariableTree(TreeContext treeContext, IdentifierTree identifierTree) {
		Entity entity = findEntity(treeContext, EntitySelector.forVariable(identifierTree));
		return entity != null && entity.isVariableTree() ? entity.asVariableTree() : null;
	}

	public static Entity getEntity(TreeContext treeContext, EntitySelector selector) {
		Entity result = findEntity(treeContext, selector);
		if (result == null) {
			throw new IllegalArgumentException("No Entity found: selector = " + selector);
		}
		return result;
	}

	@Nullable
	public static TypeMirror resolveType(TreeContext treeContext, EntitySelector selector) {
		TypeMirror result;
		{
			Entity entity = findEntity(treeContext, selector);
			if (entity != null) {
				if (entity.isElement()) {
					result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), entity.asElement());
				} else if (entity.isVariableTree()) {
					result = VariableTreeUtils.findTypeMirror(treeContext, entity.asVariableTree());
				} else {
					throw new UnreachableStateException(ExceptionUtils.render(selector));
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static TypeMirror resolveType(TreeContext treeContext, IdentifierTree identifierTree) {
		return resolveType(treeContext, EntitySelector.forVariableOrType(identifierTree));
	}
}
