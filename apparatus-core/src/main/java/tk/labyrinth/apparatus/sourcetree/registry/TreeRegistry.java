package tk.labyrinth.apparatus.sourcetree.registry;

import com.sun.source.tree.VariableTree;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.element.ElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;

/**
 * <a href="https://en.wikipedia.org/wiki/Symbol_table">Symbol Table<a> variant.
 */
public interface TreeRegistry extends TreeContext {

	// FIXME: Probably a target for rework as Identifier may point to local variables which do not have elements.
	@Deprecated
	@Nullable
	ElementTemplate findElement(EntitySelector selector);

	EntitySelectionContext getEntitySelectionContext();

	/**
	 * @param name non-null
	 *
	 * @return non-null
	 */
	@Deprecated
	TypeMirror getType(Name name);

	/**
	 * @param name non-null
	 *
	 * @return non-null
	 */
	@Deprecated
	@Nullable
	default VariableTree getVariable(Name name) {
		return null;
	}
}
