package tk.labyrinth.apparatus.sourcetree.util;

import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public class EntityUtils {

	@Nullable
	public static Element findElement(TreeContext treeContext, Entity entity) {
		Element result;
		if (entity.isElement()) {
			result = entity.asElement();
		} else if (entity.isThisEntity()) {
			result = entity.asThisEntity().getTypeElement();
		} else if (entity.isTree()) {
			result = TreeUtils.findElement(treeContext, entity.asTree());
		} else {
			throw new NotImplementedException(ExceptionUtils.render(entity));
		}
		return result;
	}

	@Nullable
	public static TypeElement findTypeElement(TreeContext treeContext, Entity entity) {
		TypeElement result;
		if (entity.isElement()) {
//			TypeMirror typeMirror = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), entity.asElement());
//			if(TypeElementUtils.find(treeContext.getProcessingEnvironment(),)){
//
//			}
//			else{
//
//			}
//			result = TypeElementUtils.entity.asElement();
			throw new NotImplementedException();
		} else if (entity.isThisEntity()) {
			result = entity.asThisEntity().getTypeElement();
		} else if (entity.isTree()) {
//			result = TreeUtils.findElement(treeContext, entity.asTree());
			throw new NotImplementedException();
		} else {
			throw new NotImplementedException(ExceptionUtils.render(entity));
		}
		return result;
	}

	@Nullable
	public static TypeHandle findTypeHandle() {
		return null;
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, Entity entity) {
		TypeMirror result;
		if (entity.isElement()) {
			result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), entity.asElement());
		} else if (entity.isThisEntity()) {
			result = TypeMirrorUtils.erasure(treeContext.getProcessingEnvironment(), entity.asThisEntity().getTypeElement());
		} else if (entity.isTree()) {
			result = TreeUtils.findTypeMirror(treeContext, entity.asTree());
		} else {
			throw new NotImplementedException(ExceptionUtils.render(entity));
		}
		return result;
	}

	public static TypeMirror getTypeMirror(TreeContext treeContext, Entity entity) {
		TypeMirror result = findTypeMirror(treeContext, entity);
		if (result == null) {
			throw new IllegalArgumentException("Not found: entity = " + entity);
		}
		return result;
	}

	public static ExecutableElement resolveExecutableElement(Entity entity) {
		ExecutableElement result;
		if (entity.isExecutableElement()) {
			result = (ExecutableElement) entity.asElement();
		} else if (entity.isMethodTree()) {
			result = MethodTreeUtils.resolveExecutableElement(entity.asMethodTree());
		} else {
			throw new UnsupportedOperationException(ExceptionUtils.render(entity));
		}
		return result;
	}
}
