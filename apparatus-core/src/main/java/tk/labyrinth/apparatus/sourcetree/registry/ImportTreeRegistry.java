package tk.labyrinth.apparatus.sourcetree.registry;

import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;

public class ImportTreeRegistry extends TreeRegistryBase {

	public ImportTreeRegistry(TreeRegistry parent) {
		super(parent);
	}

	@Override
	public EntitySelectionContext getEntitySelectionContext() {
		return EntitySelectionContext.forImportDeclaration();
	}
}
