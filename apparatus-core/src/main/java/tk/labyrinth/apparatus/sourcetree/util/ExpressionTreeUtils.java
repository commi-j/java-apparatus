package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;
import tk.labyrinth.misc4j2.exception.UnreachableStateException;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;

public class ExpressionTreeUtils {

	private static TypeMirror resolveParenthesizedTreeType(TreeContext treeContext, ParenthesizedTree parenthesizedTree) {
		return resolveType(treeContext, parenthesizedTree.getExpression());
	}

	@Nullable
	public static EntitySelectionContext buildSelectionContext(TreeContext treeContext, ExpressionTree expressionTree) {
		EntitySelectionContext result;
		if (expressionTree instanceof LiteralTree) {
			result = null;
		} else if (expressionTree instanceof MethodInvocationTree) {
			result = MethodInvocationTreeUtils.buildMethodSelectionContext(treeContext, (MethodInvocationTree) expressionTree);
		} else if (expressionTree instanceof NewClassTree) {
			result = null;
		} else {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		}
		return result;
	}

	@Nullable
	public static Entity findEntity(TreeContext treeContext, EntitySelectionContext selectionContext, ExpressionTree expressionTree) {
		Entity result;
		if (expressionTree instanceof IdentifierTree) {
			result = IdentifierTreeUtils.findEntity(treeContext, selectionContext, (IdentifierTree) expressionTree);
		} else if (expressionTree instanceof LiteralTree) {
			result = LiteralTreeUtils.getEntity((LiteralTree) expressionTree);
		} else if (expressionTree instanceof MemberSelectTree) {
			result = MemberSelectTreeUtils.findEntity(treeContext, selectionContext, (MemberSelectTree) expressionTree);
		} else if (expressionTree instanceof MethodInvocationTree) {
			result = MethodInvocationTreeUtils.getEntity((MethodInvocationTree) expressionTree);
		} else if (expressionTree instanceof NewClassTree) {
			result = NewClassTreeUtils.getEntity((NewClassTree) expressionTree);
		} else if (expressionTree instanceof ParameterizedTypeTree) {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		} else if (expressionTree instanceof ParenthesizedTree) {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		} else {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		}
		return result;
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, EntitySelectionContext selectionContext, ExpressionTree expressionTree) {
		TypeMirror result;
		if (expressionTree instanceof IdentifierTree) {
			result = IdentifierTreeUtils.findTypeMirror(treeContext, selectionContext, (IdentifierTree) expressionTree);
		} else if (expressionTree instanceof LiteralTree) {
			result = LiteralTreeUtils.getTypeMirror(treeContext, (LiteralTree) expressionTree);
		} else if (expressionTree instanceof MemberSelectTree) {
			result = MemberSelectTreeUtils.findTypeMirror(treeContext, selectionContext, (MemberSelectTree) expressionTree);
		} else if (expressionTree instanceof MethodInvocationTree) {
			result = MethodInvocationTreeUtils.findReturnType(treeContext, (MethodInvocationTree) expressionTree);
		} else if (expressionTree instanceof NewClassTree) {
			result = NewClassTreeUtils.findTypeMirror(treeContext, (NewClassTree) expressionTree);
		} else if (expressionTree instanceof ParameterizedTypeTree) {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		} else if (expressionTree instanceof ParenthesizedTree) {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		} else {
			throw new NotImplementedException(ExceptionUtils.render(expressionTree));
		}
		return result;
	}

	@Nullable
	public static TypeMirror findTypeMirror(TreeContext treeContext, ExpressionTree expressionTree) {
		if (expressionTree instanceof IdentifierTree || expressionTree instanceof MemberSelectTree) {
			throw new IllegalArgumentException("Require SelectionContext to resolve this expression: " + expressionTree);
		}
		//
		return findTypeMirror(treeContext, buildSelectionContext(treeContext, expressionTree), expressionTree);
	}

	public static TypeMirror resolveType(TreeContext treeContext, ExpressionTree expressionTree) {
		TypeMirror result;
		if (expressionTree instanceof IdentifierTree) {
			result = IdentifierTreeUtils.resolveType(treeContext, (IdentifierTree) expressionTree);
		} else if (expressionTree instanceof LiteralTree) {
			result = LiteralTreeUtils.getTypeMirror(treeContext, (LiteralTree) expressionTree);
		} else if (expressionTree instanceof MemberSelectTree) {
			result = MemberSelectTreeUtils.resolveType(treeContext, (MemberSelectTree) expressionTree);
		} else if (expressionTree instanceof MethodInvocationTree) {
			result = MethodInvocationTreeUtils.findReturnType(treeContext, (MethodInvocationTree) expressionTree);
		} else if (expressionTree instanceof NewClassTree) {
			result = NewClassTreeUtils.resolveType(treeContext, (NewClassTree) expressionTree);
		} else if (expressionTree instanceof ParameterizedTypeTree) {
			result = ParameterizedTypeTreeUtils.resolveType(treeContext, (ParameterizedTypeTree) expressionTree);
		} else if (expressionTree instanceof ParenthesizedTree) {
			result = resolveParenthesizedTreeType(treeContext, (ParenthesizedTree) expressionTree);
		} else {
			throw new UnreachableStateException(ExceptionUtils.render(expressionTree));
		}
		return result;
	}
}
