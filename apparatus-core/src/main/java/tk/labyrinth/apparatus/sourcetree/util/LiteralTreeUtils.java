package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.LiteralTree;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.TreeContext;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

public class LiteralTreeUtils {

	public static Entity getEntity(LiteralTree literalTree) {
		return Entity.ofExpressionTree(literalTree);
	}

	public static TypeMirror getTypeMirror(TreeContext treeContext, LiteralTree literalTree) {
		TypeMirror result;
		{
			Object value = literalTree.getValue();
			if (value == null) {
				result = treeContext.getProcessingEnvironment().getTypeUtils().getNullType();
			} else if (value instanceof Boolean) {
				result = treeContext.getProcessingEnvironment().getTypeUtils().getPrimitiveType(TypeKind.BOOLEAN);
			} else if (value instanceof Integer) {
				result = treeContext.getProcessingEnvironment().getTypeUtils().getPrimitiveType(TypeKind.INT);
			} else if (value instanceof Long) {
				result = treeContext.getProcessingEnvironment().getTypeUtils().getPrimitiveType(TypeKind.LONG);
			} else if (value instanceof String) {
				result = treeContext.getProcessingEnvironment().getElementUtils().getTypeElement(
						String.class.getCanonicalName()).asType();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(literalTree));
			}
		}
		return result;
	}
}
