package tk.labyrinth.apparatus.sourcetree.util;

import com.sun.source.tree.ClassTree;
import com.sun.source.util.Trees;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.JCTree;
import tk.labyrinth.jaap.util.ElementUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ClassTreeUtils {

	public static Stream<ClassTree> getHierarchy(ProcessingEnvironment processingEnvironment, ClassTree classTree) {
		List<ClassTree> result = new ArrayList<>();
		result.add(classTree);
		{
			Trees trees = Trees.instance(processingEnvironment);
			Element parent = getTypeElement(classTree);
			while (ElementUtils.isType(parent = parent.getEnclosingElement())) {
				result.add(trees.getTree((TypeElement) parent));
			}
		}
		return result.stream();
	}

	public static TypeElement getTypeElement(ClassTree classTree) {
		return ((JCTree.JCClassDecl) classTree).sym;
	}

	public static boolean isInner(ClassTree classTree) {
		return ((JCTree.JCClassDecl) classTree).sym.isInner();
	}

	public static boolean isNotInner(ClassTree classTree) {
		return !isInner(classTree);
	}

	public static boolean isStatic(ClassTree classTree) {
		return classTree.getModifiers().getFlags().contains(Modifier.STATIC);
	}

	public static boolean isTopLevel(ClassTree classTree) {
		Symbol.ClassSymbol classSymbol = ((JCTree.JCClassDecl) classTree).sym;
		return classSymbol.owner.getKind() == ElementKind.PACKAGE;
	}
}
