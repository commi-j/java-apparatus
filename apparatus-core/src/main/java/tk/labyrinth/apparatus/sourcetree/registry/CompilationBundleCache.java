package tk.labyrinth.apparatus.sourcetree.registry;

import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import javax.annotation.Nullable;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CompilationBundleCache {

	private final RoundContext context;

	private Map<String, Map<CanonicalTypeSignature, TypeElement>> typeElements = null;

	public CompilationBundleCache(RoundContext context) {
		this.context = context;
	}

	private void ensureElementsPopulated() {
		if (typeElements == null) {
			typeElements = new HashMap<>();
			context.getAllTypeElements().forEach(typeTemplate ->
					typeElements.computeIfAbsent(typeTemplate.getPackage().toString(), key -> new HashMap<>())
							.put(typeTemplate.getSignature(), typeTemplate.getTypeElement()));
		}
	}

	@Nullable
	public TypeElement getElement(String packageName, Name name) {
		TypeElement result;
		{
			ensureElementsPopulated();
			result = typeElements.getOrDefault(packageName, Collections.emptyMap()).get(CanonicalTypeSignature.ofValid(name.toString()));
		}
		return result;
	}

	@Nullable
	public TypeMirror getType(String packageName, Name name) {
		TypeMirror result;
		{
			ensureElementsPopulated();
			TypeElement element = typeElements.getOrDefault(packageName, Collections.emptyMap()).get(CanonicalTypeSignature.ofValid(name.toString()));
			result = element != null ? element.asType() : null;
		}
		return result;
	}
}
