package tk.labyrinth.apparatus.javac.util;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.List;

import java.util.stream.Stream;

public class TreeMakerUtils {

	public static JCTree.JCIdent createIdentifier(TreeMaker treeMaker, JavacElements javacElements, String identifier) {
		return treeMaker.Ident(javacElements.getName(identifier));
	}

	public static List<JCTree.JCVariableDecl> createLambdaVariables(TreeMaker treeMaker, JavacElements javacElements, Stream<String> parameterNames) {
		return parameterNames
				.map(parameterName -> treeMaker.VarDef(noModifiers(treeMaker), javacElements.getName(parameterName), null, null))
				.collect(List.collector());
	}

	/**
	 * Returns one of:<br>
	 * - {@link IdentifierTree} - foo;<br>
	 * - {@link MemberSelectTree} - foo.bar;<br>
	 *
	 * @param treeMaker     non-null
	 * @param javacElements non-null
	 * @param path          non-null
	 *
	 * @return non-null
	 */
	public static ExpressionTree createPathExpression(TreeMaker treeMaker, JavacElements javacElements, String path) {
		ExpressionTree result;
		{
			JCTree.JCExpression expressionTree = null;
			for (String name : path.split("\\.")) {
				if (expressionTree == null) {
					expressionTree = treeMaker.Ident(javacElements.getName(name));
				} else {
					expressionTree = treeMaker.Select(expressionTree, javacElements.getName(name));
				}
			}
			result = expressionTree;
		}
		return result;
	}

	public static JCTree.JCModifiers noModifiers(TreeMaker treeMaker) {
		return treeMaker.Modifiers(0);
	}
}
