package tk.labyrinth.apparatus.model.node.impl;

import tk.labyrinth.apparatus.model.node.VariableNode;

public abstract class VariableNodeSubstituteBase implements VariableNode {

	@Override
	public String toString() {
		return getVariableTree().toString();
	}
}
