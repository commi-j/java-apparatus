package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.IdentifierNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.EntityUtils;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;

@RequiredArgsConstructor
public class IdentifierNodeImpl implements IdentifierNode {

	@Getter
	private final IdentifierTree identifierTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Nullable
	@Override
	public Entity findEntity() {
		return nodeContext.findEntity(identifierTree);
	}

	@Nullable
	@Override
	public TypeHandle findType() {
		TypeHandle result;
		{
			Entity entity = findEntity();
			if (entity != null) {
				TypeMirror typeMirror = EntityUtils.findTypeMirror(nodeContext.getTreeContext(), entity);
				if (typeMirror != null) {
					result = nodeContext.getTreeContext().getProcessingContext().getTypeHandle(
							// TODO: Should it be context of this node or typeMirror is good enough?
							GenericContext.empty(),
							typeMirror);
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return identifierTree.toString();
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public IdentifierNode asIdentifier() {
			return IdentifierNodeImpl.this;
		}

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Nullable
		@Override
		public TypeHandle findType() {
			return IdentifierNodeImpl.this.findType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return identifierTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public boolean isIdentifier() {
			return true;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return identifierTree;
		}
	}
}
