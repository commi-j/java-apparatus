package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.ExceptionUtils;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.FieldNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.VariableTreeUtils;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.TypeTemplate;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Modifier;

@RequiredArgsConstructor
public class FieldNodeImpl implements FieldNode {

	@Getter
	private final NodeContext nodeContext;

	@Getter
	private final VariableTree variableTree;

	@Override
	public VariableNode asVariableNode() {
		return new VariableNodeSubstitute();
	}

	@Nullable
	@Override
	public ExpressionNode findInitializer() {
		ExpressionTree initializer = variableTree.getInitializer();
		return initializer != null ? nodeContext.getNodeRegistry().getExpression(initializer) : null;
	}

	@Override
	public String getName() {
		return variableTree.getName().toString();
	}

	@Override
	public TypeHandle getType() {
		return toElementTemplate().getType();
	}

	@Override
	public ExpressionNode getTypeNode() {
		ExpressionNode result;
		{
			Tree type = variableTree.getType();
			if (type instanceof ExpressionTree) {
				result = getNodeContext().getNodeRegistry().getExpression((ExpressionTree) type);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(type));
			}
		}
		return result;
	}

	@Override
	public boolean hasFinalModifier() {
		return variableTree.getModifiers().getFlags().contains(Modifier.FINAL);
	}

	@Override
	public boolean hasInitializer() {
		return variableTree.getInitializer() != null;
	}

	@Override
	public boolean hasStaticModifier() {
		return variableTree.getModifiers().getFlags().contains(Modifier.STATIC);
	}

	@Override
	public FieldElementTemplate toElementTemplate() {
		return nodeContext.getProcessingContext().getFieldElementTemplate(VariableTreeUtils.getElement(variableTree));
	}

	public class VariableNodeSubstitute extends VariableNodeSubstituteBase {

		@Override
		public FieldNode asFieldNode() {
			return FieldNodeImpl.this;
		}

		@Override
		public Node asNode() {
			return new FieldNodeImpl.NodeSubstitute();
		}

		@Override
		public TypeHandle findType() {
			return getType();
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public VariableTree getVariableTree() {
			return variableTree;
		}

		@Override
		public boolean isFieldNode() {
			return true;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return variableTree;
		}
	}
}
