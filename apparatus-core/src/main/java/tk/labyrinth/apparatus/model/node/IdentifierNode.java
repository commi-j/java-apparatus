package tk.labyrinth.apparatus.model.node;

import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.annotation.Nullable;

public interface IdentifierNode extends ExpressionNode.Convertible {

	@Nullable
	Entity findEntity();

	@Nullable
	TypeHandle findType();

	default TypeHandle getType() {
		TypeHandle result = findType();
		if (result == null) {
			throw new IllegalArgumentException("Not found: this = " + this);
		}
		return result;
	}
}
