package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.CompilationUnitNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;

import javax.annotation.Nullable;

@RequiredArgsConstructor
public class CompilationUnitNodeImpl implements CompilationUnitNode {

	@Getter
	private final CompilationUnitTree compilationUnitTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public Node asNode() {
		return new NodeSubstitute();
	}

	@Nullable
	@Override
	public String findPackageName() {
		ExpressionTree packageName = compilationUnitTree.getPackageName();
		return packageName != null ? packageName.toString() : null;
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return compilationUnitTree;
		}
	}
}
