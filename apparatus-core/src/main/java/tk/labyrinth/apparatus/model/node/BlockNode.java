package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.BlockTree;
import tk.labyrinth.apparatus.model.node.common.MayHaveStaticModifier;

public interface BlockNode extends
		MayHaveStaticModifier,
		Node.Convertible {

	BlockTree getBlockTree();
}
