package tk.labyrinth.apparatus.model.node;

import tk.labyrinth.jaap.handle.type.TypeHandle;

public interface NewClassNode extends ExpressionNode.Convertible {

	TypeHandle getType();
}
