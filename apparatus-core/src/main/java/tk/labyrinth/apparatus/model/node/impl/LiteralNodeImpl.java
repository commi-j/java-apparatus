package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.LiteralNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.LiteralTreeUtils;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;

@RequiredArgsConstructor
public class LiteralNodeImpl implements LiteralNode {

	@Getter
	private final LiteralTree literalTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Override
	public TypeHandle getType() {
		return nodeContext.getTreeContext().getProcessingContext().getTypeHandle(
				GenericContext.empty(),
				LiteralTreeUtils.getTypeMirror(nodeContext.getTreeContext(), literalTree));
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public LiteralNode asLiteral() {
			return LiteralNodeImpl.this;
		}

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Override
		public TypeHandle findType() {
			return getType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return literalTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public boolean isLiteral() {
			return true;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return literalTree;
		}
	}
}
