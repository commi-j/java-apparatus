package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.AssignmentNode;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.annotation.Nullable;

@RequiredArgsConstructor
public class AssignmentNodeImpl implements AssignmentNode {

	@Getter
	private final AssignmentTree assignmentTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Nullable
	@Override
	public TypeHandle findType() {
		return getExpressionNode().findType();
	}

	@Override
	public ExpressionNode getExpressionNode() {
		return nodeContext.getNodeRegistry().getExpression(assignmentTree.getExpression());
	}

	@Override
	public ExpressionNode getVariableNode() {
		return nodeContext.getNodeRegistry().getExpression(assignmentTree.getVariable());
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Nullable
		@Override
		public TypeHandle findType() {
			return AssignmentNodeImpl.this.findType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return assignmentTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return assignmentTree;
		}
	}
}
