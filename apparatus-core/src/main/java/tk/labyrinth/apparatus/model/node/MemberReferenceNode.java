package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.MemberReferenceTree;
import tk.labyrinth.apparatus.model.node.common.MethodReferencingNode;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.TypeTemplate;

import javax.annotation.Nullable;

/**
 * Syntax: [target:Expression]::[methodName:String]<br>
 * <br>
 * Must be resolved against functional interface.<br>
 * https://docs.oracle.com/javase/specs/jls/se11/html/jls-15.html#jls-15.13<br>
 */
public interface MemberReferenceNode extends ExpressionNode.Convertible, MethodReferencingNode {

	@Nullable
	TypeHandle findFunctionalInterface();

	@Nullable
	TypeHandle findType();

	MemberReferenceTree getMemberReferenceTree();

	/**
	 * May return:<br>
	 * - {@link AssignmentNode}: a = b::c;<br>
	 * - {@link MethodInvocationNode}: a(b::c);<br>
	 * - {@link TypeCastNode}: (a)b::c;<br>
	 * - {@link VariableNode}: A a = b::c;<br>
	 *
	 * @return non-null
	 */
	Node getParentNode();

	ExpressionNode getTargetNode();
}
