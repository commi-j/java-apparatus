package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.ExpressionTreeUtils;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.trees.util.MethodInvocationTreeUtils;
import tk.labyrinth.jaap.util.TypeMirrorUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import javax.lang.model.type.TypeMirror;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class MethodInvocationNodeImpl implements MethodInvocationNode {

	@Getter
	private final MethodInvocationTree methodInvocationTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod() {
		MethodElementTemplate result;
		{
			MethodSimpleSignature methodSimpleSignature = findMethodSignature();
			if (methodSimpleSignature != null) {
				ExpressionNode targetExpression = findTargetExpression();
				if (targetExpression != null) {
					// Foo.bar()
					// foo.bar()
					//
					TypeHandle type = targetExpression.findType();
					if (type != null && type.isDeclaredType()) {
						result = type.asDeclaredType().toElement().selectMethodElement(methodSimpleSignature);
					} else {
						result = null;
					}
				} else {
					// foo()
					//
					result = nodeContext.findMethod(methodInvocationTree, methodSimpleSignature);
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public MethodSimpleSignature findMethodSignature() {
		MethodSimpleSignature result;
		{
			List<TypeMirror> argumentTypes = methodInvocationTree.getArguments().stream()
					.map(argument -> ExpressionTreeUtils.findTypeMirror(nodeContext.getTreeContext(), EntitySelectionContext.forVariableOrType(), argument))
					.collect(Collectors.toList());
			if (!argumentTypes.contains(null)) {
				result = MethodSimpleSignature.of(getMethodName(), argumentTypes.stream()
						.map(argumentType -> TypeMirrorUtils.resolveUpperBound(
								nodeContext.getTreeContext().getProcessingEnvironment(), argumentType))
						.map(TypeMirror::toString));
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public TypeHandle findReturnType() {
		MethodElementTemplate method = findMethod();
		return method != null ? method.getReturnType() : null;
	}

	@Nullable
	@Override
	public Entity findTargetDeclaration() {
		Entity result;
		{
			ExpressionNode targetExpression = findTargetExpression();
			if (targetExpression != null) {
				result = ExpressionTreeUtils.findEntity(nodeContext.getTreeContext(),
						EntitySelectionContext.forVariableOrType(), targetExpression.getExpressionTree());
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public ExpressionNode findTargetExpression() {
		ExpressionNode result;
		{
			ExpressionTree methodSelect = methodInvocationTree.getMethodSelect();
			if (methodSelect instanceof IdentifierTree) {
				result = null;
			} else if (methodSelect instanceof MemberSelectTree) {
				result = nodeContext.getNodeRegistry().getExpression(((MemberSelectTree) methodSelect).getExpression());
			} else {
				throw new NotImplementedException(ExceptionUtils.render(methodSelect));
			}
		}
		return result;
	}

	@Override
	public String getMethodName() {
		return MethodInvocationTreeUtils.getSimpleName(methodInvocationTree);
	}

	@Override
	public String toString() {
		return methodInvocationTree.toString();
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public MethodInvocationNode asMethodInvocation() {
			return MethodInvocationNodeImpl.this;
		}

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Nullable
		@Override
		public TypeHandle findType() {
			return findReturnType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return methodInvocationTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public boolean isMethodInvocation() {
			return true;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public ExpressionNode asExpression() {
			return new ExpressionNodeSubstitute();
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return methodInvocationTree;
		}

		@Override
		public boolean isExpression() {
			return true;
		}
	}
}
