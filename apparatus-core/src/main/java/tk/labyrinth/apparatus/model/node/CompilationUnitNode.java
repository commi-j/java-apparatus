package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.CompilationUnitTree;

import javax.annotation.Nullable;

public interface CompilationUnitNode extends Node.Convertible {

	@Nullable
	String findPackageName();

	CompilationUnitTree getCompilationUnitTree();
}
