package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.MemberSelectNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.MemberSelectTreeUtils;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelectionContext;
import tk.labyrinth.jaap.template.TypeTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplate;

import javax.annotation.Nullable;
import javax.lang.model.element.Element;

// TODO: This one needs to know its parent to be completed. Does it?
@RequiredArgsConstructor
public class MemberSelectNodeImpl implements MemberSelectNode {

	@Getter
	private final MemberSelectTree memberSelectTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Nullable
	@Override
	public ElementTemplate findElement() {
		Element element = MemberSelectTreeUtils.findElement(nodeContext.getTreeContext(),
				EntitySelectionContext.forVariableOrTypeOrPackage(), memberSelectTree);
		return element != null ? nodeContext.getTreeContext().getProcessingContext().getElementTemplate(element) : null;
	}

	@Nullable
	@Override
	public TypeHandle findType() {
		TypeHandle result;
		{
			ElementTemplate element = findElement();
			if (element != null && !element.isPackageElement()) {
				result = element.resolveType();
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public ExpressionNode getParentExpression() {
		return nodeContext.getNodeRegistry().getExpression(memberSelectTree.getExpression());
	}

	@Override
	public String toString() {
		return memberSelectTree.toString();
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public MemberSelectNode asMemberSelect() {
			return MemberSelectNodeImpl.this;
		}

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Nullable
		@Override
		public TypeHandle findType() {
			return MemberSelectNodeImpl.this.findType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return memberSelectTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public boolean isMemberSelect() {
			return true;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return memberSelectTree;
		}
	}
}
