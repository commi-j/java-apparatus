package tk.labyrinth.apparatus.model.node.common;

public interface MayHaveStaticModifier {

	default boolean hasNoStaticModifier() {
		return !hasStaticModifier();
	}

	boolean hasStaticModifier();
}
