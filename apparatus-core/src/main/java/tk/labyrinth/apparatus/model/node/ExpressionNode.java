package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.ExpressionTree;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;

import javax.annotation.Nullable;

public interface ExpressionNode extends Node.Convertible {

	default IdentifierNode asIdentifier() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default LiteralNode asLiteral() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default MemberSelectNode asMemberSelect() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default MethodInvocationNode asMethodInvocation() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default NewClassNode asNewClass() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default TypeCastNode asTypeCast() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	@Nullable
	TypeHandle findType();

	ExpressionTree getExpressionTree();

	default boolean isIdentifier() {
		return false;
	}

	default boolean isLiteral() {
		return false;
	}

	default boolean isMemberSelect() {
		return false;
	}

	default boolean isMethodInvocation() {
		return false;
	}

	default boolean isNewClass() {
		return false;
	}

	default boolean isTypeCast() {
		return false;
	}

	interface Convertible extends Node.Convertible {

		ExpressionNode asExpression();

		@Override
		default Node asNode() {
			return asExpression().asNode();
		}
	}
}
