package tk.labyrinth.apparatus.model.node;

import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.annotation.Nullable;

/**
 * Variable = Expression
 */
public interface AssignmentNode extends ExpressionNode.Convertible {

	@Nullable
	TypeHandle findType();

	ExpressionNode getExpressionNode();

	ExpressionNode getVariableNode();
}
