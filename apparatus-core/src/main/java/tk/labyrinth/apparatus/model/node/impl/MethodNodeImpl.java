package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.MethodNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;

@RequiredArgsConstructor
public class MethodNodeImpl implements MethodNode {

	@Getter
	private final MethodTree methodTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public Node asNode() {
		return new NodeSubstitute();
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return methodTree;
		}
	}
}
