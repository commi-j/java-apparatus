package tk.labyrinth.apparatus.model.node.common;

public interface MayHaveFinalModifier {

	boolean hasFinalModifier();

	default boolean hasNoFinalModifier() {
		return !hasFinalModifier();
	}
}
