package tk.labyrinth.apparatus.model.node.common;

import tk.labyrinth.apparatus.model.node.ExpressionNode;

import javax.annotation.Nullable;

public interface MayHaveInitializer {

	@Nullable
	ExpressionNode findInitializer();

	boolean hasInitializer();

	default boolean hasNoInitializer() {
		return !hasInitializer();
	}
}
