package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeCastTree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.TypeCastNode;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.TypeTemplate;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;

@RequiredArgsConstructor
public class TypeCastNodeImpl implements TypeCastNode {

	@Getter
	private final NodeContext nodeContext;

	@Getter
	private final TypeCastTree typeCastTree;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Nullable
	@Override
	public TypeHandle findType() {
		// TODO Implement.
		throw new NotImplementedException();
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Override
		public TypeCastNode asTypeCast() {
			return TypeCastNodeImpl.this;
		}

		@Nullable
		@Override
		public TypeHandle findType() {
			return TypeCastNodeImpl.this.findType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return typeCastTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public boolean isTypeCast() {
			return true;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return typeCastTree;
		}
	}
}
