package tk.labyrinth.apparatus.model.node;

import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.element.ElementTemplate;

import javax.annotation.Nullable;

public interface MemberSelectNode extends ExpressionNode.Convertible {

	@Nullable
	ElementTemplate findElement();

	@Nullable
	TypeHandle findType();

	ExpressionNode getParentExpression();
}
