package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.FieldNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.VariableTreeUtils;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.TypeTemplate;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

@RequiredArgsConstructor
public class VariableNodeImpl implements VariableNode {

	@Getter
	private final NodeContext nodeContext;

	@Getter
	private final VariableTree variableTree;

	@Override
	public FieldNode asFieldNode() {
		return nodeContext.getNodeRegistry().getField(variableTree);
	}

	@Override
	public Node asNode() {
		return new NodeSubstitute();
	}

	@Override
	public TypeHandle findType() {
		// TODO Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isFieldNode() {
		return VariableTreeUtils.isField(nodeContext, variableTree);
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public VariableNode asVariable() {
			return VariableNodeImpl.this;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return variableTree;
		}

		@Override
		public boolean isVariable() {
			return true;
		}
	}
}
