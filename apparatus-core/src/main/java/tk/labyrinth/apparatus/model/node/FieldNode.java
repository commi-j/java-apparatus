package tk.labyrinth.apparatus.model.node;

import tk.labyrinth.apparatus.model.node.common.MayHaveFinalModifier;
import tk.labyrinth.apparatus.model.node.common.MayHaveInitializer;
import tk.labyrinth.apparatus.model.node.common.MayHaveStaticModifier;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;

public interface FieldNode extends
		MayHaveFinalModifier,
		MayHaveInitializer,
		MayHaveStaticModifier,
		VariableNode.Convertible {

	String getName();

	TypeHandle getType();

	ExpressionNode getTypeNode();

	FieldElementTemplate toElementTemplate();
}
