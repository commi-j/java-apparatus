package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.FieldNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ClassNodeImpl implements ClassNode {

	@Getter
	private final ClassTree classTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public Node asNode() {
		return new NodeSubstitute();
	}

	@Override
	public Stream<FieldNode> getFields() {
		return classTree.getMembers().stream()
				.filter(VariableTree.class::isInstance)
				.map(VariableTree.class::cast)
				.map(member -> nodeContext.getNodeRegistry().getField(member));
	}

	@Override
	public Stream<BlockNode> getInitializerBlocks() {
		return classTree.getMembers().stream()
				.filter(BlockTree.class::isInstance)
				.map(BlockTree.class::cast)
				.map(member -> nodeContext.getNodeRegistry().getBlock(member));
	}

	@Override
	public String toString() {
		// FIXME: Make it return binary name (or canonical).
		return classTree.getSimpleName().toString();
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return classTree;
		}
	}
}
