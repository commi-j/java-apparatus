package tk.labyrinth.apparatus.model.node.impl;

import tk.labyrinth.apparatus.model.node.ExpressionNode;

public abstract class ExpressionNodeSubstituteBase implements ExpressionNode {

	@Override
	public String toString() {
		return getExpressionTree().toString();
	}
}
