package tk.labyrinth.apparatus.model.node;

import tk.labyrinth.jaap.handle.type.TypeHandle;

/**
 * 12 - int;<br>
 * "string" - String;<br>
 * null - null;<br>
 */
public interface LiteralNode extends ExpressionNode.Convertible {

	TypeHandle getType();
}
