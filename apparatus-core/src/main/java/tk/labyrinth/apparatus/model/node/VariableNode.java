package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.VariableTree;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.TypeTemplate;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;

import javax.annotation.Nullable;

// TODO: Split into: Field, LocalVariable, MethodParameter, LambdaParameter.
public interface VariableNode extends Node.Convertible, NodeContext.Provider {

	default FieldNode asFieldNode() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	@Nullable
	TypeHandle findType();

	VariableTree getVariableTree();

	default boolean isFieldNode() {
		return false;
	}

	interface Convertible extends Node.Convertible {

		@Override
		default Node asNode() {
			return asVariableNode().asNode();
		}

		VariableNode asVariableNode();
	}
}
