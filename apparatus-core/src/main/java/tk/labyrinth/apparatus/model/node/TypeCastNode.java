package tk.labyrinth.apparatus.model.node;

import tk.labyrinth.jaap.handle.type.TypeHandle;

import javax.annotation.Nullable;

public interface TypeCastNode extends ExpressionNode.Convertible {

	@Nullable
	TypeHandle findType();
}
