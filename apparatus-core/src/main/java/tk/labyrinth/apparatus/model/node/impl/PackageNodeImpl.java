package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.PackageTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.PackageNode;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;

@RequiredArgsConstructor
public class PackageNodeImpl implements PackageNode {

	@Getter
	private final NodeContext nodeContext;

	@Getter
	private final PackageTree packageTree;

	@Override
	public Node asNode() {
		return new NodeSubstitute();
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return packageTree;
		}
	}
}
