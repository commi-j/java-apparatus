package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.Tree;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;

public interface Node {

	default ExpressionNode asExpression() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	default VariableNode asVariable() {
		throw new UnsupportedOperationException(ExceptionUtils.render(this));
	}

	NodeContext getNodeContext();

	Tree getTree();

	default boolean isExpression() {
		return false;
	}

	default boolean isVariable() {
		return false;
	}

	interface Convertible {

		Node asNode();

		NodeContext getNodeContext();
	}
}
