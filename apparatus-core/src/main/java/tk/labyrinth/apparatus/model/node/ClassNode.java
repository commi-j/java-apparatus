package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.ClassTree;

import java.util.stream.Stream;

public interface ClassNode extends Node.Convertible {

	ClassTree getClassTree();

	Stream<FieldNode> getFields();

	Stream<BlockNode> getInitializerBlocks();
}
