package tk.labyrinth.apparatus.model.node.common;

import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import javax.annotation.Nullable;

public interface MethodReferencingNode {

	@Nullable
	MethodElementTemplate findMethod();

	default MethodElementTemplate getMethod() {
		MethodElementTemplate result = findMethod();
		if (result == null) {
			throw new IllegalArgumentException("Not found: this = " + this);
		}
		return result;
	}

	String getMethodName();
}
