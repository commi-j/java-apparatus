package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;

@RequiredArgsConstructor
public class BlockNodeImpl implements BlockNode {

	@Getter
	private final BlockTree blockTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public Node asNode() {
		return new NodeSubstitute();
	}

	@Override
	public boolean hasStaticModifier() {
		return blockTree.isStatic();
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return blockTree;
		}
	}
}
