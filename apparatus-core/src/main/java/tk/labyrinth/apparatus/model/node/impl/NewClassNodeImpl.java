package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.NewClassNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.apparatus.sourcetree.util.NewClassTreeUtils;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;

@RequiredArgsConstructor
public class NewClassNodeImpl implements NewClassNode {

	@Getter
	private final NewClassTree newClassTree;

	@Getter
	private final NodeContext nodeContext;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Override
	public TypeHandle getType() {
		return nodeContext.getTreeContext().getProcessingContext().getTypeHandle(
				// TODO: Use proper GenericContext
				GenericContext.empty(),
				NewClassTreeUtils.findTypeMirror(nodeContext.getTreeContext(), newClassTree));
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public NewClassNode asNewClass() {
			return NewClassNodeImpl.this;
		}

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Override
		public TypeHandle findType() {
			return getType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return newClassTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public boolean isNewClass() {
			return true;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return newClassTree;
		}
	}
}
