package tk.labyrinth.apparatus.model.node.impl;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.Tree;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.model.node.TypeCastNode;
import tk.labyrinth.apparatus.model.node.VariableNode;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class MemberReferenceNodeImpl implements MemberReferenceNode {

	@Getter
	private final MemberReferenceTree memberReferenceTree;

	@Getter
	private final NodeContext nodeContext;

	@Getter
	private final Node parentNode;

	@Override
	public ExpressionNode asExpression() {
		return new ExpressionNodeSubstitute();
	}

	@Nullable
	@Override
	public TypeHandle findFunctionalInterface() {
		TypeHandle result;
		if (parentNode.isExpression()) {
			ExpressionNode expressionNode = parentNode.asExpression();
			if (expressionNode.isMethodInvocation()) {
				// FIXME: Implement.
				result = null;
			} else if (expressionNode.isTypeCast()) {
				TypeCastNode typeCastNode = expressionNode.asTypeCast();
				result = typeCastNode.findType();
			} else {
				throw new NotImplementedException(ExceptionUtils.render(this));
			}
		} else if (parentNode.isVariable()) {
			VariableNode variableNode = parentNode.asVariable();
			result = variableNode.findType();
		} else {
			throw new NotImplementedException(ExceptionUtils.render(this));
		}
		return result;
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod() {
		MethodElementTemplate result;
		{
			TypeHandle expressionType = getTargetNode().findType();
			if (expressionType != null && expressionType.isDeclaredType()) {
				List<MethodElementTemplate> potentialMethods = expressionType.asDeclaredType().toElement()
						.getNonOverridenMethods(getMethodName())
						.collect(Collectors.toList());
				// TODO: Learn how to pick good method from different ones.
				result = potentialMethods.size() == 1 ? potentialMethods.get(0) : null;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public TypeHandle findType() {
		// TODO Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getMethodName() {
		return memberReferenceTree.getName().toString();
	}

	@Override
	public ExpressionNode getTargetNode() {
		return nodeContext.getNodeRegistry().getExpression(memberReferenceTree.getQualifierExpression());
	}

	public class ExpressionNodeSubstitute extends ExpressionNodeSubstituteBase {

		@Override
		public Node asNode() {
			return new NodeSubstitute();
		}

		@Nullable
		@Override
		public TypeHandle findType() {
			return MemberReferenceNodeImpl.this.findType();
		}

		@Override
		public ExpressionTree getExpressionTree() {
			return memberReferenceTree;
		}

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}
	}

	public class NodeSubstitute extends NodeSubstituteBase {

		@Override
		public NodeContext getNodeContext() {
			return nodeContext;
		}

		@Override
		public Tree getTree() {
			return memberReferenceTree;
		}
	}
}
