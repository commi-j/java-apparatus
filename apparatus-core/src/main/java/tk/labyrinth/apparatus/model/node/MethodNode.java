package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.MethodTree;

public interface MethodNode extends Node.Convertible {

	MethodTree getMethodTree();
}
