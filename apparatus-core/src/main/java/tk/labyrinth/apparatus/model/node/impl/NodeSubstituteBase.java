package tk.labyrinth.apparatus.model.node.impl;

import tk.labyrinth.apparatus.model.node.Node;

public abstract class NodeSubstituteBase implements Node {

	@Override
	public String toString() {
		return getTree().toString();
	}
}
