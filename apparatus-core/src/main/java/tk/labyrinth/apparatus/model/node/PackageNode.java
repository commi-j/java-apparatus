package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.PackageTree;

public interface PackageNode extends Node.Convertible {

	PackageTree getPackageTree();
}
