package tk.labyrinth.apparatus.model.node;

import com.sun.source.tree.MethodInvocationTree;
import tk.labyrinth.apparatus.model.node.common.MethodReferencingNode;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.apparatus.sourcetree.scope.NodeContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.signature.MethodSimpleSignature;

import javax.annotation.Nullable;

public interface MethodInvocationNode extends
		ExpressionNode.Convertible,
		MethodReferencingNode,
		NodeContext.Provider {

	/**
	 * This may yield null if any of arguments can not be resolved to a type.
	 *
	 * @return nullable
	 */
	@Nullable
	MethodSimpleSignature findMethodSignature();

	@Nullable
	TypeHandle findReturnType();

	/**
	 * foo() - no TargetDeclaration;<br>
	 * foo.bar() - foo is VariableOrType;<br>
	 * foo().bar() - foo is MethodInvocation;<br>
	 * foo.bar.bar() - foo is VariableOrTypeOrPackage, bar is VariableOrType;<br>
	 *
	 * @return nullable
	 */
	@Nullable
	Entity findTargetDeclaration();

	/**
	 * foo() - no TargetExpression;<br>
	 * foo.bar() - TargetExpression is Identifier;<br>
	 * foo.bar.bar() - TargetExpression is MemberSelect;<br>
	 * foo().bar() - TargetExpression is MethodInvocation;<br>
	 *
	 * @return nullable
	 */
	@Nullable
	ExpressionNode findTargetExpression();

	MethodInvocationTree getMethodInvocationTree();

	default Entity getTargetDeclaration() {
		Entity result = findTargetDeclaration();
		if (result == null) {
			throw new IllegalArgumentException("Not found: this = " + this);
		}
		return result;
	}

	default ExpressionNode getTargetExpression() {
		ExpressionNode result = findTargetExpression();
		if (result == null) {
			throw new IllegalArgumentException("Not found: this = " + this);
		}
		return result;
	}
}
