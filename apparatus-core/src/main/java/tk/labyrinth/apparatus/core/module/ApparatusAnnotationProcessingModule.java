package tk.labyrinth.apparatus.core.module;

public interface ApparatusAnnotationProcessingModule {

	/**
	 * {@link ClassLoader} capable of discovering relevant resources.
	 */
	ClassLoader CLASS_LOADER = ApparatusAnnotationProcessingModule.class.getClassLoader();

	void processRound(ApparatusRoundContext context);
}
