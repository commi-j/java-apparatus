package tk.labyrinth.apparatus.core.constructor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Value;
import lombok.experimental.NonFinal;
import tk.labyrinth.apparatus.core.method.MethodBodyBuilder;

@NonFinal
@Value
public abstract class ConstructorAdditionDescriptorMixin {

	@JsonIgnore
	MethodBodyBuilder bodyBuilder;
}
