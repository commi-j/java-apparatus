package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import tk.labyrinth.jaap.model.declaration.ConstructorModifier;
import tk.labyrinth.jaap.model.declaration.JavaConstructorModifier;

import java.io.IOException;
import java.util.Objects;

public class ConstructorModifierMapping {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		return module;
	}

	public static class Deserializer extends StdDeserializer<ConstructorModifier> {

		public Deserializer() {
			super(ConstructorModifier.class);
		}

		@Override
		public ConstructorModifier deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getValueAsString();
			if (!Objects.equals(value, value.toLowerCase())) {
				throw new IllegalArgumentException("Require lowercase: " + value);
			}
			return JavaConstructorModifier.valueOf(value.toUpperCase());
		}
	}
}
