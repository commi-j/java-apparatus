package tk.labyrinth.apparatus.core.constructor;

import lombok.Value;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.ExceptionUtils;
import tk.labyrinth.jaap.model.declaration.ConstructorDeclaration;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import javax.annotation.Nullable;
import java.util.List;

@Value
public class ConstructorAdditionDescriptor {

	/**
	 * null means element will not be added to compiled class, just a hint for IDE.
	 */
	@Nullable
	List<String> body;

	/**
	 * null means element will not be added to compiled class, just a hint for IDE.
	 */
	@Nullable
	ConstructorBodyBuilder bodyBuilder;

	ConstructorDeclaration declaration;

	CanonicalTypeSignature parent;

	/**
	 * As this element is "added" there must be something that makes it exists. This is a reference to such entity.
	 */
	String reference;

	public ConstructorBodyBuilder getBodyBuilderOrFail() {
		if (bodyBuilder == null) {
			throw new IllegalStateException(ExceptionUtils.render(this));
		}
		return bodyBuilder;
	}
}
