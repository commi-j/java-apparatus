package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import tk.labyrinth.jaap.model.ElementSignature;

import java.io.IOException;

public class ElementSignatureMapping {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer());
		return module;
	}

	public static class Deserializer extends StdDeserializer<ElementSignature> {

		public Deserializer() {
			super(ElementSignature.class);
		}

		@Override
		public ElementSignature deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			return ElementSignature.ofValid(p.getValueAsString());
		}
	}

	public static class Serializer extends StdSerializer<ElementSignature> {

		public Serializer() {
			super(ElementSignature.class);
		}

		@Override
		public void serialize(ElementSignature value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.toString());
		}
	}
}
