package tk.labyrinth.apparatus.core.tool;

import tk.labyrinth.jaap.context.RoundContext;

public interface RoundContextAware {

	void acceptRoundContext(RoundContext roundContext);
}
