package tk.labyrinth.apparatus.core.tool;

import tk.labyrinth.apparatus.index.OldIndexEntry;

/**
 * Benefits from:<br>
 * - {@link RoundContextAware};<br>
 */
public interface IndexEntryInvalidationDecider {

	boolean shouldInvalidate(OldIndexEntry oldIndexEntry);
}
