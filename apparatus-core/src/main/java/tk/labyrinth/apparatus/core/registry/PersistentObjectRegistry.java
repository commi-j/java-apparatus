package tk.labyrinth.apparatus.core.registry;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.index.IndexRegistry;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.apparatus.index.OldIndexEntry;
import tk.labyrinth.apparatus.misc4j.lib.jackson.ObjectMapperUtils;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class PersistentObjectRegistry<T> implements ApparatusObjectRegistry<T> {

	private final Set<T> cache = new HashSet<>();

	private final IndexRegistry indexRegistry;

	private final ObjectMapper objectMapper;

	private final Class<T> type;

	private boolean fetched = false;

	private void fetchIfNecessary() {
		if (!fetched) {
			indexRegistry.get(type)
					.map(OldIndexEntry::getValue)
					.map(value -> ObjectMapperUtils.readValue(objectMapper, value, type))
					.forEach(cache::add);
			fetched = true;
		}
	}

	@Override
	public void add(ApparatusAnnotationProcessingModule module, @Nullable TypeElementTemplate origin, T value) {
		fetchIfNecessary();
		// TODO: Check if not present in cache already.
		cache.add(value);
		indexRegistry.add(new NewIndexEntry(
				type.getName(),
				module.getClass().getSimpleName(),
				origin != null ? origin.getSignature() : null,
				ObjectMapperUtils.writeValueAsString(objectMapper, value)));
	}

	@Override
	public Stream<T> get() {
		fetchIfNecessary();
		return cache.stream();
	}
}
