package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;

import java.io.IOException;

public class JavaMethodModifierMapping {

	public static SimpleModule registerWith(SimpleModule module) {
		module.addSerializer(new Serializer());
		return module;
	}

	public static class Serializer extends StdSerializer<JavaMethodModifier> {

		public Serializer() {
			super(JavaMethodModifier.class);
		}

		@Override
		public void serialize(JavaMethodModifier value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.toString().toLowerCase());
		}
	}
}
