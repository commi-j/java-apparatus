package tk.labyrinth.apparatus.core.methodinvocation;

import lombok.Value;
import tk.labyrinth.apparatus.core.method.MethodElementDescriptor;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;

import java.util.List;

@Value
public class MethodInvocationReplacementDescriptor {

	List<MethodElementDescriptor> argumentDescriptors;

	MethodFullSignature from;

	MethodFullSignature to;
}
