package tk.labyrinth.apparatus.core.constructor;

import com.sun.tools.javac.tree.JCTree;
import tk.labyrinth.apparatus.core.AdvancedTreeMaker;

import java.util.List;

public interface ConstructorBodyBuilder {

	JCTree.JCBlock buildBody(AdvancedTreeMaker treeMaker);

	List<String> buildBodyStatements();
}
