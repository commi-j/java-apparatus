package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodModifier;

import java.io.IOException;
import java.util.Objects;

public class MethodModifierMapping {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		return module;
	}

	public static class Deserializer extends StdDeserializer<MethodModifier> {

		public Deserializer() {
			super(MethodModifier.class);
		}

		@Override
		public MethodModifier deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getValueAsString();
			if (!Objects.equals(value, value.toLowerCase())) {
				throw new IllegalArgumentException("Require lowercase: " + value);
			}
			return JavaMethodModifier.valueOf(value.toUpperCase());
		}
	}
}
