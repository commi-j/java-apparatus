package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import tk.labyrinth.apparatus.core.constructor.ConstructorAdditionDescriptor;
import tk.labyrinth.apparatus.core.constructor.ConstructorAdditionDescriptorMixin;
import tk.labyrinth.apparatus.misc4j.lib.jackson.ObjectMapperFactory;

public class CoreObjectMapperFactory {

	public static ObjectMapper create() {
		ObjectMapper result = ObjectMapperFactory.defaultConfigured();
		{
			SimpleModule module = new SimpleModule();
			{
				CanonicalTypeSignatureSignatureMapping.registerWith(module);
				ConstructorModifierMapping.registerWith(module);
				ElementSignatureMapping.registerWith(module);
				JavaConstructorModifierMapping.registerWith(module);
				JavaMethodModifierMapping.registerWith(module);
				MethodModifierMapping.registerWith(module);
				TypeDescriptionMapping.registerWith(module);
			}
			{
				module.setMixInAnnotation(ConstructorAdditionDescriptor.class, ConstructorAdditionDescriptorMixin.class);
			}
			result.registerModule(module);
		}
		return result;
	}
}
