package tk.labyrinth.apparatus.core.util;

import org.apache.commons.io.IOUtils;
import tk.labyrinth.misc4j2.java.io.IoUtils;
import tk.labyrinth.misc4j2.java.util.function.CheckedSupplier;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ResourceUtils extends tk.labyrinth.misc4j2.java.io.ResourceUtils {

	public static List<String> readLines(CheckedSupplier<InputStream, ? extends IOException> inputStreamSupplier) {
		return IoUtils.tryReturnWithResourceSupplier(inputStreamSupplier,
				(InputStream is) -> {
					// Replaces org.apache.commons.io.IOUtils#readLines(java.io.InputStream,java.nio.charset.Charset)
					// as it was failing compiling with NoSuchMethodError (due to older versions of library presented).
					return IOUtils.readLines(new InputStreamReader(is, StandardCharsets.UTF_8));
				});
	}
}
