package tk.labyrinth.apparatus.core.methodinvocation;

import lombok.Value;
import tk.labyrinth.apparatus.core.method.MethodElementDescriptor;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import java.util.List;

@Value
public class ResolvedMethodInvocationReplacementDescriptor {

	List<MethodElementDescriptor> argumentDescriptors;

	MethodElementTemplate from;

	MethodElementTemplate to;
}
