package tk.labyrinth.apparatus.core.method;

import lombok.Value;
import tk.labyrinth.jaap.model.ElementSignature;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import javax.annotation.Nullable;

@Value
public class MethodAdditionDescriptor {

	/**
	 * Null means method will not be added to compiled class, just a hint for IDE.
	 */
	@Nullable
	MethodBodyBuilder bodyBuilder;

	MethodDeclaration declaration;

	CanonicalTypeSignature parent;

	/**
	 * As this element is "added" there must be something that makes it exists. This is a reference to such entity.
	 */
	ElementSignature reference;
}
