package tk.labyrinth.apparatus.core.registry;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import tk.labyrinth.apparatus.core.constructor.ConstructorAdditionDescriptor;
import tk.labyrinth.apparatus.core.method.MethodAdditionDescriptor;
import tk.labyrinth.apparatus.core.methodinvocation.MethodInvocationReplacementDescriptor;
import tk.labyrinth.apparatus.index.IndexRegistry;

public class ApparatusRegistry {

	@Getter
	private final ApparatusObjectRegistry<ConstructorAdditionDescriptor> constructorAdditionDescriptorRegistry;

	@Getter
	private final ApparatusObjectRegistry<MethodAdditionDescriptor> methodAdditionDescriptorRegistry;

	@Getter
	private final ApparatusObjectRegistry<MethodInvocationReplacementDescriptor> methodInvocationReplacementDescriptorRegistry;

	public ApparatusRegistry(IndexRegistry indexRegistry, ObjectMapper objectMapper) {
		constructorAdditionDescriptorRegistry = new PersistentObjectRegistry<>(indexRegistry, objectMapper, ConstructorAdditionDescriptor.class);
		methodAdditionDescriptorRegistry = new PersistentObjectRegistry<>(indexRegistry, objectMapper, MethodAdditionDescriptor.class);
		methodInvocationReplacementDescriptorRegistry = new InMemoryObjectRegistry<>();
	}
}
