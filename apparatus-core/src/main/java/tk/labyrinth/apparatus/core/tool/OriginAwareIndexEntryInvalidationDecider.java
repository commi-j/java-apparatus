package tk.labyrinth.apparatus.core.tool;

import tk.labyrinth.apparatus.index.OldIndexEntry;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import java.util.Set;
import java.util.stream.Collectors;

public class OriginAwareIndexEntryInvalidationDecider implements IndexEntryInvalidationDecider, RoundContextAware {

	private Set<CanonicalTypeSignature> roundOrigins = null;

	@Override
	public void acceptRoundContext(RoundContext roundContext) {
		roundOrigins = roundContext.getTopLevelTypeElements()
				.map(TypeElementTemplate::getSignature)
				.collect(Collectors.toSet());
	}

	@Override
	public boolean shouldInvalidate(OldIndexEntry oldIndexEntry) {
		CanonicalTypeSignature origin = oldIndexEntry.getOrigin();
		return origin != null && roundOrigins.contains(origin);
	}
}
