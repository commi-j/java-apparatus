package tk.labyrinth.apparatus.core.tool;

import java.util.stream.Stream;

public interface ApparatusServiceLoader {

	<T> Stream<T> load(Class<T> type);
}
