package tk.labyrinth.apparatus.core.method;

import com.sun.source.tree.ExpressionTree;
import lombok.Value;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;

@Value
public class ArgumentMethodElementDescriptor implements MethodElementDescriptor {

	int index;

	@Override
	public ExpressionTree resolve(MethodInvocationNode node) {
		return node.getMethodInvocationTree().getArguments().get(index);
	}
}
