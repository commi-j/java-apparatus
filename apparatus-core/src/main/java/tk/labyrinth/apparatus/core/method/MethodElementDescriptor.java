package tk.labyrinth.apparatus.core.method;

import com.sun.source.tree.ExpressionTree;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;

public interface MethodElementDescriptor {

	ExpressionTree resolve(MethodInvocationNode node);
}
