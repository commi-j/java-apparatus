package tk.labyrinth.apparatus.core.module;

import lombok.Value;
import tk.labyrinth.apparatus.core.registry.ApparatusRegistry;
import tk.labyrinth.apparatus.index.IndexRegistry;
import tk.labyrinth.jaap.context.RoundContext;

import javax.annotation.processing.Messager;

@Value
public class ApparatusRoundContext {

	IndexRegistry indexRegistry;

	ApparatusRegistry registry;

	RoundContext roundContext;

	public Messager getMessager() {
		return roundContext.getRound().getProcessingEnvironment().getMessager();
	}

	public void requestNextRound() {
		// TODO
	}
}
