package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import java.io.IOException;

public class TypeDescriptionMapping {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer());
		return module;
	}

	public static class Deserializer extends StdDeserializer<TypeDescription> {

		public Deserializer() {
			super(TypeDescription.class);
		}

		@Override
		public TypeDescription deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			return TypeDescription.of(p.getValueAsString());
		}
	}

	public static class Serializer extends StdSerializer<TypeDescription> {

		public Serializer() {
			super(TypeDescription.class);
		}

		@Override
		public void serialize(TypeDescription value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.toString());
		}
	}
}
