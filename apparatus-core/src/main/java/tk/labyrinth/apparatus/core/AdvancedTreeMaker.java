package tk.labyrinth.apparatus.core;

import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Position;
import lombok.Getter;
import tk.labyrinth.jaap.langmodel.AnnprocElementFactory;
import tk.labyrinth.jaap.langmodel.AnnprocTypeMirrorFactory;
import tk.labyrinth.jaap.langmodel.TypeMirrorFactory;
import tk.labyrinth.jaap.model.declaration.ConstructorDeclaration;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.stream.Stream;

public class AdvancedTreeMaker {

	private final JavacProcessingEnvironment processingEnvironment;

	private final Symtab symtab;

	@Getter
	private final TreeMaker treeMaker;

	private final TypeMirrorFactory typeMirrorFactory;

	public AdvancedTreeMaker(ProcessingEnvironment processingEnvironment) {
		this.processingEnvironment = (JavacProcessingEnvironment) processingEnvironment;
		symtab = Symtab.instance(this.processingEnvironment.getContext());
		treeMaker = TreeMaker.instance(this.processingEnvironment.getContext())
				.at(Position.NOPOS);
		typeMirrorFactory = new AnnprocTypeMirrorFactory(new AnnprocElementFactory(processingEnvironment), processingEnvironment);
	}

	public void addChild(JCTree.JCClassDecl parent, JCTree.JCMethodDecl child) {
		int lastPosition = parent.defs.get(parent.defs.size() - 1).pos;
		//
		// FIXME: Speculative magic number, we need to actually calculate accumulated offset of created trees.
		int currentPosition = lastPosition + 1000;
		//
		// FIXME: Assigning same value may not work correct, need to study this case more.
		//  It seems that JavaParser adds about +3 for each tree.
		child.setPos(currentPosition);
		child.params.forEach(param -> {
			param.setPos(currentPosition);
			param.sym.pos = currentPosition;
		});
		//
		parent.defs = parent.defs.append(child);
	}

	public JCTree.JCBlock createBlock(List<JCTree.JCStatement> statements) {
		return treeMaker.Block(0, statements);
	}

	public JCTree.JCBlock createBlock(Stream<JCTree.JCStatement> statements) {
		return createBlock(statements.collect(List.collector()));
	}

	public JCTree.JCMethodDecl createConstructor(ConstructorDeclaration declaration, JCTree.JCBlock body) {
		return treeMaker.MethodDef(
				modifiersWithPublic(),
				createName("<init>"),
				null,
				List.nil(),
				null,
				declaration.getFormalParameters().stream()
						.map(this::createFormalParameter)
						.collect(List.collector()),
				List.nil(),
				body,
				null);
	}

	public JCTree.JCBlock createEmptyBlock() {
		return createBlock(List.nil());
	}

	public JCTree.JCVariableDecl createFormalParameter(FormalParameterDeclaration declaration) {
		return treeMaker.Param(
				createName(declaration.getName()),
				(Type) typeMirrorFactory.get(declaration.getType()),
				null);
	}

	public Name createName(String name) {
		return processingEnvironment.getElementUtils().getName(name);
	}

	/**
	 * Returns one of:<br>
	 * - {@link IdentifierTree} - foo;<br>
	 * - {@link MemberSelectTree} - foo.bar;<br>
	 *
	 * @param path non-null
	 *
	 * @return non-null
	 */
	public JCTree.JCExpression createPathExpression(String path) {
		JCTree.JCExpression result;
		{
			JCTree.JCExpression expressionTree = null;
			for (String name : path.split("\\.")) {
				if (expressionTree == null) {
					expressionTree = treeMaker.Ident(processingEnvironment.getElementUtils().getName(name));
				} else {
					expressionTree = treeMaker.Select(expressionTree, processingEnvironment.getElementUtils().getName(name));
				}
			}
			result = expressionTree;
		}
		return result;
	}

	public JCTree.JCModifiers modifiersWithPublic() {
		return treeMaker.Modifiers(Flags.PUBLIC);
	}

	public JCTree.JCModifiers noModifiers() {
		return treeMaker.Modifiers(0);
	}
}
