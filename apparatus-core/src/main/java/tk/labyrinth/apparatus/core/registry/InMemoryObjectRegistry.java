package tk.labyrinth.apparatus.core.registry;

import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class InMemoryObjectRegistry<T> implements ApparatusObjectRegistry<T> {

	private final Set<T> cache = new HashSet<>();

	@Override
	public void add(ApparatusAnnotationProcessingModule module, @Nullable TypeElementTemplate origin, T value) {
		// Has no use for module and origin for now.
		//
		// TODO: Check if not present in cache already.
		cache.add(value);
	}

	@Override
	public Stream<T> get() {
		return cache.stream();
	}
}
