package tk.labyrinth.apparatus.core.registry;

import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.annotation.Nullable;
import java.util.stream.Stream;

public interface ApparatusObjectRegistry<T> {

	void add(ApparatusAnnotationProcessingModule module, @Nullable TypeElementTemplate origin, T value);

	default void add(ApparatusAnnotationProcessingModule module, T value) {
		add(module, null, value);
	}

	Stream<T> get();
}
