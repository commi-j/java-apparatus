package tk.labyrinth.apparatus.core.methodinvocation;

import com.sun.source.tree.CompilationUnitTree;
import lombok.RequiredArgsConstructor;
import lombok.val;
import tk.labyrinth.apparatus.model.node.common.MethodReferencingNode;
import tk.labyrinth.apparatus.sourcetree.registry.AdvancedNodeVisitor;
import tk.labyrinth.apparatus.sourcetree.registry.TreeRegistry;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class MethodInvocationReplacementAnalyzer {

	private final MethodInvocationReplacementContext replacementContext;

	private final TreeRegistry treeRegistry;

	private Stream<MethodInvocationManipulationRequest> findRequest(MethodReferencingNode node) {
		ResolvedMethodInvocationReplacementDescriptor replacementDescriptor = replacementContext.findReplacementDescriptor(node);
		return replacementDescriptor != null
				? Stream.of(new MethodInvocationManipulationRequest(replacementDescriptor, node))
				: null;
	}

	public Stream<MethodInvocationManipulationRequest> collectRequests(Stream<CompilationUnitTree> compilationUnitTrees) {
		val visitor = new AdvancedNodeVisitor<MethodInvocationManipulationRequest>()
				.visitMemberReference(this::findRequest)
				.visitMethodInvocation(this::findRequest);
		return compilationUnitTrees.flatMap(compilationUnitTree ->
				compilationUnitTree.accept(visitor.getTreeVisitor(), treeRegistry));
	}
}
