package tk.labyrinth.apparatus.core.method;

import com.sun.source.tree.ExpressionTree;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.TreeMaker;
import tk.labyrinth.apparatus.javac.util.TreeMakerUtils;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

public class TargetMethodElementDescriptor implements MethodElementDescriptor {

	@Override
	public ExpressionTree resolve(MethodInvocationNode node) {
		ExpressionTree result;
		{
			ExpressionNode target = node.findTargetExpression();
			if (target != null) {
				result = target.getExpressionTree();
			} else {
				JavacProcessingEnvironment javacProcessingEnvironment = (JavacProcessingEnvironment)
						node.getNodeContext().getTreeContext().getProcessingEnvironment();
				TreeMaker treeMaker = TreeMaker.instance(javacProcessingEnvironment.getContext());
				//
				TypeElementTemplate methodParentTypeElement = node.getMethod().getParent();
				TypeElementTemplate typeElementOfThis = node.getNodeContext().getEnclosingTypeElement(node.getMethodInvocationTree()).getTypeChain()
						.filter(typeElement -> typeElement.isAssignableTo(methodParentTypeElement))
						.findFirst().orElseThrow();
				result = TreeMakerUtils.createPathExpression(treeMaker, javacProcessingEnvironment.getElementUtils(),
						typeElementOfThis.getSignature() + ".this");
			}
		}
		return result;
	}
}
