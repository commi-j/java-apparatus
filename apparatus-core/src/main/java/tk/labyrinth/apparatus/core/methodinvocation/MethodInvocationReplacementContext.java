package tk.labyrinth.apparatus.core.methodinvocation;

import lombok.RequiredArgsConstructor;
import lombok.val;
import tk.labyrinth.apparatus.core.registry.ApparatusRegistry;
import tk.labyrinth.apparatus.model.node.common.MethodReferencingNode;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class MethodInvocationReplacementContext {

	private final ProcessingContext processingContext;

	private final ApparatusRegistry registry;

	/**
	 * Key - MethodName
	 */
	private Map<String, Map<MethodElementTemplate, ResolvedMethodInvocationReplacementDescriptor>> replacementDescriptors = null;

	private void initializeReplacementDescriptors() {
		if (replacementDescriptors == null) {
			replacementDescriptors = new HashMap<>();
			registry.getMethodInvocationReplacementDescriptorRegistry().get()
					.map(replacementDescriptor -> resolve(processingContext, replacementDescriptor))
					.forEach(replacementDescriptor -> replacementDescriptors
							.computeIfAbsent(
									replacementDescriptor.getFrom().getSimpleNameAsString(),
									key -> new HashMap<>())
							.put(replacementDescriptor.getFrom(), replacementDescriptor));
		}
	}

	@Nullable
	public ResolvedMethodInvocationReplacementDescriptor findReplacementDescriptor(MethodReferencingNode fromNode) {
		initializeReplacementDescriptors();
		//
		ResolvedMethodInvocationReplacementDescriptor result;
		{
			val namedManipulations = replacementDescriptors.getOrDefault(
					fromNode.getMethodName(), Collections.emptyMap());
			if (!namedManipulations.isEmpty()) {
				MethodElementTemplate method = fromNode.findMethod();
				if (method != null) {
					result = namedManipulations.get(method);
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	public static ResolvedMethodInvocationReplacementDescriptor resolve(ProcessingContext processingContext, MethodInvocationReplacementDescriptor replacementDescriptor) {
		return new ResolvedMethodInvocationReplacementDescriptor(
				replacementDescriptor.getArgumentDescriptors(),
				processingContext.getMethodElementTemplate(replacementDescriptor.getFrom()),
				processingContext.getMethodElementTemplate(replacementDescriptor.getTo()));
	}
}
