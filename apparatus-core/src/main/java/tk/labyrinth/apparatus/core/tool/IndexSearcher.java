package tk.labyrinth.apparatus.core.tool;

import tk.labyrinth.apparatus.index.OldIndexEntry;

import java.util.stream.Stream;

public interface IndexSearcher {

	String PATH = "META-INF/services/apparatus";

	/**
	 * Searches for resources with name equal to <b>key</b> located in {@link #PATH META-INF/services/apparatus}.<br>
	 * The search is made in all dependency sources which are non-modifiable from this Searcher's point of view.<br>
	 * <br>
	 * FIXME: Define whether we want to include module's own resource location to the output (we'd better not as it is handled separately on changes within module).<br>
	 *
	 * @param key non-null
	 *
	 * @return non-null
	 */
	Stream<OldIndexEntry> search(String key);

	// FIXME: Move to some utility class.
	static String buildPath(String key) {
		return PATH + "/" + key;
	}
}
