package tk.labyrinth.apparatus.core.methodinvocation;

import lombok.Value;
import tk.labyrinth.apparatus.model.node.common.MethodReferencingNode;

@Value
public class MethodInvocationManipulationRequest {

	ResolvedMethodInvocationReplacementDescriptor manipulation;

	MethodReferencingNode node;
}
