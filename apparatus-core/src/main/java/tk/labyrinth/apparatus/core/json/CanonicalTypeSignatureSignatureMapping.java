package tk.labyrinth.apparatus.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import java.io.IOException;

public class CanonicalTypeSignatureSignatureMapping {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer());
		return module;
	}

	public static class Deserializer extends StdDeserializer<CanonicalTypeSignature> {

		public Deserializer() {
			super(CanonicalTypeSignature.class);
		}

		@Override
		public CanonicalTypeSignature deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			return CanonicalTypeSignature.ofValid(p.getValueAsString());
		}
	}

	public static class Serializer extends StdSerializer<CanonicalTypeSignature> {

		public Serializer() {
			super(CanonicalTypeSignature.class);
		}

		@Override
		public void serialize(CanonicalTypeSignature value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.toString());
		}
	}
}
