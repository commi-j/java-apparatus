package tk.labyrinth.apparatus.core.module;

import lombok.Value;
import tk.labyrinth.jaap.context.RoundContext;

@Value
public class ModuleRound {

	RoundContext roundContext;
}
