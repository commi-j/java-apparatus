@file:Suppress("UNCHECKED_CAST")

dependencies {
	//
	// TODO: Unused now
	//	api(project(":apparatus-model"))
	//
	api(project(":apparatus-misc4j2"))
	//
	api(libraries["tk.labyrinth:jaap"])
	//
	//
	testCompileOnly(libraries["com.google.auto.service:auto-service"])
	testAnnotationProcessor(libraries["com.google.auto.service:auto-service"])
}
//
(extra["addJavacExportsToCompiler"] as () -> Unit).invoke()
(extra["configurePublishingToBintray"] as () -> Unit).invoke()
