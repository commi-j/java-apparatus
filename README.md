# Java Apparatus

[![LICENSE](https://img.shields.io/badge/license-MIT-green.svg?label=License)](LICENSE)
[![PIPELINE](https://gitlab.com/commi-j/java-apparatus/badges/master/pipeline.svg)](https://gitlab.com/commi-j/java-apparatus/-/pipelines)

A tool for enhanced Java development experience.

## Features

- [Extension Methods](apparatus-extension-methods) - allows static methods to be
syntactically represented as instance methods;
- [Smart Constructors](apparatus-smart-constructors) - generates constructor
that invokes super and initializes final fields;

## Installation Guide

Apparatus is currently in alpha so it is not available via default resource repositories. You need to use custom library and plugin repositories specified below.

In order to use Apparatus features you need to do the following:
1. Add 'library' dependency to your project - this lets you declare feature annotations;
1. Add 'annproc' dependency to your project - 
1. Install IntelliJ IDEA Plugin - this enables code alterations to be visible in IDE;

#### Maven

For Maven you need to add library dependency (with provided scope) and also custom repository where you can find it.

```xml
<dependency>
    <groupId>tk.labyrinth</groupId>
    <artifactId>apparatus-*</artifactId>
    <version>0.2.3</version>
    <scope>provided</scope>
</dependency>
```

(Replace '*' with whicheve feature you need.)

```xml
<repository>
    <id>bintray-java-apparatus</id>
    <url>https://dl.bintray.com/commitman/java-apparatus</url>
</repository>
```

#### Gradle

TODO

#### IntelliJ Idea

1. Add Custom Plugin Repository: https://commi-j.gitlab.io/java-apparatus/updatePlugins.xml
   - Guide: https://www.jetbrains.com/help/idea/managing-plugins.html#repos
1. Install **Apparatus One** via IDE plugin UI;


## Miscellaneous

- https://checkerframework.org/
- https://github.com/google/auto


- http://javaparser.org/

## Troubleshooting

### Annproc

- Make sure libraries with Processors and their dependencies are in compiler classpath:
  - Gradle - added as **annotationProcessor**;
  - Maven - not of **runtime** scope;
- Debug breakpoints:
  - java.util.ServiceLoader$ProviderImpl#get
  - com.sun.tools.javac.processing.JavacProcessingEnvironment#doProcessing
  - com.sun.tools.javac.processing.JavacProcessingEnvironment.ServiceIterator

### Build

- Gradle daemon does not release jar files after build, so no clean can be performed after that. To fix that run gradle in debug which will create new daemon for each invocation.
