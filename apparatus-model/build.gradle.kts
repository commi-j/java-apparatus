@file:Suppress("UNCHECKED_CAST")

dependencies {
	//
	api(libraries["tk.labyrinth:jaap"])
	//
	implementation(project(":apparatus-misc4j2"))
}
//
//(extra["configurePublishingToBintray"] as () -> Unit).invoke()
