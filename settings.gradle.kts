//
rootProject.name = "apparatus-parent"
//
include("apparatus-annotation-processor")
include("apparatus-core")
include("apparatus-extension-methods")
include("apparatus-extension-methods:extension-methods-library")
include("apparatus-extension-methods:extension-methods-usages")
include("apparatus-idea-plugin")
include("apparatus-misc4j2")
include("apparatus-smart-constructors")
include("apparatus-smart-constructors:smart-constructors-usages")
include("apparatus-testing")
//
// FIXME: Unused
include("apparatus-model")
