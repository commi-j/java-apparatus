@file:Suppress("UNCHECKED_CAST")

plugins {
	`java-library`
	`maven-publish`
	//
	// https://github.com/fkorotkov/gradle-libraries-plugin
	id("com.github.fkorotkov.libraries").version("1.1")
	//
	id("com.jfrog.bintray").version("1.8.5")
}
//
allprojects {
	group = "tk.labyrinth.apparatus"
	version = "0.2.3"
	//
	apply {
		`java-library`
		libraries
		`maven-publish`
	}
	//
	repositories {
		mavenCentral()
		mavenLocal()
	}
	//
	dependencies {
		val versionJpig = "0.8.0d"
		val versionJunit5 = "5.5.2"
		val versionLog4j = "2.12.1"
		val versionLombok = "1.18.12"
		val versionSlf4j = "1.7.28"
		//
		implementation("com.google.code.findbugs", "jsr305", "3.0.2")
		//
		compileOnly("org.projectlombok", "lombok", versionLombok)
		annotationProcessor("org.projectlombok", "lombok", versionLombok)
		//
		compileOnly("org.slf4j", "slf4j-api", versionSlf4j)
		//
		//
		testRuntimeOnly("org.apache.logging.log4j", "log4j-slf4j-impl", versionLog4j)
		//
		testImplementation("org.junit.jupiter", "junit-jupiter", versionJunit5)
		//
		testCompileOnly("org.projectlombok", "lombok", versionLombok)
		testAnnotationProcessor("org.projectlombok", "lombok", versionLombok)
		//
		testCompileOnly("org.slf4j", "slf4j-api", versionSlf4j)
		//
		testImplementation(libraries["tk.labyrinth:jaap-testing"])
	}
	//
	tasks {
		//
		compileJava {
			sourceCompatibility = "11"
			targetCompatibility = "11"
		}
		//
		test {
			useJUnitPlatform()
		}
	}
	//
	//	publishing {
	//		publications {
	//			create<MavenPublication>("maven") {
	//				from(components["java"])
	//			}
	//		}
	//	}
	//
	extra["addJavacExportsToCompiler"] = {
		tasks {
			val addExportsOptions = mutableListOf<String>()
			//
			addExportsOptions.add("--add-exports=jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED")
			addExportsOptions.add("--add-exports=jdk.compiler/com.sun.tools.javac.model=ALL-UNNAMED")
			addExportsOptions.add("--add-exports=jdk.compiler/com.sun.tools.javac.processing=ALL-UNNAMED")
			addExportsOptions.add("--add-exports=jdk.compiler/com.sun.tools.javac.tree=ALL-UNNAMED")
			addExportsOptions.add("--add-exports=jdk.compiler/com.sun.tools.javac.util=ALL-UNNAMED")
			//
			compileJava {
				options.compilerArgs.addAll(addExportsOptions)
			}
			compileTestJava {
				options.compilerArgs.addAll(addExportsOptions)
			}
		}
	}
	extra["configurePublishingToBintray"] = {
		//
		apply {
			bintray
			plugin("maven-publish")
		}
		//
		val publicationArtifactId = project.name
		val publicationName = "maven"
		//
		bintray {
			override = true
			publish = true
			user = System.getenv("BINTRAY_USERNAME")
			key = System.getenv("BINTRAY_API_KEY")
			pkg = PackageConfig().apply {
				repo = "java-apparatus"
				name = publicationArtifactId
				setLicenses("MIT")
				vcsUrl = "https://gitlab.com/commi-j/java-apparatus"
				//				setVersion {
				//					name = project.version as String
				//				}
				version = VersionConfig().apply {
					name = project.version as String
				}
			}
			setPublications(publicationName)
		}
		//
		publishing {
			publications {
				create<MavenPublication>(publicationName) {
					from(components["java"])
				}
			}
		}
	}
}
//
(extra["configurePublishingToBintray"] as () -> Unit).invoke()
//
//
// Utility
//
inline val ObjectConfigurationAction.bintray: ObjectConfigurationAction
	get() = plugin("com.jfrog.bintray")
inline val ObjectConfigurationAction.`java-library`: ObjectConfigurationAction
	get() = plugin("java-library")
inline val ObjectConfigurationAction.libraries: ObjectConfigurationAction
	get() = plugin("com.github.fkorotkov.libraries")
inline val ObjectConfigurationAction.`maven-publish`: ObjectConfigurationAction
	get() = plugin("maven-publish")
