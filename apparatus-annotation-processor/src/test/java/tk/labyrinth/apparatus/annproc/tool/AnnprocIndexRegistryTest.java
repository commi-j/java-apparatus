package tk.labyrinth.apparatus.annproc.tool;

import j2a.EmptyClass;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.core.module.ApparatusRoundContext;
import tk.labyrinth.apparatus.core.tool.IndexEntryInvalidationDecider;
import tk.labyrinth.apparatus.core.tool.IndexSearcher;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.apparatus.index.OldIndexEntry;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.misc4j2.java.io.IoUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class AnnprocIndexRegistryTest {

	public static final String indexKey = "entries";

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	private List<String> readOutputEntries() {
		return IoUtils.unchecked(() -> FileUtils.readLines(
				new File(testCompiler.getOutputDirectory().toFile() + "/" + IndexSearcher.buildPath(indexKey)),
				StandardCharsets.UTF_8));
	}

	@Test
	void testFlushWithAppend() {
		AppendTestModule testModule = new AppendTestModule();
		//
		testCompiler.run(
				CompilationTarget.ofSourceTypes(EmptyClass.class),
				new ApparatusAnnotationProcessor(false)
						.withModule(testModule));
		//
		Assertions.assertEquals(
				List.of("{\"module\":\"AppendTestModule\",\"value\":\"aEntry0\"}"),
				readOutputEntries());
		//
		testCompiler.run(
				CompilationTarget.ofSourceTypes(EmptyClass.class),
				new ApparatusAnnotationProcessor(false)
						.withModule(testModule));
		//
		Assertions.assertEquals(
				List.of(
						"{\"module\":\"AppendTestModule\",\"value\":\"aEntry0\"}",
						"{\"module\":\"AppendTestModule\",\"value\":\"aEntry1\"}"),
				readOutputEntries());
	}

	@Test
	void testFlushWithOverride() {
		OverrideTestModule testModule = new OverrideTestModule();
		//
		testCompiler.run(
				CompilationTarget.ofSourceTypes(EmptyClass.class),
				new ApparatusAnnotationProcessor(false)
						.withModule(testModule));
		//
		Assertions.assertEquals(
				List.of("{\"module\":\"OverrideTestModule\",\"value\":\"oEntry0\"}"),
				readOutputEntries());
		//
		testCompiler.run(
				CompilationTarget.ofSourceTypes(EmptyClass.class),
				new ApparatusAnnotationProcessor(false)
						.withModule(testModule));
		//
		Assertions.assertEquals(
				List.of("{\"module\":\"OverrideTestModule\",\"value\":\"oEntry1\"}"),
				readOutputEntries());
	}

	private static class AppendTestModule implements ApparatusAnnotationProcessingModule {

		private final List<List<String>> previousEntries = new ArrayList<>();

		private int count = 0;

		@Override
		public void processRound(ApparatusRoundContext context) {
			if (context.getRoundContext().getRound().isLast()) {
				previousEntries.add(context.getIndexRegistry().get(indexKey)
						.map(OldIndexEntry::getValue)
						.collect(Collectors.toList()));
				context.getIndexRegistry().add(new NewIndexEntry(
						indexKey,
						this.getClass().getSimpleName(),
						null,
						"aEntry" + count++));
			}
		}
	}

	private static class OverrideTestModule implements
			ApparatusAnnotationProcessingModule,
			IndexEntryInvalidationDecider {

		private int count = 0;

		@Override
		public void processRound(ApparatusRoundContext context) {
			if (context.getRoundContext().getRound().isLast()) {
				context.getIndexRegistry().add(new NewIndexEntry(
						indexKey,
						this.getClass().getSimpleName(),
						null,
						"oEntry" + count++));
			}
		}

		@Override
		public boolean shouldInvalidate(OldIndexEntry oldIndexEntry) {
			return oldIndexEntry.getValue().startsWith("oEntry");
		}
	}
}
