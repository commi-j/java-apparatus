package tk.labyrinth.apparatus.annproc.tool;

import j2a.EmptyClass;
import j2a.NotImplementedNumber;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.core.module.ApparatusRoundContext;
import tk.labyrinth.apparatus.core.tool.IndexSearcher;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.misc4j2.java.io.IoUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;

class AnnprocIndexRegistryAdvancedTest {

	public static final String indexKey = "entries";

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	private List<String> readOutputEntries() {
		return IoUtils.unchecked(() -> FileUtils.readLines(
				new File(testCompiler.getOutputDirectory().toFile() + "/" + IndexSearcher.buildPath(indexKey)),
				StandardCharsets.UTF_8));
	}

	@Test
	void test() {
		testCompiler.run(
				CompilationTarget.ofSourceTypes(
						EmptyClass.class,
						NotImplementedNumber.class),
				new ApparatusAnnotationProcessor(false)
						.withModule(new AdvancedTestModule("0")));
		//
		Assertions.assertEquals(
				List.of(
						"{\"module\":\"AdvancedTestModule\",\"origin\":\"j2a.EmptyClass\",\"value\":\"j2a.EmptyClass0\"}",
						"{\"module\":\"AdvancedTestModule\",\"origin\":\"j2a.NotImplementedNumber\",\"value\":\"j2a.NotImplementedNumber0\"}"),
				readOutputEntries());
		//
		testCompiler.run(
				CompilationTarget.ofSourceTypes(EmptyClass.class),
				new ApparatusAnnotationProcessor(false)
						.withModule(new AdvancedTestModule("1")));
		//
		Assertions.assertEquals(
				List.of(
						"{\"module\":\"AdvancedTestModule\",\"origin\":\"j2a.NotImplementedNumber\",\"value\":\"j2a.NotImplementedNumber0\"}",
						"{\"module\":\"AdvancedTestModule\",\"origin\":\"j2a.EmptyClass\",\"value\":\"j2a.EmptyClass1\"}"),
				readOutputEntries());
	}

	@RequiredArgsConstructor
	private static class AdvancedTestModule implements ApparatusAnnotationProcessingModule {

		private final String suffix;

		@Override
		public void processRound(ApparatusRoundContext context) {
			context.getRoundContext().getAllTypeElements().forEach(typeElementTemplate ->
					context.getIndexRegistry().add(new NewIndexEntry(
							indexKey,
							this.getClass().getSimpleName(),
							typeElementTemplate.getSignature(),
							typeElementTemplate.getSignature() + suffix)));
		}
	}
}
