package j2a.sibling;

import com.sun.tools.javac.tree.JCTree;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.core.AdvancedTreeMaker;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.core.module.ApparatusRoundContext;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.sourcetree.scope.ElementNodeUtils;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.model.declaration.ConstructorDeclaration;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.testing.CompilationException;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.misc4j2.java.io.IoUtils;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

public class SiblingsTest {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testFailIfNoPosSpecifiedForFormalParameter() {
		ContribAssertions.assertThrows(
				() -> testCompiler.run(
						CompilationTarget.builder()
								.sourceTypes(
										FirstSibling.class,
										SecondSibling.class)
								.build(),
						new ApparatusAnnotationProcessor(false)
								.withModule(new NoPosGeneratingModule())),
				fault -> {
					Assertions.assertEquals(CompilationException.class, fault.getClass());
					List<String> output = ((CompilationException) fault).getOutput();
					{
						List<String> messages = readNoPosMessage();
						Assertions.assertEquals(messages, output.subList(0, messages.size()));
					}
				});
	}

	@Test
	void testSucceedIfPosSpecifiedForFormalParameter() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceTypes(
								FirstSibling.class,
								SecondSibling.class)
						.build(),
				new ApparatusAnnotationProcessor(false)
						.withModule(new PosGeneratingModule()));
	}

	private static List<String> readNoPosMessage() {
		List<Integer> versionNumbers = Runtime.version().version();
		String versionString = versionNumbers.stream()
				.map(String::valueOf)
				.collect(Collectors.joining("."));
		//
		return IoUtils.unchecked(() -> IOUtils.readLines(SiblingsTest.class.getResourceAsStream(
				"noPosMessage-" + versionNumbers.get(0) + ".txt"),
				StandardCharsets.UTF_8)).stream()
				.map(line -> line.replace("${version}", versionString))
				.collect(Collectors.toList());
	}

	private static class NoPosGeneratingModule implements ApparatusAnnotationProcessingModule {

		@Override
		public void processRound(ApparatusRoundContext context) {
			if (context.getRoundContext().getRound().isFirst()) {
				AdvancedTreeMaker treeMaker = new AdvancedTreeMaker(context.getRoundContext().getProcessingContext().getProcessingEnvironment());
				//
				JCTree.JCMethodDecl constructor = treeMaker.createConstructor(
						ConstructorDeclaration.builder()
								.formalParameters(List.of(
										FormalParameterDeclaration.builder()
												.name("i")
												.type(TypeDescription.ofNonParameterized(int.class))
												.build()))
								.build(),
						treeMaker.createEmptyBlock());
				//
				ClassNode classNode = ElementNodeUtils.getClass(context.getRoundContext()
						.getProcessingContext().getTypeElementTemplate(FirstSibling.class));
				JCTree.JCClassDecl jcClassDecl = (JCTree.JCClassDecl) classNode.getClassTree();
				jcClassDecl.defs = jcClassDecl.defs.append(constructor);
			}
		}
	}

	private static class PosGeneratingModule implements ApparatusAnnotationProcessingModule {

		@Override
		public void processRound(ApparatusRoundContext context) {
			if (context.getRoundContext().getRound().isFirst()) {
				AdvancedTreeMaker treeMaker = new AdvancedTreeMaker(context.getRoundContext().getProcessingContext().getProcessingEnvironment());
				//
				JCTree.JCMethodDecl constructor = treeMaker.createConstructor(
						ConstructorDeclaration.builder()
								.formalParameters(List.of(
										FormalParameterDeclaration.builder()
												.name("i")
												.type(TypeDescription.ofNonParameterized(int.class))
												.build()))
								.build(),
						treeMaker.createEmptyBlock());
				//
				ClassNode classNode = ElementNodeUtils.getClass(context.getRoundContext()
						.getProcessingContext().getTypeElementTemplate(FirstSibling.class));
				JCTree.JCClassDecl jcClassDecl = (JCTree.JCClassDecl) classNode.getClassTree();
				jcClassDecl.defs = jcClassDecl.defs.append(constructor);
				//
				constructor.getParameters().get(0).pos = 1000;
			}
		}
	}
}
