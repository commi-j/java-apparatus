package j2a;

import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;

public class NotImplementedNumber extends Number {

	@Override
	public double doubleValue() {
		throw new NotImplementedException();
	}

	@Override
	public float floatValue() {
		throw new NotImplementedException();
	}

	@Override
	public int intValue() {
		throw new NotImplementedException();
	}

	@Override
	public long longValue() {
		throw new NotImplementedException();
	}
}
