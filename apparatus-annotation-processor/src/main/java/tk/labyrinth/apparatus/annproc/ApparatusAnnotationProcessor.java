package tk.labyrinth.apparatus.annproc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auto.service.AutoService;
import com.sun.source.tree.CompilationUnitTree;
import tk.labyrinth.apparatus.annproc.constructors.ConstructorAdditionProcessor;
import tk.labyrinth.apparatus.annproc.methodinvocations.MethodInvocationReplacementProcessor;
import tk.labyrinth.apparatus.annproc.tool.AnnprocIndexRegistry;
import tk.labyrinth.apparatus.core.json.CoreObjectMapperFactory;
import tk.labyrinth.apparatus.core.methodinvocation.MethodInvocationManipulationRequest;
import tk.labyrinth.apparatus.core.methodinvocation.MethodInvocationReplacementAnalyzer;
import tk.labyrinth.apparatus.core.methodinvocation.MethodInvocationReplacementContext;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.core.module.ApparatusRoundContext;
import tk.labyrinth.apparatus.core.registry.ApparatusRegistry;
import tk.labyrinth.apparatus.core.tool.IndexEntryInvalidationDecider;
import tk.labyrinth.apparatus.core.tool.OriginAwareIndexEntryInvalidationDecider;
import tk.labyrinth.apparatus.core.tool.RoundContextAware;
import tk.labyrinth.apparatus.misc4j.java.util.ServiceLoaderUtils;
import tk.labyrinth.apparatus.sourcetree.registry.CompilationBundleTreeRegistry;
import tk.labyrinth.apparatus.sourcetree.util.CompilationUnitTreeUtils;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;
import tk.labyrinth.jaap.template.element.enhanced.EnhancedProcessing;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;

import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

// TODO: This one must have little imports to be guaranteedly instatiatible and must invoke another instance that will may fail due to imports.
@AutoService(Processor.class)
@SupportedAnnotationTypes("*")
public class ApparatusAnnotationProcessor extends CallbackAnnotationProcessor {

	private final Set<ApparatusAnnotationProcessingModule> modules = new HashSet<>();

	private final ObjectMapper objectMapper = CoreObjectMapperFactory.create();

	private final SyntheticElementTemplateRegistry syntheticElementTemplateRegistry = new SyntheticElementTemplateRegistry();

	private AnnprocIndexRegistry indexRegistry = null;

	private ApparatusRegistry registry = null;

	public ApparatusAnnotationProcessor() {
		this(true);
	}

	public ApparatusAnnotationProcessor(boolean loadModules) {
		onInit(processingEnvironment -> {
			if (loadModules) {
				modules.addAll(ServiceLoaderUtils.load(ApparatusAnnotationProcessingModule.class));
			}
			//
			indexRegistry = new AnnprocIndexRegistry(objectMapper, processingEnvironment);
			indexRegistry.addIndexEntryInvalidationDecider(new OriginAwareIndexEntryInvalidationDecider());
			registry = new ApparatusRegistry(indexRegistry, objectMapper);
			//
			modules.forEach(module -> {
				if (module instanceof IndexEntryInvalidationDecider) {
					indexRegistry.addIndexEntryInvalidationDecider((IndexEntryInvalidationDecider) module);
				}
			});
		});
		onEachRound(round -> {
			//
		});
		onFirstRound(round -> {
			RoundContext roundContext = RoundContext.of(round, EnhancedProcessing.createContext(round.getProcessingEnvironment(),
					syntheticElementTemplateRegistry));
			{
				indexRegistry.getIndexEntryInvalidationDeciders().forEach(indexEntryInvalidationDecider -> {
					if (indexEntryInvalidationDecider instanceof RoundContextAware) {
						((RoundContextAware) indexEntryInvalidationDecider).acceptRoundContext(roundContext);
					}
				});
			}
			{
				ApparatusRoundContext apparatusRoundContext = new ApparatusRoundContext(
						indexRegistry, registry, roundContext);
				modules.forEach(module -> module.processRound(apparatusRoundContext));
			}
			{
				// ConstructorAdditions
				//
				ConstructorAdditionProcessor processor = new ConstructorAdditionProcessor(roundContext.getProcessingContext());
				registry.getConstructorAdditionDescriptorRegistry().get()
						.filter(constructorAdditionDescriptor -> constructorAdditionDescriptor.getBodyBuilder() != null)
						.forEach(processor::process);
			}
			{
				// MethodAdditions
				//
				registry.getMethodAdditionDescriptorRegistry().get().forEach(methodAdditionDescriptor ->
						syntheticElementTemplateRegistry.registerMethodDeclaration(
								methodAdditionDescriptor.getParent(),
								methodAdditionDescriptor.getDeclaration()));
			}
			{
				// MethodInvocationReplacements
				//
				// TODO: Replace Invocations
				List<CompilationUnitTree> compilationUnitTrees = CompilationUnitTreeUtils.streamFrom(round)
						.collect(Collectors.toList());
				//
				MethodInvocationReplacementContext replacementContext = new MethodInvocationReplacementContext(roundContext.getProcessingContext(), registry);
				CompilationBundleTreeRegistry treeRegistry = new CompilationBundleTreeRegistry(roundContext);
				//
				List<MethodInvocationManipulationRequest> requests;
				{
					MethodInvocationReplacementAnalyzer analyzer = new MethodInvocationReplacementAnalyzer(
							replacementContext, treeRegistry);
					requests = analyzer.collectRequests(compilationUnitTrees.stream())
							.collect(Collectors.toList());
				}
				{
					MethodInvocationReplacementProcessor processor = new MethodInvocationReplacementProcessor();
					requests.forEach(processor::process);
				}
			}
		});
		onLastRound(round -> {
			RoundContext roundContext = RoundContext.of(round, EnhancedProcessing.createContext(round.getProcessingEnvironment(),
					syntheticElementTemplateRegistry));
			{
				ApparatusRoundContext apparatusRoundContext = new ApparatusRoundContext(
						indexRegistry, registry, roundContext);
				modules.forEach(module -> module.processRound(apparatusRoundContext));
			}
			{
				indexRegistry.flush();
			}
		});
	}

	public ApparatusAnnotationProcessor withModule(ApparatusAnnotationProcessingModule module) {
		modules.add(module);
		return this;
	}
}
