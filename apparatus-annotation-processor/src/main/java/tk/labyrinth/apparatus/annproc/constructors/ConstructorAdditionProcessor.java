package tk.labyrinth.apparatus.annproc.constructors;

import com.sun.tools.javac.tree.JCTree;
import tk.labyrinth.apparatus.core.AdvancedTreeMaker;
import tk.labyrinth.apparatus.core.constructor.ConstructorAdditionDescriptor;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.sourcetree.scope.ElementNodeUtils;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

public class ConstructorAdditionProcessor {

	private final ProcessingContext processingContext;

	private final AdvancedTreeMaker treeMaker;

	public ConstructorAdditionProcessor(ProcessingContext processingContext) {
		this.processingContext = processingContext;
		this.treeMaker = new AdvancedTreeMaker(processingContext.getProcessingEnvironment());
	}

	public void process(ConstructorAdditionDescriptor constructorAdditionDescriptor) {
		TypeElementTemplate typeElementTemplate = processingContext.getTypeElementTemplate(constructorAdditionDescriptor.getParent());
		ClassNode classNode = ElementNodeUtils.getClass(typeElementTemplate);
		JCTree.JCClassDecl jcClassDecl = (JCTree.JCClassDecl) classNode.getClassTree();
		//
		JCTree.JCMethodDecl constructor = treeMaker.createConstructor(
				constructorAdditionDescriptor.getDeclaration(),
				constructorAdditionDescriptor.getBodyBuilderOrFail().buildBody(treeMaker));
		//
		treeMaker.addChild(jcClassDecl, constructor);
	}
}
