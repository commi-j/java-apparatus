package tk.labyrinth.apparatus.annproc.tool;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.core.tool.IndexSearcher;
import tk.labyrinth.apparatus.core.util.ResourceUtils;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.apparatus.index.OldIndexEntry;
import tk.labyrinth.misc4j2.java.io.IoUtils;

import javax.annotation.processing.Filer;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.net.URL;
import java.util.Objects;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class AnnprocIndexSearcher implements IndexSearcher {

	private final Filer filer;

	private final ObjectMapper objectMapper;

	private Stream<OldIndexEntry> readIndexEntries(URL url) {
		return ResourceUtils.readLines(url::openStream).stream()
				.map(line -> Objects.requireNonNull(IoUtils.unchecked(() ->
						objectMapper.readValue(line, NewIndexEntry.class))).toOldIndexEntry(url));
	}

	@Override
	public Stream<OldIndexEntry> search(String key) {
		String path = IndexSearcher.buildPath(key);
		Stream<URL> dependencyResourceUrls = ResourceUtils.getResourceUrls(path, getClass().getClassLoader());
		Stream<URL> outputResourceUrl = Objects.requireNonNull(IoUtils.unchecked(() -> {
			FileObject fileObject = filer.getResource(StandardLocation.CLASS_OUTPUT, "", path);
			return fileObject.getLastModified() != 0 ? Stream.of(fileObject.toUri().toURL()) : Stream.empty();
		}));
		return Stream.concat(dependencyResourceUrls, outputResourceUrl)
				.flatMap(this::readIndexEntries);
	}
}
