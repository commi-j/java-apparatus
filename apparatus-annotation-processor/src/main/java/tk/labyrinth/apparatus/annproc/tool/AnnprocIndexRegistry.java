package tk.labyrinth.apparatus.annproc.tool;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.apache.commons.io.output.CloseShieldWriter;
import tk.labyrinth.apparatus.core.tool.IndexEntryInvalidationDecider;
import tk.labyrinth.apparatus.core.tool.IndexSearcher;
import tk.labyrinth.apparatus.index.IndexRegistry;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.apparatus.index.OldIndexEntry;
import tk.labyrinth.misc4j2.java.io.IoUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnnprocIndexRegistry implements IndexRegistry {

	private final Map<String, List<NewIndexEntry>> currentCache = new HashMap<>();

	@Getter
	private final List<IndexEntryInvalidationDecider> indexEntryInvalidationDeciders = new ArrayList<>();

	private final IndexSearcher indexSearcher;

	private final ObjectMapper objectMapper;

	private final Map<String, List<OldIndexEntry>> previousCache = new HashMap<>();

	private final ProcessingEnvironment processingEnvironment;

	public AnnprocIndexRegistry(ObjectMapper objectMapper, ProcessingEnvironment processingEnvironment) {
		this.objectMapper = objectMapper;
		this.processingEnvironment = processingEnvironment;
		indexSearcher = new AnnprocIndexSearcher(processingEnvironment.getFiler(), objectMapper);
	}

	@Override
	public void add(NewIndexEntry newIndexEntry) {
		currentCache.computeIfAbsent(newIndexEntry.getKey(), key -> new ArrayList<>()).add(newIndexEntry);
	}

	public void addIndexEntryInvalidationDecider(IndexEntryInvalidationDecider indexEntryInvalidationDecider) {
		indexEntryInvalidationDeciders.add(indexEntryInvalidationDecider);
	}

	@Override
	public void flush() {
		currentCache.forEach((key, newIndexEntries) -> IoUtils.unchecked(() -> {
			// FIXME: Currently we load entries from deps but filter them out by url, we don't need to do it.
			// FIXME: Currently we also receives new entries but filter them out as they don't have urls.
			//  Need to replace it with smth smarter.
			List<OldIndexEntry> loadedEntries = get(key)
					.collect(Collectors.toList());
			//
			FileObject destination = processingEnvironment.getFiler().createResource(
					StandardLocation.CLASS_OUTPUT, "", IndexSearcher.buildPath(key));
			URL destinationUrl = destination.toUri().toURL();
			//
			List<NewIndexEntry> entriesToRestore = loadedEntries.stream()
					.filter(loadedEntry -> Objects.equals(loadedEntry.getLocation(), destinationUrl))
					.map(OldIndexEntry::toNewIndexEntry)
					.collect(Collectors.toList());
			//
			IoUtils.tryWithResource(
					new OutputStreamWriter(destination.openOutputStream(), StandardCharsets.UTF_8),
					writer -> Stream.concat(entriesToRestore.stream(), newIndexEntries.stream())
							// FIXME: Add sorting (find the algorithm to sort).
							.forEach(newIndexEntry -> IoUtils.unchecked(() -> {
								// JsonGenerator created for each single write closes provided Writer,
								// so we need to shield it. See ObjectMapper#writeValue's javadoc.
								objectMapper.writeValue(new CloseShieldWriter(writer), newIndexEntry);
								writer.append('\n');
							})));
		}));
	}

	@Override
	public Stream<OldIndexEntry> get(String key) {
		return Stream.concat(
				currentCache.getOrDefault(key, List.of()).stream()
						.map(NewIndexEntry::toOldIndexEntry),
				previousCache.computeIfAbsent(key, cacheKey -> indexSearcher.search(key)
						.filter(indexEntry -> indexEntryInvalidationDeciders.stream().noneMatch(indexEntryInvalidationDecider ->
								indexEntryInvalidationDecider.shouldInvalidate(indexEntry)))
						.collect(Collectors.toList())).stream());
	}
}
