package tk.labyrinth.apparatus.annproc.methodinvocations;

import com.sun.source.tree.Tree;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.List;
import tk.labyrinth.apparatus.core.methodinvocation.MethodInvocationManipulationRequest;
import tk.labyrinth.apparatus.core.methodinvocation.ResolvedMethodInvocationReplacementDescriptor;
import tk.labyrinth.apparatus.javac.util.TreeMakerUtils;
import tk.labyrinth.apparatus.model.node.ExpressionNode;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.MethodInvocationNode;
import tk.labyrinth.apparatus.model.node.common.MethodReferencingNode;
import tk.labyrinth.apparatus.sourcetree.model.Entity;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.misc4j2.collectoin.StreamUtils;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

public class MethodInvocationReplacementProcessor {

	void processMemberReference(ResolvedMethodInvocationReplacementDescriptor replacementDescriptor, MemberReferenceNode node) {
		JCTree.JCLambda replacement;
		{
			TreeMaker treeMaker;
			JavacElements javacElements;
			{
				JavacProcessingEnvironment javacProcessingEnvironment = (JavacProcessingEnvironment)
						node.getNodeContext().getTreeContext().getProcessingEnvironment();
				treeMaker = TreeMaker.instance(javacProcessingEnvironment.getContext());
				javacElements = javacProcessingEnvironment.getElementUtils();
			}
			//
			List<String> lambdaVariableNames;
			List<JCTree.JCExpression> lambdaExpressionArguments;
			{
				Entity targetEntity = findMemberReferenceTargetEntity(node.getTargetNode());
				if (targetEntity.isTypeElement()) {
					// Lambda will have receiver of target type:
					//  "Type::method" -> "(it)->Extensions.method(it)"
					// FIXME: Speculatively presume it can only occur when there are no other parameters in method. Need to check for sure.
					lambdaVariableNames = List.of(replacementDescriptor.getTo().getFormalParameter(0).getSimpleNameAsString());
					lambdaExpressionArguments = List.of(TreeMakerUtils.createIdentifier(treeMaker, javacElements, lambdaVariableNames.get(0)));
				} else {
					// Lambda will invoke extension method with target as first argument:
					//  "instance::method" -> "()->Extensions.method(instance)"
					//  "instance::method" -> "(a,b)->Extensions.method(instance,a,b)"
					lambdaVariableNames = replacementDescriptor.getTo().getFormalParameters()
							.skip(1)
							.map(FormalParameterElementTemplate::getSimpleNameAsString)
							.collect(List.collector());
					lambdaExpressionArguments = StreamUtils
							.concat(
									(JCTree.JCExpression) node.getTargetNode().getExpressionTree(),
									lambdaVariableNames.stream().map(parameterName ->
											TreeMakerUtils.createIdentifier(treeMaker, javacElements, parameterName)))
							.collect(List.collector());
				}
			}
			//
			// FIXME: Pass arguments to ArgumentResolvers.
			replacement = treeMaker.Lambda(
					TreeMakerUtils.createLambdaVariables(treeMaker, javacElements, lambdaVariableNames.stream()),
					treeMaker.App(
							treeMaker.QualIdent((Symbol) replacementDescriptor.getTo().getExecutableElement()),
							lambdaExpressionArguments));
		}
		{
			Tree originParent = node.getParentNode().getTree();
			JCTree replaced;
			if (originParent instanceof JCTree.JCAssign) {
				JCTree.JCAssign jcAssign = (JCTree.JCAssign) originParent;
				replaced = jcAssign.rhs;
				jcAssign.rhs = replacement;
			} else if (originParent instanceof JCTree.JCMethodInvocation) {
				JCTree.JCMethodInvocation jcMethodInvocation = (JCTree.JCMethodInvocation) originParent;
				// FIXME: Support multiple parameters.
				replaced = jcMethodInvocation.args.get(0);
				jcMethodInvocation.args = List.of(replacement);
			} else if (originParent instanceof JCTree.JCTypeCast) {
				JCTree.JCTypeCast jcTypeCast = (JCTree.JCTypeCast) originParent;
				replaced = jcTypeCast.expr;
				jcTypeCast.expr = replacement;
			} else if (originParent instanceof JCTree.JCVariableDecl) {
				JCTree.JCVariableDecl jcVariableDecl = (JCTree.JCVariableDecl) originParent;
				replaced = jcVariableDecl.init;
				jcVariableDecl.init = replacement;
			} else {
				throw new NotImplementedException(ExceptionUtils.render(originParent));
			}
			{
				// FIXME: This is to avoid failure at com.sun.tools.javac.comp.Flow:2510
				replacement.params.forEach(param -> param.pos = replaced.pos);
			}
		}
	}

	void processMethodInvocation(ResolvedMethodInvocationReplacementDescriptor replacementDescriptor, MethodInvocationNode node) {
		JavacProcessingEnvironment javacProcessingEnvironment = (JavacProcessingEnvironment)
				node.getNodeContext().getTreeContext().getProcessingEnvironment();
		TreeMaker treeMaker = TreeMaker.instance(javacProcessingEnvironment.getContext());
		//
		JCTree.JCMethodInvocation replacement = treeMaker.App(
				treeMaker.QualIdent((Symbol) replacementDescriptor.getTo().getExecutableElement()),
				replacementDescriptor.getArgumentDescriptors().stream()
						.map(argumentDescriptor -> argumentDescriptor.resolve(node))
						.map(JCTree.JCExpression.class::cast)
						.collect(List.collector()));
		JCTree.JCMethodInvocation origin = (JCTree.JCMethodInvocation) node.getMethodInvocationTree();
		origin.args = replacement.args;
		origin.meth = replacement.meth;
	}

	public void process(MethodInvocationManipulationRequest request) {
		ResolvedMethodInvocationReplacementDescriptor replacementDescriptor = request.getManipulation();
		MethodReferencingNode node = request.getNode();
		if (node instanceof MemberReferenceNode) {
			processMemberReference(replacementDescriptor, (MemberReferenceNode) node);
		} else if (node instanceof MethodInvocationNode) {
			processMethodInvocation(replacementDescriptor, (MethodInvocationNode) node);
		} else {
			throw new NotImplementedException(ExceptionUtils.render(request));
		}
	}

	private static Entity findMemberReferenceTargetEntity(ExpressionNode target) {
		Entity result;
		{
			if (target.isIdentifier()) {
				result = target.asIdentifier().findEntity();
			} else if (target.isMemberSelect()) {
				ElementTemplate elementTemplate = target.asMemberSelect().findElement();
				result = elementTemplate != null ? Entity.ofElement(elementTemplate.getElement()) : null;
			} else {
				throw new NotImplementedException(ExceptionUtils.render(target));
			}
		}
		return result;
	}
}
