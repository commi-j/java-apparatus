package tk.labyrinth.apparatus.annproc.tool;

import tk.labyrinth.apparatus.core.tool.ApparatusServiceLoader;

import java.util.ServiceLoader;
import java.util.stream.Stream;

public class AnnprocServiceLoader implements ApparatusServiceLoader {

	@Override
	public <T> Stream<T> load(Class<T> type) {
		return ServiceLoader.load(type, type.getClassLoader()).stream()
				.map(ServiceLoader.Provider::get);
	}
}
