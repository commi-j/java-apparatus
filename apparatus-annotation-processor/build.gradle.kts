@file:Suppress("UNCHECKED_CAST")

dependencies {
	val versionAutoService = "1.0-rc6"
	//
	api(project(":apparatus-core"))
	//
	compileOnly("com.google.auto.service", "auto-service", versionAutoService)
	annotationProcessor("com.google.auto.service", "auto-service", versionAutoService)
}
//
(extra["addJavacExportsToCompiler"] as () -> Unit).invoke()
(extra["configurePublishingToBintray"] as () -> Unit).invoke()
