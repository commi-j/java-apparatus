package tk.labyrinth.apparatus.extensionmethods.priv;

import tk.labyrinth.apparatus.core.method.ArgumentMethodElementDescriptor;
import tk.labyrinth.apparatus.core.method.TargetMethodElementDescriptor;
import tk.labyrinth.apparatus.core.methodinvocation.MethodInvocationReplacementDescriptor;
import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.misc4j2.collectoin.ListUtils;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ExtensionMethodsUtils {

	public static MethodInvocationReplacementDescriptor buildMethodInvocationReplacementDescriptor(MethodElementTemplate extensionMethod) {
		return new MethodInvocationReplacementDescriptor(
				ListUtils.collect(
						new TargetMethodElementDescriptor(),
						IntStream.range(1, extensionMethod.getFormalParameterCount())
								.mapToObj(index -> new ArgumentMethodElementDescriptor(index - 1))),
				createAddedMethodSignature(extensionMethod),
				extensionMethod.getFullSignature());
	}

	public static Stream<MethodElementTemplate> collectExtensionMethods(RoundContext roundContext) {
		return roundContext.getAllTypeElements()
				.flatMap(TypeElementTemplate::getDeclaredMethods)
				.filter(declaredMethod -> declaredMethod.hasOldMergedAnnotation(ExtensionMethod.class));
	}

	public static MethodFullSignature createAddedMethodSignature(MethodElementTemplate extensionMethod) {
		String parent = resolveTargetTypeElement(extensionMethod).getSignature().toString();
		String name = extensionMethod.getSimpleNameAsString();
		String parameters = extensionMethod.getFormalParameters()
				.skip(1)
				.map(FormalParameterElementTemplate::getType)
				.map(TypeHandle::getErasureString)
				.collect(Collectors.joining(","));
		// FIXME: Use method where we don't need to concat strings.
		return MethodFullSignature.of(parent + "#" + name + "(" + parameters + ")");
	}

	public static TypeElementTemplate resolveTargetTypeElement(MethodElementTemplate extensionMethod) {
		TypeElementTemplate result;
		{
			TypeHandle typeHandle = extensionMethod.getFormalParameter(0).getType();
			if (typeHandle.isDeclaredType()) {
				result = typeHandle.asDeclaredType().toElement();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
