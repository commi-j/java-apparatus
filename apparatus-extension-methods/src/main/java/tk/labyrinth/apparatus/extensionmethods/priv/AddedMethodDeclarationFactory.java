package tk.labyrinth.apparatus.extensionmethods.priv;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.apparatus.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.declaration.TypeParameterDeclaration;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddedMethodDeclarationFactory {

	public static TypeDescription adjustTypeDescription(
			Map<String, Pair<Boolean, String>> typeParameterMappings,
			TypeDescription typeDescription) {
		return TypeDescription.of(typeParameterMappings.entrySet().stream().reduce(
				typeDescription.toString(),
				(typeDescriptionString, entry) -> typeDescriptionString.replace(
						entry.getKey(),
						entry.getValue().getRight()),
				FunctionUtils::throwUnreachableStateException));
	}

	public static MethodDeclaration create(MethodElementTemplate extensionMethod) {
		TypeHandle originalMethodFirstFormalParameterType = extensionMethod.getFormalParameter(0).getType();
		String addedMethodSignature = ExtensionMethodsUtils.createAddedMethodSignature(extensionMethod).toString();
		//
		// Entry here means we should remove key from list of method type parameters
		// and replace all references to it with value.
		Map<String, Pair<Boolean, String>> typeParameterMappings;
		if (originalMethodFirstFormalParameterType.isParameterizedType()) {
			// public <E> void foo(List<E> self);
			//
			// If we have TypeParameters on first parameter that matches ones on it's type,
			// they are to be rewired and corresponding VariableTypes are to be discarded.
			//
			ParameterizedTypeHandle parameterizedTypeHandle = originalMethodFirstFormalParameterType.asParameterizedType();
			typeParameterMappings = createTypeParameterMappings(
					extensionMethod.getTypeParameters().collect(Collectors.toList()),
					parameterizedTypeHandle,
					addedMethodSignature);
			//
			// If variables declared at method level matches any of these they should be discarded.
		} else if (originalMethodFirstFormalParameterType.isVariableType()) {
			// public <T> void foo(T self);
			//
			throw new NotImplementedException();
		} else {
			// No VariableType affection.
			//
			typeParameterMappings = Map.of();
		}
		List<FormalParameterDeclaration> formalParameters = createFormalParameterDeclarations(
				typeParameterMappings,
				extensionMethod.getFormalParameters());
		//
		return MethodDeclaration.builder()
				.formalParameters(formalParameters)
				.modifiers(List.of(JavaMethodModifier.PUBLIC))
				.name(extensionMethod.getSimpleNameAsString())
				.returnType(AddedMethodDeclarationFactory.adjustTypeDescription(
						typeParameterMappings,
						extensionMethod.getReturnType().getDescription()))
				.typeParameters(createTypeParameterDeclarations(
						typeParameterMappings,
						extensionMethod.getTypeParameters()))
				.build();
	}

	public static List<FormalParameterDeclaration> createFormalParameterDeclarations(
			Map<String, Pair<Boolean, String>> typeParameterMappings,
			Stream<FormalParameterElementTemplate> formalParameterElements) {
		return formalParameterElements
				.skip(1)
				.map(FormalParameterElementTemplate::getDeclaration)
				.map(declaration -> declaration.toBuilder()
						.type(adjustTypeDescription(typeParameterMappings, declaration.getType()))
						.build())
				.collect(Collectors.toList());
	}

	public static String createSignature(String parent, String simpleName, Stream<String> parameters) {
		return parent + "#" + simpleName + "(" + parameters.collect(Collectors.joining(",")) + ")";
	}

	public static List<TypeParameterDeclaration> createTypeParameterDeclarations(
			Map<String, Pair<Boolean, String>> typeParameterMappings,
			Stream<TypeParameterElementTemplate> typeParameterElements) {
		return typeParameterElements
				.filter(typeParameterElement ->
						!typeParameterMappings.get(typeParameterElement.getSignature()).getLeft())
				.map(typeParameterElement -> TypeParameterDeclaration.builder()
						.boundTypes(typeParameterElement.getBounds().stream()
								.map(TypeHandle::getDescription)
								.map(typeDescription -> adjustTypeDescription(typeParameterMappings, typeDescription))
								.collect(Collectors.toList()))
						.name(typeParameterElement.getSimpleNameAsString())
						.build())
				.collect(Collectors.toList());
	}

	/**
	 * Return model:<br>
	 * - K - TypeParameter of original method;<br>
	 * - V.L - whether this parameter is consumed by addedMethod's parent;<br>
	 * - V.R - TypeParameter to map, either owned by addedMethod or it's parent;<br>
	 *
	 * @param originalMethodTypeParameterElements    non-null
	 * @param originalMethodFirstFormalParameterType non-null
	 * @param addedMethodSignature                   non-null
	 *
	 * @return non-null
	 */
	public static Map<String, Pair<Boolean, String>> createTypeParameterMappings(
			List<TypeParameterElementTemplate> originalMethodTypeParameterElements,
			ParameterizedTypeHandle originalMethodFirstFormalParameterType,
			String addedMethodSignature) {
		Map<String, Pair<Boolean, String>> result = new HashMap<>();
		{
			TypeElementTemplate extensionTargetTypeElement = originalMethodFirstFormalParameterType.toElement();
			//
			Set<TypeParameterElementTemplate> consumedTypeParameterElements = new HashSet<>();
			for (int i = 0; i < originalMethodFirstFormalParameterType.getParameterCount(); i++) {
				TypeHandle parameter = originalMethodFirstFormalParameterType.getParameter(i);
				if (parameter.isVariableType()) {
					TypeParameterElementTemplate typeToExclude = parameter.asVariableType().toElement();
					result.put(
							typeToExclude.getSignature(),
							Pair.of(true, extensionTargetTypeElement.getTypeParameter(i).getSignature()));
					consumedTypeParameterElements.add(typeToExclude);
				}
			}
			originalMethodTypeParameterElements.stream()
					.filter(typeParameterElement -> !consumedTypeParameterElements.contains(typeParameterElement))
					.forEach(typeParameterElement -> result.put(
							typeParameterElement.getSignature(),
							Pair.of(false, addedMethodSignature +
									SignatureSeparators.TYPE_PARAMETER +
									typeParameterElement.getSimpleNameAsString())));
		}
		return result;
	}
}
