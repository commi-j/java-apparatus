package tk.labyrinth.apparatus.extensionmethods.priv;

import com.google.auto.service.AutoService;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.core.module.ApparatusRoundContext;
import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.apparatus.index.OldIndexEntry;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import javax.tools.Diagnostic;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@AutoService(ApparatusAnnotationProcessingModule.class)
public class ApparatusExtensionMethodsModule implements ApparatusAnnotationProcessingModule {

	@Override
	public void processRound(ApparatusRoundContext context) {
		if (context.getRoundContext().getRound().isFirst()) {
			{
				List<MethodElementTemplate> validExtensionMethods = new ArrayList<>();
				List<Pair<MethodElementTemplate, List<String>>> invalidExtensionMethods = new ArrayList<>();
				//
				ExtensionMethodsUtils.collectExtensionMethods(context.getRoundContext()).forEach(extensionMethod -> {
					List<String> inconsistencies = ExtensionMethodsValidator.validate(extensionMethod);
					if (inconsistencies == null) {
						validExtensionMethods.add(extensionMethod);
					} else {
						invalidExtensionMethods.add(Pair.of(extensionMethod, inconsistencies));
					}
				});
				//
				// FIXME: Use specific type-safe registry.
				validExtensionMethods.forEach(extensionMethod -> context.getIndexRegistry().add(new NewIndexEntry(
						ExtensionMethod.class.getName(),
						getClass().getSimpleName(),
						extensionMethod.getTopLevelTypeElement().getSignature(),
						extensionMethod.getFullSignature().toString())));
				//
				validExtensionMethods.forEach(extensionMethod -> context.getRegistry()
						.getMethodAdditionDescriptorRegistry()
						.add(
								this,
								extensionMethod.getTopLevelTypeElement(),
								AddedMethodAdditionDescriptorFactory.create(extensionMethod)));
				//
				invalidExtensionMethods.forEach(pair -> context.getMessager().printMessage(
						Diagnostic.Kind.WARNING,
						pair.getValue().toString(),
						pair.getKey().getExecutableElement()));
			}
			{
				List<MethodElementTemplate> extensionMethods = context.getIndexRegistry().get(ExtensionMethod.class)
						.map(OldIndexEntry::getValue)
						.map(fullMethodSignature -> context.getRoundContext().getProcessingContext()
								// FIXME: Add method to use String directly.
								.findMethodElementTemplate(MethodFullSignature.of(fullMethodSignature)))
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
				//
				extensionMethods.forEach(extensionMethod -> context.getRegistry()
						.getMethodInvocationReplacementDescriptorRegistry()
						.add(
								this,
								extensionMethod.getTopLevelTypeElement(),
								ExtensionMethodsUtils.buildMethodInvocationReplacementDescriptor(extensionMethod)));
			}
		}
	}
}
