package tk.labyrinth.apparatus.extensionmethods.priv;

import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class ExtensionMethodsValidator {

	/**
	 * @param extensionMethod non-null
	 *
	 * @return null or non-empty
	 */
	@Nullable
	public static List<String> validate(MethodElementTemplate extensionMethod) {
		List<String> inconsistencies = new ArrayList<>();
		{
			if (extensionMethod.isEffectivelyNonPublic()) {
				inconsistencies.add("Require extensionMethod.effectivelyPublic == true");
			}
			if (extensionMethod.isEffectivelyNonStatic()) {
				inconsistencies.add("Require extensionMethod.effectivelyStatic == true");
			}
			if (extensionMethod.getFormalParameterCount() < 1) {
				inconsistencies.add("Require extensionMethod.formalParameterCount >= 1");
			}
		}
		return inconsistencies.isEmpty() ? null : inconsistencies;
	}
}
