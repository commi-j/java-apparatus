package tk.labyrinth.apparatus.extensionmethods.priv;

import tk.labyrinth.apparatus.core.method.MethodAdditionDescriptor;
import tk.labyrinth.jaap.model.ElementSignature;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;

import java.util.List;

public class AddedMethodAdditionDescriptorFactory {

	public static MethodAdditionDescriptor create(MethodElementTemplate extensionMethod) {
		{
			List<String> inconsistencies = ExtensionMethodsValidator.validate(extensionMethod);
			if (inconsistencies != null) {
				throw new IllegalArgumentException("Inconsistencies = " + inconsistencies + ", " +
						"extensionMethod = " + extensionMethod);
			}
		}
		//
		String parentSignature;
		{
			// TODO: Support where firstParameter is Variable and we should infer type from TypeParameter bounds.
			parentSignature = extensionMethod.getFormalParameter(0).getType().getErasureString();
		}
		MethodDeclaration addedMethodDeclaration = AddedMethodDeclarationFactory.create(extensionMethod);
		//
		return new MethodAdditionDescriptor(
				null,
				addedMethodDeclaration,
				CanonicalTypeSignature.ofValid(parentSignature),
				// FIXME: Use MethodFullSignature to ElementSignature.
				ElementSignature.ofValid(extensionMethod.getFullSignature().toString()));
	}
}
