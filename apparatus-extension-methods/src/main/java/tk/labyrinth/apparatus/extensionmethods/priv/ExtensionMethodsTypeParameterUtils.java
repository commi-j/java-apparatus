package tk.labyrinth.apparatus.extensionmethods.priv;

import tk.labyrinth.jaap.handle.type.ParameterizedTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import java.util.HashMap;
import java.util.Map;

public class ExtensionMethodsTypeParameterUtils {

	public static Map<TypeParameterElementTemplate, TypeParameterElementTemplate> createStaticToInstanceMappings(
			ParameterizedTypeHandle extensionTargetType) {
		Map<TypeParameterElementTemplate, TypeParameterElementTemplate> result = new HashMap<>();
		{
			TypeElementTemplate extensionTargetTypeElement = extensionTargetType.toElement();
			//
			for (int i = 0; i < extensionTargetType.getParameterCount(); i++) {
				TypeHandle parameter = extensionTargetType.getParameter(i);
				if (parameter.isVariableType()) {
					result.put(parameter.asVariableType().toElement(), extensionTargetTypeElement.getTypeParameter(i));
				}
			}
		}
		return result;
	}
}
