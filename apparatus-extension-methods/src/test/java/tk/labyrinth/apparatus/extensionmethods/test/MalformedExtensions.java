package tk.labyrinth.apparatus.extensionmethods.test;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

public class MalformedExtensions {

	@ExtensionMethod
	void allInOne() {
		// no-op
	}

	@ExtensionMethod
	public void notStatic(String it) {
		// no-op
	}

	@ExtensionMethod
	static void notPublic(String it) {
		// no-op
	}

	@ExtensionMethod
	public static void noParameters() {
		// no-op
	}
}
