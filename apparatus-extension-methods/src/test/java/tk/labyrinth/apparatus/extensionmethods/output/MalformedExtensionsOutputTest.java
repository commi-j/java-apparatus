package tk.labyrinth.apparatus.extensionmethods.output;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.extensionmethods.test.MalformedExtensions;
import tk.labyrinth.apparatus.testing.compile.CompilerAwareTestBase;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.misc4j2.lib.junit5.ContribAssertions;

import java.util.List;

public class MalformedExtensionsOutputTest extends CompilerAwareTestBase {

	@Test
	void testOutput() {
		List<String> output = getTestCompiler().run(
				CompilationTarget.builder()
						.sourceTypes(MalformedExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
		//
		ContribAssertions.assertEquals(
				List.of(
						"src/test/java/tk/labyrinth/apparatus/extensionmethods/test/MalformedExtensions.java:8: warning: [Require extensionMethod.effectivelyPublic == true, Require extensionMethod.effectivelyStatic == true, Require extensionMethod.formalParameterCount >= 1]",
						"\tvoid allInOne() {",
						"\t     ^",
						"src/test/java/tk/labyrinth/apparatus/extensionmethods/test/MalformedExtensions.java:13: warning: [Require extensionMethod.effectivelyStatic == true]",
						"\tpublic void notStatic(String it) {",
						"\t            ^",
						"src/test/java/tk/labyrinth/apparatus/extensionmethods/test/MalformedExtensions.java:18: warning: [Require extensionMethod.effectivelyPublic == true]",
						"\tstatic void notPublic(String it) {",
						"\t            ^",
						"src/test/java/tk/labyrinth/apparatus/extensionmethods/test/MalformedExtensions.java:23: warning: [Require extensionMethod.formalParameterCount >= 1]",
						"\tpublic static void noParameters() {",
						"\t                   ^",
						"4 warnings"),
				output.stream().map(line -> line.replaceAll("\\\\", "/")));
		Assertions.assertFalse(getTestCompiler().getOutputDirectory().resolve("META-INF").toFile().exists());
	}
}
