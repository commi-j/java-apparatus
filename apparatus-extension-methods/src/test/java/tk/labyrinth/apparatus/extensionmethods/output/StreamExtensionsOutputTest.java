package tk.labyrinth.apparatus.extensionmethods.output;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.extensionmethods.test.StreamExtensions;
import tk.labyrinth.apparatus.misc4j.lib.junit5.FileAssertions;
import tk.labyrinth.apparatus.testing.compile.CompilerAwareTestBase;
import tk.labyrinth.jaap.core.CompilationTarget;

import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;

public class StreamExtensionsOutputTest extends CompilerAwareTestBase {

	@Test
	void testOutput(TestInfo testInfo) {
		getTestCompiler().run(
				CompilationTarget.builder()
						.sourceTypes(StreamExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
		//
		String path = "META-INF";
		FileAssertions.assertContentEquals(
				getTestCompilerOutputDirectory(testInfo).resolve(path),
				getTestCompiler().getOutputDirectory().resolve(path));
	}

	private static Path getTestCompilerOutputDirectory(TestInfo testInfo) {
		Method method = testInfo.getTestMethod().orElseThrow();
		Class<?> declaringClass = method.getDeclaringClass();
		//
		Path result;
		{
			URL url = ClassLoader.getSystemResource(
					declaringClass.getPackageName().replace(".", "/") + "/" +
							declaringClass.getSimpleName() + "#" + method.getName());
			URI uri;
			try {
				uri = url.toURI();
			} catch (URISyntaxException ex) {
				// FIXME: URL to URI mapping.
				throw new RuntimeException(ex);
			}
			result = Path.of(uri);
		}
		return result;
	}
}
