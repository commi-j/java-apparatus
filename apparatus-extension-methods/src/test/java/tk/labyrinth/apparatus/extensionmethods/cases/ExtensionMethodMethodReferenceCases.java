package tk.labyrinth.apparatus.extensionmethods.cases;

import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;

import java.util.List;

public class ExtensionMethodMethodReferenceCases {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testInstanceMethodReferences() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("java/InstanceMethodReferences.java"))
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testTypeMethodReferences() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("java/TypeMethodReferences.java"))
						.build(),
				new ApparatusAnnotationProcessor());
	}
}
