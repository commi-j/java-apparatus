package tk.labyrinth.apparatus.extensionmethods.cases;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.extensionmethods.test.TestExtensions;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;

import java.util.List;

public class InvocationTests {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	/**
	 * No replacements here. Just checking it has no errors.
	 */
	@Test
	void testExtensionMethodInvocation() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("ExtensionMethodInvocation.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	/**
	 * No replacements here. Just checking it has no errors.
	 */
	@Test
	void testExtensionMethodInvocationWithLiteralParameter() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("ExtensionMethodInvocationWithLiteralParameter.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	/**
	 * No replacements here. Just checking it has no errors.
	 */
	@Test
	void testExtensionMethodInvocationWithSamePackage() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("ExtensionMethodInvocationWithSamePackage.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testInvocationOnExtensionMethod() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnExtensionMethod.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testInvocationOnLiteral() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnLiteral.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Disabled
	@Test
	void testInvocationOnLiteralWithClassNameMismatch() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnLiteralWithClassNameMismatch.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testInvocationOnLiteralWithLocalVariableParameter() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnLiteralWithLocalVariableParameter.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testInvocationOnLocalVariable() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnLocalVariable.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testInvocationOnMethodInvocation() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnMethodInvocation.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testInvocationOnNewClass() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnNewClass.java"))
						.sourceTypes(TestExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
	}

	@Test
	void testInvocationsOnImplicitTarget() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of(
								"tk/labyrinth/apparatus/extensionmethods/resource/InvocationsOnImplicitTarget.java",
								"tk/labyrinth/apparatus/extensionmethods/resource/ext/TestExtensionsForImplicitTarget.java"))
						.build(),
				new ApparatusAnnotationProcessor());
	}
}
