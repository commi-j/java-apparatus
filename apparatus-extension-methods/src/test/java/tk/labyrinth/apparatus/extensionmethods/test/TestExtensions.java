package tk.labyrinth.apparatus.extensionmethods.test;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class TestExtensions {

	@ExtensionMethod
	public static void printSelf(Object it) {
		Objects.requireNonNull(it, "it");
		//
		System.out.println(it);
	}

	@ExtensionMethod
	public static void printSelf(Object it, String format) {
		Objects.requireNonNull(it, "it");
		//
		System.out.println(String.format(format, it));
	}

	@ExtensionMethod
	public static String toKebabCase(String it) {
		Objects.requireNonNull(it, "it");
		//
		return it.toLowerCase().replaceAll("[\\s_]", "-");
	}

	@ExtensionMethod
	public static String toSnakeCase(String it) {
		Objects.requireNonNull(it, "it");
		//
		return it.toLowerCase().replaceAll("[\\s-]", "_");
	}
}
