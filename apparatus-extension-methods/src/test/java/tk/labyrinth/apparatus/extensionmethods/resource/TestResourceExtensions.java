package tk.labyrinth.apparatus.extensionmethods.resource;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.util.Objects;

public class TestResourceExtensions {

	@ExtensionMethod
	public static void printSelf(Object it) {
		Objects.requireNonNull(it, "it");
		//
		System.out.println(it);
	}

	@ExtensionMethod
	public static void printSelf(Object it, String format) {
		Objects.requireNonNull(it, "it");
		//
		System.out.println(String.format(format, it));
	}
}
