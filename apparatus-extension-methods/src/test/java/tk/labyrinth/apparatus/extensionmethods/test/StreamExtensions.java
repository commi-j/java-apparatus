package tk.labyrinth.apparatus.extensionmethods.test;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExtensions {

	@ExtensionMethod
	public static <T> List<T> collectToList(Stream<T> it) {
		Objects.requireNonNull(it, "it");
		//
		return it.collect(Collectors.toList());
	}

	@ExtensionMethod
	public static <T, R extends T> Stream<R> filterMap(Stream<T> it, Class<R> type) {
		Objects.requireNonNull(it, "it");
		//
		return it.filter(type::isInstance).map(type::cast);
	}
}
