package tk.labyrinth.apparatus.extensionmethods.priv;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.extensionmethods.test.StreamExtensions;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.JavaMethodModifier;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.declaration.TypeParameterDeclaration;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class AddedMethodDeclarationFactoryTest {

	@Test
	void testCreate(ProcessingContext processingContext) {
		Assertions.assertEquals(
				MethodDeclaration.builder()
						.modifiers(List.of(
								JavaMethodModifier.PUBLIC))
						.name("collectToList")
						.returnType(TypeDescription.of("java.util.List<java.util.stream.Stream%T>"))
						.build(),
				AddedMethodDeclarationFactory.create(
						processingContext.getMethodElementTemplateByName(
								StreamExtensions.class, "collectToList")));
		Assertions.assertEquals(
				MethodDeclaration.builder()
						.formalParameters(List.of(
								FormalParameterDeclaration.builder()
										.name("type")
										.type(TypeDescription.of("java.lang.Class<java.util.stream.Stream#filterMap(java.lang.Class)%R>"))
										.build()))
						.modifiers(List.of(
								JavaMethodModifier.PUBLIC))
						.name("filterMap")
						.returnType(TypeDescription.of("java.util.stream.Stream<java.util.stream.Stream#filterMap(java.lang.Class)%R>"))
						.typeParameters(List.of(
								TypeParameterDeclaration.builder()
										.boundTypes(List.of(
												TypeDescription.of("java.util.stream.Stream%T")))
										.name("R")
										.build()))
						.build(),
				AddedMethodDeclarationFactory.create(
						processingContext.getMethodElementTemplateByName(
								StreamExtensions.class, "filterMap")));
	}
}