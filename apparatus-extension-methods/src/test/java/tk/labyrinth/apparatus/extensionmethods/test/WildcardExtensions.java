package tk.labyrinth.apparatus.extensionmethods.test;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.util.List;
import java.util.Objects;

public class WildcardExtensions {

	@ExtensionMethod
	public static List<? extends Number> doSmth(Number it) {
		Objects.requireNonNull(it, "it");
		//
		return List.of();
	}
}
