package tk.labyrinth.apparatus.extensionmethods.cases;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.extensionmethods.test.WildcardExtensions;
import tk.labyrinth.apparatus.testing.compile.CompilerAwareTestBase;
import tk.labyrinth.jaap.core.CompilationTarget;

import java.util.List;

public class WildcardReturnTypeCases extends CompilerAwareTestBase {

	@Test
	void testDoSmthWithNumberWildcard() {
		List<String> sourceFileNames = List.of(
				"j2a/wildcard/WildcardReturnTypeUsagesWithConstructor.java");
		//
		getTestCompiler().run(
				CompilationTarget.builder()
						.sourceResources(sourceFileNames)
						.sourceTypes(WildcardExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
		//
		Assertions.assertEquals(readExpectedDecompilerOutput(sourceFileNames), decompile(sourceFileNames));
	}
}
