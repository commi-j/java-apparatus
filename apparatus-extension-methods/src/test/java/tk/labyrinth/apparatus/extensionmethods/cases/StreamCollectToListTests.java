package tk.labyrinth.apparatus.extensionmethods.cases;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.extensionmethods.test.StreamExtensions;
import tk.labyrinth.apparatus.testing.compile.CompilerAwareTestBase;
import tk.labyrinth.jaap.core.CompilationTarget;

import java.util.List;

public class StreamCollectToListTests extends CompilerAwareTestBase {

	@Test
	void testWithField() {
		//
		// We can write class names here and add ".java" and ".class" later but
		// if we keep it this way IDEA helps navigate to these files (with ctrl+click).
		List<String> sourceFileNames = List.of(
				"j2a/streamtolist/StreamCollectToListUsagesWithField.java");
		//
		getTestCompiler().run(
				CompilationTarget.builder()
						.sourceResources(sourceFileNames)
						.sourceTypes(StreamExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
		//
		Assertions.assertEquals(readExpectedDecompilerOutput(sourceFileNames), decompile(sourceFileNames));
	}

	@Test
	void testWithLocalVariable() {
		//
		// We can write class names here and add ".java" and ".class" later but
		// if we keep it this way IDEA helps navigate to these files (with ctrl+click).
		List<String> sourceFileNames = List.of(
				"j2a/streamtolist/StreamCollectToListUsagesWithLocalVariable.java");
		//
		getTestCompiler().run(
				CompilationTarget.builder()
						.sourceResources(sourceFileNames)
						.sourceTypes(StreamExtensions.class)
						.build(),
				new ApparatusAnnotationProcessor());
		//
		Assertions.assertEquals(readExpectedDecompilerOutput(sourceFileNames), decompile(sourceFileNames));
	}
}
