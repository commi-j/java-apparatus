package tk.labyrinth.apparatus.annproc.methodinvocations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.core.method.MethodAdditionDescriptor;
import tk.labyrinth.apparatus.core.methodinvocation.MethodInvocationReplacementContext;
import tk.labyrinth.apparatus.extensionmethods.priv.AddedMethodAdditionDescriptorFactory;
import tk.labyrinth.apparatus.extensionmethods.priv.ExtensionMethodsUtils;
import tk.labyrinth.apparatus.model.node.MemberReferenceNode;
import tk.labyrinth.apparatus.model.node.Node;
import tk.labyrinth.apparatus.testing.TestNodeUtils;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.enhanced.EnhancedProcessing;
import tk.labyrinth.jaap.template.element.synthetic.SyntheticElementTemplateRegistry;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

@ExtendWithJaap
class MethodInvocationReplacementProcessorTest {

	@CompilationTarget(sourceResources = "java/BigDecimalPowMethodReference.java")
	@Test
	void testProcessMemberReference(AnnotationProcessingRound round) {
		RoundContext roundContext;
		MethodElementTemplate methodElementTemplate;
		{
			SyntheticElementTemplateRegistry syntheticElementTemplateRegistry = new SyntheticElementTemplateRegistry();
			roundContext = RoundContext.of(round, EnhancedProcessing.createContext(
					round.getProcessingEnvironment(), syntheticElementTemplateRegistry));
			//
			methodElementTemplate = roundContext.getProcessingContext().getMethodElementTemplate(
					"java.BigDecimalPowMethodReference#powExt(java.math.BigDecimal,int,java.math.MathContext)");
			{
				MethodAdditionDescriptor methodAdditionDescriptor = AddedMethodAdditionDescriptorFactory.create(
						methodElementTemplate);
				syntheticElementTemplateRegistry.registerMethodDeclaration(
						methodAdditionDescriptor.getParent(),
						methodAdditionDescriptor.getDeclaration());
			}
		}
		MemberReferenceNode node = TestNodeUtils.collectMemberReferences(roundContext)
				.skip(1)
				.findFirst().orElseThrow();
		//
		Node parentNode = node.getParentNode();
		//
		new MethodInvocationReplacementProcessor().processMemberReference(
				MethodInvocationReplacementContext.resolve(
						roundContext.getProcessingContext(),
						ExtensionMethodsUtils.buildMethodInvocationReplacementDescriptor(methodElementTemplate)),
				node);
		//
		Assertions.assertEquals("map.forEach((n,mc)->java.BigDecimalPowMethodReference.powExt(target, n, mc))",
				parentNode.toString());
	}
}
