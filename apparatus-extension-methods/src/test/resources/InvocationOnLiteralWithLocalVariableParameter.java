package tk.labyrinth.apparatus.extensionmethods.resource;

public class InvocationOnLiteralWithLocalVariableParameter {

	static {
		String pattern = "Text: %s";
		"Hello World".printSelf(pattern);
	}
}
