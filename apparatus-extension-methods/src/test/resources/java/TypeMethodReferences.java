package java;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.util.function.Consumer;

@SuppressWarnings("ALL")
public class TypeMethodReferences {

	static {
		{
			// Existing Methods
			//
			{
				// Method Invocation
				//
				consume(Target::doSmth);
				//
				consume(it -> it.doSmth());
			}
			{
				// Type Cast
				//
				Object consumer0 = (Consumer<Target>) Target::doSmth;
				//
				Object consumer1 = (Consumer<Target>) it -> it.doSmth();
			}
			{
				// Variable Assignment
				//
				Consumer<Target> consumer;
				//
				consumer = Target::doSmth;
				//
				consumer = it -> it.doSmth();
			}
			{
				// Variable Declaration
				//
				Consumer<Target> consumer0 = Target::doSmth;
				//
				Consumer<Target> consumer1 = it -> it.doSmth();
			}
		}
		{
			// Extension Methods
			//
			{
				// Method Invocation
				//
				consume(Target::doSmthExt);
				//
				consume(TypeMethodReferences::doSmthExt);
				consume(it -> TypeMethodReferences.doSmthExt(it));
			}
			{
				// Type Cast
				//
				Object consumer0 = (Consumer<Target>) Target::doSmthExt;
				//
				Object consumer1 = (Consumer<Target>) TypeMethodReferences::doSmthExt;
				Object consumer2 = (Consumer<Target>) it -> TypeMethodReferences.doSmthExt(it);
			}
			{
				// Variable Assignment
				//
				Consumer<Target> consumer;
				//
				consumer = Target::doSmthExt;
				//
				consumer = TypeMethodReferences::doSmthExt;
				consumer = it -> TypeMethodReferences.doSmthExt(it);
			}
			{
				// Variable Declaration
				//
				Consumer<Target> consumer0 = Target::doSmthExt;
				//
				Consumer<Target> consumer1 = TypeMethodReferences::doSmthExt;
				Consumer<Target> consumer2 = it -> TypeMethodReferences.doSmthExt(it);
			}
		}
	}

	static void consume(Consumer<Target> consumer) {
		// no-op
	}

	@ExtensionMethod
	public static void doSmthExt(Target it) {
		// no-op
	}

	public static class Target {

		public void doSmth() {
			// no-op
		}
	}
}
