package java;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;

@SuppressWarnings("ALL")
public class BigDecimalPowMethodReference {

	static {
		Map<Integer, MathContext> map = Map.of(3, MathContext.UNLIMITED);
		BigDecimal target = new BigDecimal("12");
		{
			map.forEach(target::pow);
			map.forEach((key, value) -> target.pow(key, value));
		}
		{
			map.forEach(target::powExt);
			map.forEach((key, value) -> BigDecimalPowMethodReference.powExt(target, key, value));
		}
	}

	@ExtensionMethod
	public static boolean powExt(BigDecimal it, int n, MathContext mc) {
		throw new NotImplementedException();
	}
}
