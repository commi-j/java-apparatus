package java;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

@SuppressWarnings("ALL")
public class InstanceMethodReferences {

	static {
		Target target = new Target();
		{
			// Existing Methods
			//
			{
				// No Arguments (Runnable, Supplier)
				//
				Runnable r0 = target::doNothing0;
				Runnable r1 = target::getVoid0;
				Supplier<Void> s0 = target::getVoid0;
				//
				Supplier<Void> s1 = () -> target.getVoid0();
			}
			{
				// One Argument (Consumer, Function, UnaryOperator)
				//
				Consumer<Void> c0 = target::doNothing1;
				Consumer<Void> c1 = target::getVoid1;
				Function<Void, Void> f = target::getVoid1;
				UnaryOperator<Void> o0 = target::getVoid1;
				//
				UnaryOperator<Void> o1 = v0 -> target.getVoid1(v0);
			}
			{
				// Two Arguments (BiConsumer, BiFunction, BinaryOperator)
				//
				BiConsumer<Void, Void> c0 = target::doNothing2;
				BiConsumer<Void, Void> c1 = target::getVoid2;
				BiFunction<Void, Void, Void> f = target::getVoid2;
				BinaryOperator<Void> o0 = target::getVoid2;
				//
				BinaryOperator<Void> o1 = (v0, v1) -> target.getVoid2(v0, v1);
			}
		}
		{
			// Extension Methods
			//
			{
				// No Arguments (Runnable, Supplier)
				//
				Runnable r0 = target::doNothing0Ext;
				Runnable r1 = target::getVoid0Ext;
				Supplier<Void> s0 = target::getVoid0Ext;
				//
				Supplier<Void> s1 = () -> InstanceMethodReferences.getVoid0Ext(target);
			}
			{
				// One Argument (Consumer, Function, UnaryOperator)
				//
				Consumer<Void> c0 = target::doNothing1Ext;
				Consumer<Void> c1 = target::getVoid1Ext;
				Function<Void, Void> f = target::getVoid1Ext;
				UnaryOperator<Void> o0 = target::getVoid1Ext;
				//
				UnaryOperator<Void> o1 = v0 -> InstanceMethodReferences.getVoid1Ext(target, v0);
			}
			{
				// Two Arguments (BiConsumer, BiFunction, BinaryOperator)
				//
				BiConsumer<Void, Void> c0 = target::doNothing2Ext;
				BiConsumer<Void, Void> c1 = target::getVoid2Ext;
				BiFunction<Void, Void, Void> f = target::getVoid2Ext;
				BinaryOperator<Void> o0 = target::getVoid2Ext;
				//
				BinaryOperator<Void> o1 = (v0, v1) -> InstanceMethodReferences.getVoid2Ext(target, v0, v1);
			}
		}
	}

	@ExtensionMethod
	public static void doNothing0Ext(Target it) {
		// no-op
	}

	@ExtensionMethod
	public static void doNothing1Ext(Target it, Void v0) {
		// no-op
	}

	@ExtensionMethod
	public static void doNothing2Ext(Target it, Void v0, Void v1) {
		// no-op
	}

	@ExtensionMethod
	public static Void getVoid0Ext(Target it) {
		return null;
	}

	@ExtensionMethod
	public static Void getVoid1Ext(Target it, Void v0) {
		return null;
	}

	@ExtensionMethod
	public static Void getVoid2Ext(Target it, Void v0, Void v1) {
		return null;
	}

	public static class Target {

		public void doNothing0() {
			// no-op
		}

		public void doNothing1(Void v0) {
			// no-op
		}

		public void doNothing2(Void v0, Void v1) {
			// no-op
		}

		public Void getVoid0() {
			return null;
		}

		public Void getVoid1(Void v0) {
			return null;
		}

		public Void getVoid2(Void v0, Void v1) {
			return null;
		}
	}
}
