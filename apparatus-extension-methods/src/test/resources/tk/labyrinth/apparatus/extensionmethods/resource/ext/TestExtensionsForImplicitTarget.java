package tk.labyrinth.apparatus.extensionmethods.resource.ext;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;
import tk.labyrinth.apparatus.extensionmethods.resource.InvocationsOnImplicitTarget;

import java.util.Objects;

public class TestExtensionsForImplicitTarget {

	@ExtensionMethod
	public static void printNested(InvocationsOnImplicitTarget.Nested it) {
		Objects.requireNonNull(it, "it");
		//
		System.out.println(it);
	}

	@ExtensionMethod
	public static void printNestedSuper(InvocationsOnImplicitTarget.NestedSuper it) {
		Objects.requireNonNull(it, "it");
		//
		System.out.println(it);
	}

	@ExtensionMethod
	public static void printParent(InvocationsOnImplicitTarget it) {
		Objects.requireNonNull(it, "it");
		//
		System.out.println(it);
	}
}
