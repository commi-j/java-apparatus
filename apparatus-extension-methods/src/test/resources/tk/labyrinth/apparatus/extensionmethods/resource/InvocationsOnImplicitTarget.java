package tk.labyrinth.apparatus.extensionmethods.resource;

public class InvocationsOnImplicitTarget {

	public class Nested extends NestedSuper {

		{
			printNested();
			printNestedSuper();
			printParent();
		}
	}

	public static class NestedSuper {
		// empty
	}
}
