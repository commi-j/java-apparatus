package j2a.streamtolist;

import java.util.List;
import java.util.stream.Stream;

public class StreamCollectToListUsagesWithField {

	private static final Stream<String> stream = Stream.of("a", "b");

	static {
		stream.collectToList();
		List<String> list = stream.collectToList();
	}
}
