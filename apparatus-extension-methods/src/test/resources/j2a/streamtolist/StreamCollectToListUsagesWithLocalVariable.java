package j2a.streamtolist;

import java.util.stream.Stream;

public class StreamCollectToListUsagesWithLocalVariable {

	static {
		Stream<String> stream = Stream.of("a", "b");
		stream.collectToList();
	}
}
