/*
 * Decompiled with CFR 0.151.
 */
package j2a.streamtolist;

import java.util.stream.Stream;
import tk.labyrinth.apparatus.extensionmethods.test.StreamExtensions;

public class StreamCollectToListUsagesWithLocalVariable {
    static {
        Stream<String> stream = Stream.of("a", "b");
        StreamExtensions.collectToList(stream);
    }
}

