/*
 * Decompiled with CFR 0.151.
 */
package j2a.streamtolist;

import java.util.List;
import java.util.stream.Stream;
import tk.labyrinth.apparatus.extensionmethods.test.StreamExtensions;

public class StreamCollectToListUsagesWithField {
    private static final Stream<String> stream = Stream.of("a", "b");

    static {
        StreamExtensions.collectToList(stream);
        List<String> list = StreamExtensions.collectToList(stream);
    }
}

