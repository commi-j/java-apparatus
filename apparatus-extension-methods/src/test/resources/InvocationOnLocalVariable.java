package tk.labyrinth.apparatus.extensionmethods.resource;

public class InvocationOnLocalVariable {

	static {
		String localVariable = "Hello World";
		System.out.println(localVariable.toKebabCase());
	}
}
