package tk.labyrinth.apparatus.extensionmethods.resource;

import tk.labyrinth.apparatus.extensionmethods.test.TestExtensions;

public class ExtensionMethodInvocationWithLiteralParameter {

	{
		TestExtensions.printSelf("Hello World", "aw");
	}
}
