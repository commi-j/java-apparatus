# Java Apparatus: Extension Methods

#### Extension Methods

##### Usage

1. Create public static method with at least one parameter;
1. Annotate it with @tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;
1. Compile module;
1. See type of first parameter having additional method similar to the one declared minus first parameter;
   - Invocations of this additional method are replaced with original method during compilation;

TODO: Add visual examples and some docs.

##### Known Issues

- IDE
  - Works bad with multiple modules
    - Can only calculate methods for one module at a time
    - To switch module you need to modify any file of focused one
  - EM suggests are not provided in IDE inside receiver type of EM;
    - Works fine if receiver is of super type;
    - If method is typed manually, it is resolved correctly.
- Compilation
  - Works on method references only if they don't have overloads (matching only works by name)
  - Does not work on lambda parameters
  - Does not work with generics

##### Roadmap

1. Lambda variables;
1. Messages for bad declarations;
1. Failure protection;
1. Generics;
1. Method reference overloads;
