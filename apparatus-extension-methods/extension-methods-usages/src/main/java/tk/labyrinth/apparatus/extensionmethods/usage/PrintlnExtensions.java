package tk.labyrinth.apparatus.extensionmethods.usage;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.util.Objects;

public class PrintlnExtensions {

	@ExtensionMethod
	public static void printSelf(Object it) {
		Objects.requireNonNull(it, "it");
		System.out.println(it);
	}

	public class Inner {
		// Can not contain static members.
	}

	public static class StaticNested {

		@ExtensionMethod
		public static void printSelf(Object it, String format) {
			Objects.requireNonNull(it, "it");
			System.out.println(String.format(format, it));
		}
	}
}
