package tk.labyrinth.apparatus.extensionmethods.usage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.function.Consumer;

public class InputStreamExtensions {

	//	@ExtensionMethod
	public static <I extends InputStream> void consumeAndClose(I it, Consumer<I> consumer) {
		Objects.requireNonNull(it, "it");
		Objects.requireNonNull(consumer, "consumer");
		//
		try {
			consumer.accept(it);
		} finally {
			try {
				it.close();
			} catch (IOException ex) {
				//noinspection ThrowFromFinallyBlock
				throw new RuntimeException(ex);
			}
		}
	}
}
