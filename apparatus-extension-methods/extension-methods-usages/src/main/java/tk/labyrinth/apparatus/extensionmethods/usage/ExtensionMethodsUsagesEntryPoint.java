package tk.labyrinth.apparatus.extensionmethods.usage;

import tk.labyrinth.apparatus.extensionmethods.methods.JavaTimeExtensions;

import java.time.OffsetDateTime;
import java.util.Date;

@SuppressWarnings("StringOperationCanBeSimplified")
public class ExtensionMethodsUsagesEntryPoint {

	public static void main(String... args) {
		String pattern = "Text: %s";
		{
			// Variable.
			pattern.printSelf();
		}
		{
			// Constructor.
			new String().printSelf();
		}
		{
			// StaticMethod.
			String.valueOf(12).printSelf();
		}
		{
			// Literal.
			"Hello World".printSelf();
		}
		{
			// Literal & overloaded ExtensionMethod.
			"Hello World".printSelf(pattern);
		}
		{
			// The method below becomes ambiguous from the IDEs point of view as there is an extension method
			//   PrintlnExtensions.StaticNested#printSelf(Object,String)
			// which translates into
			//   Object#printSelf(String)
			// which collides with
			//   PrintlnExtensions#printSelf(Object)
			//
			// PrintlnExtensions.printSelf("Hello World");
		}
		{
			// ExtensionMethods themselves are not to be replaced.
			PrintlnExtensions.StaticNested.printSelf("Hello World", pattern);
		}
		{
			Date someDate = new Date();
			OffsetDateTime offsetDateTime = someDate.toOffsetDateTime();
			Date sameDate = someDate.toOffsetDateTime().toDate();
			//
			JavaTimeExtensions.toDate(offsetDateTime);
		}
	}
}
