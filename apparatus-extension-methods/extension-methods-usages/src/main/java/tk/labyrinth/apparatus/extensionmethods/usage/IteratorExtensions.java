package tk.labyrinth.apparatus.extensionmethods.usage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class IteratorExtensions {

	//	@ExtensionMethod
	public static <E> ArrayList<E> toArrayList(Iterator<E> it) {
		Objects.requireNonNull(it, "it");
		//
		ArrayList<E> result = new ArrayList<>();
		it.forEachRemaining(result::add);
		return result;
	}

	//	@ExtensionMethod
	public static <E> List<E> toList(Iterator<E> it) {
		return toArrayList(it);
	}
}
