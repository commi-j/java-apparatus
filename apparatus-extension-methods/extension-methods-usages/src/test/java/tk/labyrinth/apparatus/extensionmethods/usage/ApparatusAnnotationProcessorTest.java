package tk.labyrinth.apparatus.extensionmethods.usage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.apparatus.core.method.MethodAdditionDescriptor;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.core.module.ApparatusRoundContext;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;

class ApparatusAnnotationProcessorTest {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testNoIndexEntryDuplicates() {
		testCompiler.run(
				CompilationTarget.ofObject(),
				new ApparatusAnnotationProcessor()
						.withModule(new TestModule()));
	}

	private static class TestModule implements ApparatusAnnotationProcessingModule {

		@Override
		public void processRound(ApparatusRoundContext context) {
			if (context.getRoundContext().getRound().isLast()) {
				Assertions.assertEquals(
						7,
						context.getIndexRegistry().get(MethodAdditionDescriptor.class).count());
			}
		}
	}
}
