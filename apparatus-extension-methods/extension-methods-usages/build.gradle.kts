//
dependencies {
	//
	annotationProcessor(project(":apparatus-annotation-processor"))
	//
	implementation(project(":apparatus-extension-methods"))
	annotationProcessor(project(":apparatus-extension-methods"))
	//
	implementation(project(":apparatus-extension-methods:extension-methods-library"))
	annotationProcessor(project(":apparatus-extension-methods:extension-methods-library"))
	//
	implementation(project(":apparatus-misc4j2"))
}
