@file:Suppress("UNCHECKED_CAST")

dependencies {
	val autoServiceVersion = "1.0-rc6"
	//
	api(project(":apparatus-annotation-processor"))
	//
	compileOnly("com.google.auto.service", "auto-service", autoServiceVersion)
	annotationProcessor("com.google.auto.service", "auto-service", autoServiceVersion)
	//
	testImplementation(project(":apparatus-testing"))
}
//
(extra["configurePublishingToBintray"] as () -> Unit).invoke()
