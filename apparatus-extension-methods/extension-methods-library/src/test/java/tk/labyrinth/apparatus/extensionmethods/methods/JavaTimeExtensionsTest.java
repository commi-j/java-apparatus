package tk.labyrinth.apparatus.extensionmethods.methods;

import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;

import java.util.List;

@ExtendWithJaap
class JavaTimeExtensionsTest {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testInvocationOnNewClass() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("InvocationOnNewClass.java"))
						.build(),
				new ApparatusAnnotationProcessor());
	}
}
