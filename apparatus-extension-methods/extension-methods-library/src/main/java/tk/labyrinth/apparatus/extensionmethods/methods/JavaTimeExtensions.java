package tk.labyrinth.apparatus.extensionmethods.methods;

import tk.labyrinth.apparatus.extensionmethods.pub.ExtensionMethod;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;

public class JavaTimeExtensions {

	@ExtensionMethod
	public static LocalDateTime atEndOfDay(LocalDate it) {
		Objects.requireNonNull(it, "it");
		//
		return it.atTime(LocalTime.MAX);
	}

	@ExtensionMethod
	public static LocalDateTime atEndOfDay(LocalDateTime it) {
		Objects.requireNonNull(it, "it");
		//
		return it.with(LocalTime.MAX);
	}

	@ExtensionMethod
	public static LocalDateTime atStartOfDay(LocalDateTime it) {
		Objects.requireNonNull(it, "it");
		//
		return it.with(LocalTime.MIN);
	}

	@ExtensionMethod
	public static Date toDate(OffsetDateTime it) {
		Objects.requireNonNull(it, "it");
		return Date.from(it.toInstant());
	}

	@ExtensionMethod
	public static OffsetDateTime toOffsetDateTime(Date it) {
		Objects.requireNonNull(it, "it");
		return it.toInstant().atOffset(ZoneOffset.UTC);
	}
}
