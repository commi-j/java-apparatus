//
dependencies {
	//
	annotationProcessor(project(":apparatus-annotation-processor"))
	//
	implementation(project(":apparatus-extension-methods"))
	annotationProcessor(project(":apparatus-extension-methods"))
	//
	//
	testImplementation(project(":apparatus-annotation-processor"))
}
