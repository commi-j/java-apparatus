package tk.labyrinth.apparatus.smartconstructors;

import com.sun.tools.javac.tree.JCTree;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.apparatus.core.AdvancedTreeMaker;
import tk.labyrinth.apparatus.core.constructor.ConstructorBodyBuilder;
import tk.labyrinth.apparatus.misc4j.java.util.collectoin.StreamUtils;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class SmartConstructorsConstructorBodyBuilder implements ConstructorBodyBuilder {

	private final List<String> fieldNames;

	private final List<FormalParameterElementTemplate> superConstructorParameters;

	@Override
	public JCTree.JCBlock buildBody(AdvancedTreeMaker treeMaker) {
		return treeMaker.createBlock(StreamUtils.concat(
				!superConstructorParameters.isEmpty()
						? Stream.of(createSuperInvocation(treeMaker,
						superConstructorParameters.stream().map(ElementTemplate::getSimpleNameAsString)))
						: Stream.empty(),
				fieldNames.stream()
						.map(fieldName -> createFieldAssignmentStatement(treeMaker, fieldName)),
				// TODO: Existing constructor
				Stream.empty()));
	}

	@Override
	public List<String> buildBodyStatements() {
		return Stream.concat(
				createSuperInvocationStatement(superConstructorParameters.stream()
						.map(ElementTemplate::getSimpleNameAsString)),
				createFieldAssignmentStatements(fieldNames.stream())
		).collect(Collectors.toList());
	}

	private static JCTree.JCStatement createFieldAssignmentStatement(AdvancedTreeMaker treeMaker, String fieldName) {
		return treeMaker.getTreeMaker().Exec(treeMaker.getTreeMaker().Assign(
				treeMaker.createPathExpression("this." + fieldName),
				treeMaker.createPathExpression(fieldName)));
	}

	private static Stream<String> createFieldAssignmentStatements(Stream<String> fieldNames) {
		return fieldNames.map(fieldName -> String.format("this.%s = %s;", fieldName, fieldName));
	}

	private static JCTree.JCStatement createSuperInvocation(AdvancedTreeMaker treeMaker, Stream<String> argumentNames) {
		return treeMaker.getTreeMaker().Exec(treeMaker.getTreeMaker().Apply(
				com.sun.tools.javac.util.List.nil(),
				treeMaker.createPathExpression("super"),
				argumentNames
						.map(treeMaker::createPathExpression)
						.collect(com.sun.tools.javac.util.List.collector())));
	}

	private static Stream<String> createSuperInvocationStatement(Stream<String> argumentNames) {
		Stream<String> result;
		{
			List<String> argumentNameList = argumentNames.collect(Collectors.toList());
			if (!argumentNameList.isEmpty()) {
				result = Stream.of(argumentNameList.stream().collect(Collectors.joining(",", "super(", ");")));
			} else {
				result = Stream.empty();
			}
		}
		return result;
	}
}
