package tk.labyrinth.apparatus.smartconstructors;

import tk.labyrinth.apparatus.core.IndexedForApparatus;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@IndexedForApparatus
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface SmartConstructor {
	// empty
}
