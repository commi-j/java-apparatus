package tk.labyrinth.apparatus.smartconstructors;

import com.google.auto.service.AutoService;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.IdentifierTree;
import tk.labyrinth.apparatus.core.constructor.ConstructorAdditionDescriptor;
import tk.labyrinth.apparatus.core.module.ApparatusAnnotationProcessingModule;
import tk.labyrinth.apparatus.core.module.ApparatusRoundContext;
import tk.labyrinth.apparatus.misc4j.java.util.collectoin.StreamUtils;
import tk.labyrinth.apparatus.model.node.BlockNode;
import tk.labyrinth.apparatus.model.node.ClassNode;
import tk.labyrinth.apparatus.model.node.FieldNode;
import tk.labyrinth.apparatus.sourcetree.registry.FilteringTreeCollector;
import tk.labyrinth.apparatus.sourcetree.scope.ElementNodeUtils;
import tk.labyrinth.jaap.model.declaration.ConstructorDeclaration;
import tk.labyrinth.jaap.model.declaration.FormalParameterDeclaration;
import tk.labyrinth.jaap.model.declaration.JavaConstructorModifier;
import tk.labyrinth.jaap.template.element.ConstructorElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;

import javax.lang.model.element.Name;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AutoService(ApparatusAnnotationProcessingModule.class)
public class ApparatusSmartConstructorsModule implements ApparatusAnnotationProcessingModule {

	private void processTypeElement(ApparatusRoundContext context, TypeElementTemplate typeElementTemplate) {
		ClassNode classNode = ElementNodeUtils.getClass(typeElementTemplate);
		//
		List<FieldNode> fields;
		{
			List<FieldNode> notInitializedFields = classNode.getFields()
					.filter(FieldNode::hasNoStaticModifier)
					.filter(FieldNode::hasFinalModifier)
					.filter(FieldNode::hasNoInitializer)
					.collect(Collectors.toList());
			if (!notInitializedFields.isEmpty()) {
				List<String> assignedVariableNames = classNode.getInitializerBlocks()
						.filter(BlockNode::hasNoStaticModifier)
						.flatMap(initializerBlock -> initializerBlock.getBlockTree()
								.accept(new FilteringTreeCollector<>(), AssignmentTree.class))
						.map(AssignmentTree::getVariable)
						// FIXME: Make sure we have to care only about Identifiers.
						.filter(IdentifierTree.class::isInstance)
						.map(IdentifierTree.class::cast)
						.map(IdentifierTree::getName)
						.map(Name::toString)
						.collect(Collectors.toList());
				fields = notInitializedFields.stream()
						.filter(fieldNode -> !assignedVariableNames.contains(fieldNode.getName()))
						.collect(Collectors.toList());
			} else {
				fields = List.of();
			}
		}
		{
			// TODO: Find existing constructor (0 or 1);
			// TODO: Remove fields that may be initialized in constructor
		}
		List<FormalParameterElementTemplate> superParameters;
		{
			TypeElementTemplate superclass = typeElementTemplate.getSuperclass();
			List<ConstructorElementTemplate> superConstructors = superclass.getConstructors().collect(Collectors.toList());
			if (!superConstructors.isEmpty()) {
				if (superConstructors.size() == 1) {
					// Either proper super constructor or no-args constructor which will be ignored.
					superParameters = superConstructors.get(0).getFormalParameters().collect(Collectors.toList());
				} else {
					// Multiple super constructors - ambiguity.
					superParameters = List.of();
					// FIXME: Error, multiple super constructors
					// TODO: Add possibility to specify which constructor to use
					// TODO: Super may be inferred from existing constructor
				}
			} else {
				// No constructors - nothing to add.
				superParameters = List.of();
			}
		}
		{
			if (!fields.isEmpty() || !superParameters.isEmpty()) {
				SmartConstructorsConstructorBodyBuilder bodyBuilder = new SmartConstructorsConstructorBodyBuilder(
						fields.stream()
								.map(FieldNode::getName)
								.collect(Collectors.toList()),
						superParameters);
				ConstructorAdditionDescriptor constructorAdditionDescriptor = new ConstructorAdditionDescriptor(
						bodyBuilder.buildBodyStatements(),
						bodyBuilder,
						ConstructorDeclaration.builder()
								.formalParameters(createFormalParameterDeclarations(fields, superParameters))
								.modifiers(List.of(JavaConstructorModifier.PUBLIC))
								.build(),
						typeElementTemplate.getSignature(),
						typeElementTemplate.getOldDirectAnnotation(SmartConstructor.class).getSignature());
				context.getRegistry().getConstructorAdditionDescriptorRegistry().add(
						this,
						typeElementTemplate.getTopLevelTypeElement(),
						constructorAdditionDescriptor);
			}
		}
	}

	@Override
	public void processRound(ApparatusRoundContext context) {
		context.getRoundContext().getAllTypeElements()
				.filter(typeElementTemplate -> typeElementTemplate.hasOldMergedAnnotation(SmartConstructor.class))
				.forEach(typeElementTemplate -> processTypeElement(context, typeElementTemplate));
	}

	private static List<FormalParameterDeclaration> createFormalParameterDeclarations(List<FieldNode> fields, List<FormalParameterElementTemplate> superParameters) {
		return StreamUtils.concat(
				fields.stream().map(ApparatusSmartConstructorsModule::fieldToConstructorFormalParameter),
				superParameters.stream().map(ApparatusSmartConstructorsModule::superParameterToConstructorFormalParameter),
				Stream.empty()
		).collect(Collectors.toList());
	}

	private static FormalParameterDeclaration fieldToConstructorFormalParameter(FieldNode field) {
		return FormalParameterDeclaration.builder()
				.name(field.getName())
				.type(field.getType().getDescription())
				.build();
	}

	private static FormalParameterDeclaration superParameterToConstructorFormalParameter(FormalParameterElementTemplate superParameter) {
		return FormalParameterDeclaration.builder()
				.name(superParameter.getSimpleNameAsString())
				.type(superParameter.getType().getDescription())
				.build();
	}
}
