package j2a;

import tk.labyrinth.apparatus.smartconstructors.SmartConstructor;

import java.math.BigDecimal;

@SmartConstructor
public class SmartConstructorWithFields {

	final String a;

	private final BigDecimal b;

	private final String c = "initialized in declaration";

	private final String d;

	{
		d = "initialized in init block";
	}
}
