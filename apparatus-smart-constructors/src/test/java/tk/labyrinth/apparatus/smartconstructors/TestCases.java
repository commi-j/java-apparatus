package tk.labyrinth.apparatus.smartconstructors;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.annproc.ApparatusAnnotationProcessor;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;

import java.util.List;

public class TestCases {

	private final JaapTestCompiler testCompiler = new JaapTestCompiler();

	@Test
	void testAddrIsSet() {
		//
		// TODO: Write about problem detected.
		// com.sun.tools.javac.comp.Flow.AssignAnalyzer.visitVarDef
		// com.sun.tools.javac.comp.Flow.AssignAnalyzer.newVar
	}

	@Test
	void testSiblings() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of(
								"j2a/sibling/FirstSibling.java",
								"j2a/sibling/SecondSibling.java"))
						.build(),
				createProcessor());
	}

	// TODO
	@Disabled
	@Test
	void testSmartConstructorWithConstructor() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("j2a/SmartConstructorWithConstructor.java"))
						.build(),
				createProcessor());
	}

	@Test
	void testSmartConstructorWithFields() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("j2a/SmartConstructorWithFields.java"))
						.build(),
				createProcessor());
	}

	@Test
	void testSmartConstructorWithSuper() {
		testCompiler.run(
				CompilationTarget.builder()
						.sourceResources(List.of("j2a/SmartConstructorWithSuper.java"))
						.build(),
				createProcessor());
	}

	private static ApparatusAnnotationProcessor createProcessor() {
		return new ApparatusAnnotationProcessor(false)
				.withModule(new ApparatusSmartConstructorsModule());
	}
}
