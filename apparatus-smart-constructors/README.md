# Java Apparatus: Smart Constructors

#### Features

Classes annotated with @SmartConstructor will receive generated public
constructor that has parameters for both super constructor and all
non-initialized final fields.

##### Use Cases

@SmartConstructors are primarily designed for framework where developer does not
create objects on his own. Spring would be notable example of such framework.

@SmartConstructors enable you to use both constructor injection and inheritance
with no need for boilerplating with super invocation.

#### Getting Started

See [Getting Started](..) section of root project.

The artifact for this feature is **apparatus-smart-constructors**.

#### Comparison to Lombok

Lombok has several annotations that generate constructors with parameters:

##### @AllArgsConstructor/@RequiredArgsConstructor

Both only works where there is a no-args super constructor
available. @SmartConstructor has no such limitation.

##### @SuperBuilder

Works with super classes but requires them to also be annotated with
@SuperBuilder, which is not always possible. It also generates a constructor
with intermediary "builder" object not suitable for DI frameworks.

#### Limitations and Roadmap

- TODO
- TODO
