package j2a.etc;

import j2a.SmartConstructorWithFields;
import j2a.SmartConstructorWithSuper;

import java.math.BigDecimal;
import java.math.BigInteger;

public class SmartConstructorsMain {

	public static void main(String... args) {
		{
			System.out.println(new SmartConstructorWithFields("a", BigDecimal.ZERO));
		}
		{
			System.out.println(new SmartConstructorWithSuper(BigDecimal.ZERO, BigInteger.ZERO));
		}
	}
}
