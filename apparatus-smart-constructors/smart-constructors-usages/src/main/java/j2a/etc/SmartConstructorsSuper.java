package j2a.etc;

import java.math.BigDecimal;
import java.math.BigInteger;

public class SmartConstructorsSuper {

	private final BigDecimal bd;

	private final BigInteger bi;

	public SmartConstructorsSuper(BigDecimal bd, BigInteger bi) {
		this.bd = bd;
		this.bi = bi;
	}
}
