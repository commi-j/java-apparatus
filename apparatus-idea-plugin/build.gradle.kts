//
plugins {
	id("org.jetbrains.intellij").version("0.7.2")
	java
}
//
intellij {
	//
	setPlugins("java")
	//
	// https://github.com/JetBrains/gradle-intellij-plugin#rundebug-ide-properties
	sandboxDirectory = "${project.projectDir}/.idea-sandbox"
	//
	version = "2019.3.3"
}
//
dependencies {
	implementation(project(":apparatus-core"))
}
//
tasks.publishPlugin {
	channels("alpha")
	token(System.getProperty("org.gradle.project.intellijPublishToken"))
}
tasks.runIde {}
//
