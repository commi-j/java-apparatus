package tk.labyrinth.apparatus.one.testing;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extension;

@ExtendWith(ModuleParameterResolver.class)
public class ApparatusOneExtension implements Extension {
	// empty
}
