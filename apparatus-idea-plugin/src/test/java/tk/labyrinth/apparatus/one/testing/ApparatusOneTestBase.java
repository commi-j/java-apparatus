package tk.labyrinth.apparatus.one.testing;

import com.intellij.openapi.projectRoots.JavaSdk;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.testFramework.LightProjectDescriptor;
import com.intellij.testFramework.fixtures.DefaultLightProjectDescriptor;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public class ApparatusOneTestBase extends LightJavaCodeInsightFixtureTestCase {

	@AfterEach
	private void afterEach() throws Exception {
		tearDown();
	}

	@BeforeEach
	private void beforeEach() throws Exception {
		setUp();
	}

	@NotNull
	@Override
	protected LightProjectDescriptor getProjectDescriptor() {
		return new DefaultLightProjectDescriptor() {
			@Override
			public Sdk getSdk() {
				return JavaSdk.getInstance().createJdk("java 1.8", "lib/mockJDK-1.8", false);
			}
		};
	}
}
