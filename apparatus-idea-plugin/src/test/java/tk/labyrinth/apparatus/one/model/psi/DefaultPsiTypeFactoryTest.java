package tk.labyrinth.apparatus.one.model.psi;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiTypeParameter;
import com.intellij.psi.impl.compiled.ClsJavaCodeReferenceElementImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.apparatus.one.testing.ApparatusOneTestBase;

import java.util.Optional;

// TODO: Enhance with JUnit5 extension.
@SuppressWarnings("TestMethodWithIncorrectSignature")
class DefaultPsiTypeFactoryTest extends ApparatusOneTestBase {

	@Test
	void testGetVariable() {
		ApplicationManager.getApplication().runReadAction(() -> {
			Module module = getModule();
			DefaultPsiElementFactory psiElementFactory = new DefaultPsiElementFactory(module);
			PsiTypeFactory psiTypeFactory = new DefaultPsiTypeFactory(module, psiElementFactory);
			//
			PsiClassType optionalTFromTypeFactory = psiTypeFactory.getVariable("java.util.Optional%T");
			//
			Assertions.assertEquals("T", optionalTFromTypeFactory.getCanonicalText());
			Assertions.assertEquals(
					psiElementFactory.getType(Optional.class).getTypeParameters()[0],
					optionalTFromTypeFactory.resolve());
		});
	}

	@Test
	void testVariableTypeWithProperCanonicalText() {
		ApplicationManager.getApplication().runReadAction(() -> {
			Module module = getModule();
			DefaultPsiElementFactory psiElementFactory = new DefaultPsiElementFactory(module);
			com.intellij.psi.PsiElementFactory intellijPsiElementFactory = psiElementFactory.getIntellijPsiElementFactory();
			//
			PsiTypeParameter optionalTPsiTypeParameter = psiElementFactory.getTypeParameter("java.util.Optional%T");
			//
			Assertions.assertEquals(
					"T",
					intellijPsiElementFactory.createType(optionalTPsiTypeParameter).getCanonicalText());
			Assertions.assertEquals(
					"",
					intellijPsiElementFactory.createType(intellijPsiElementFactory.createClassReferenceElement(
							optionalTPsiTypeParameter)).getCanonicalText());
			Assertions.assertEquals(
					"",
					intellijPsiElementFactory.createType(intellijPsiElementFactory.createReferenceExpression(
							optionalTPsiTypeParameter)).getCanonicalText());
			Assertions.assertEquals(
					"T",
					intellijPsiElementFactory.createType(new ClsJavaCodeReferenceElementImpl(
							optionalTPsiTypeParameter.getParent(),
							optionalTPsiTypeParameter.getText())).getCanonicalText());
		});
	}
}