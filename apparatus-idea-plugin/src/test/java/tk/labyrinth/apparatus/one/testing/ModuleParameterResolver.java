package tk.labyrinth.apparatus.one.testing;

import com.intellij.openapi.module.Module;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.platform.commons.support.ReflectionSupport;

public class ModuleParameterResolver implements ParameterResolver {

	@Override
	public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
		Object result;
		{
			Object testInstance = extensionContext.getRequiredTestInstance();
			if (testInstance instanceof LightJavaCodeInsightFixtureTestCase) {
				try {
					result = ReflectionSupport.invokeMethod(
							LightJavaCodeInsightFixtureTestCase.class.getDeclaredMethod("getModule"),
							testInstance);
				} catch (NoSuchMethodException ex) {
					throw new RuntimeException(ex);
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Override
	public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
		return parameterContext.getParameter().getType() == Module.class;
	}
}
