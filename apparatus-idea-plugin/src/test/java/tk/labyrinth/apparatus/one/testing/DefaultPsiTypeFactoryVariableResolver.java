package tk.labyrinth.apparatus.one.testing;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.apparatus.one.model.psi.DefaultPsiTypeFactory;

public class DefaultPsiTypeFactoryVariableResolver implements ParameterResolver {

	@Override
	public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
		return parameterContext.getParameter().getType() == DefaultPsiTypeFactory.class;
	}
}
