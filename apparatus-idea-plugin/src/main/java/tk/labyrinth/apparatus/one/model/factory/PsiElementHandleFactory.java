package tk.labyrinth.apparatus.one.model.factory;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiMethod;
import tk.labyrinth.apparatus.one.model.element.PsiMethodElementHandle;
import tk.labyrinth.apparatus.one.model.element.PsiTypeElementHandle;
import tk.labyrinth.jaap.template.element.ElementTemplateFactory;

public interface PsiElementHandleFactory extends ElementTemplateFactory {

	PsiMethodElementHandle getMethod(PsiMethod psiMethod);

	PsiTypeElementHandle getType(PsiClass psiClass);

	PsiTypeElementHandle getType(PsiClassType psiClassType);
}
