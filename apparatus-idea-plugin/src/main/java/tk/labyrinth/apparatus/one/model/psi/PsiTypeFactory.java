package tk.labyrinth.apparatus.one.model.psi;

import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiPrimitiveType;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiWildcardType;
import lombok.val;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.annotation.Nullable;

public interface PsiTypeFactory {

	@Nullable
	PsiType find(String typeDescriptionString);

	@Nullable
	PsiType find(TypeDescription typeDescription);

	@Nullable
	PsiClassType findClass(String typeDescriptionString);

	@Nullable
	PsiClassType findClass(TypeDescription typeDescription);

	@Nullable
	PsiPrimitiveType findKeyword(String keywordTypeName);

	@Nullable
	PsiClassType findVariable(String typeDescriptionString);

	@Nullable
	PsiClassType findVariable(TypeDescription typeDescription);

	@Nullable
	PsiWildcardType findWildcard(TypeDescription typeDescription);

	default PsiType get(String fullTypeSignature) {
		val result = find(fullTypeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullTypeSignature = " + fullTypeSignature);
		}
		return result;
	}

	default PsiClassType getClass(String fullTypeSignature) {
		val result = findClass(fullTypeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullTypeSignature = " + fullTypeSignature);
		}
		return result;
	}

	default PsiClassType getVariable(String typeDescriptionString) {
		val result = findVariable(typeDescriptionString);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeDescriptionString = " + typeDescriptionString);
		}
		return result;
	}
}
