package tk.labyrinth.apparatus.one.model.element;

import com.intellij.psi.PsiMethod;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.apparatus.one.model.factory.PsiElementHandleFactory;
import tk.labyrinth.jaap.annotation.OldMergedAnnotation;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.OldAnnotationHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.MethodModifier;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.template.OldAnnotationTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import javax.lang.model.element.Name;
import java.lang.annotation.Annotation;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class PsiMethodElementHandle implements MethodElementTemplate {

	private final PsiElementHandleFactory elementHandleFactory;

	@Getter
	private final PsiMethod psiMethod;

	@Nullable
	@Override
	public OldAnnotationHandle findOldDirectAnnotation(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public OldAnnotationHandle findOldDirectAnnotation(Class<? extends Annotation> type) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public int getFormalParameterCount() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<FormalParameterElementTemplate> getFormalParameters() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<MethodModifier> getModifiers() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<OldAnnotationHandle> getOldDirectAnnotations() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<OldAnnotationHandle> getOldDirectAnnotations(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<OldMergedAnnotation> getOldMergedAnnotations(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<OldMergedAnnotation> getOldMergedAnnotations(Class<? extends Annotation> annotationType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeElementTemplate getParent() {
		return elementHandleFactory.getType(psiMethod.getContainingClass());
	}

	@Override
	public ProcessingContext getProcessingContext() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeHandle getReturnType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getSignature() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Name getSimpleName() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getSimpleNameAsString() {
		return psiMethod.getName();
	}

	@Override
	public int getTypeParameterCount() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<TypeParameterElementTemplate> getTypeParameters() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitPublicModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasExplicitStaticModifier() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasOldMergedAnnotation(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasOldMergedAnnotation(Class<? extends Annotation> annotationType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonPublic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyNonStatic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyPublic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isEffectivelyStatic() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String toString() {
		return psiMethod.toString();
//		return getFullSignature().toString();
	}
}
