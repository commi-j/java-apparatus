package tk.labyrinth.apparatus.one.model.psi;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiTypeParameter;
import com.intellij.psi.PsiTypeParameterListOwner;
import lombok.val;
import tk.labyrinth.jaap.model.ElementSignature;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;

import javax.annotation.Nullable;

public interface PsiElementFactory {

	@Nullable
	PsiElement findElement(ElementSignature fullElementSignature);

	@Nullable
	default PsiElement findElement(String fullElementSignature) {
		return findElement(ElementSignature.ofValid(fullElementSignature));
	}

	@Nullable
	default PsiMethod findMethod(String methodFullSignature) {
		return findMethod(MethodFullSignature.of(methodFullSignature));
	}

	@Nullable
	PsiMethod findMethod(MethodFullSignature methodFullSignature);

	@Nullable
	PsiClass findType(CanonicalTypeSignature canonicalTypeSignature);

	@Nullable
	PsiClass findType(Class<?> type);

	@Nullable
	PsiClass findType(String fullTypeSignature);

	PsiTypeParameter findTypeParameter(String typeParameterElementSignature);

	/**
	 * Method or Type.
	 *
	 * @param elementFullSignature non-null
	 *
	 * @return nullable
	 */
	@Nullable
	PsiTypeParameterListOwner findTypeParameterListOwner(String elementFullSignature);

	default PsiElement getElement(ElementSignature fullElementSignature) {
		val result = findElement(fullElementSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullElementSignature = " + fullElementSignature);
		}
		return result;
	}

	default PsiElement getElement(String fullElementSignature) {
		val result = findElement(fullElementSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullElementSignature = " + fullElementSignature);
		}
		return result;
	}

	default PsiMethod getMethod(String fullMethodSignature) {
		val result = findMethod(fullMethodSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullMethodSignature = " + fullMethodSignature);
		}
		return result;
	}

	default PsiClass getType(CanonicalTypeSignature canonicalTypeSignature) {
		PsiClass result = findType(canonicalTypeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: canonicalTypeSignature = " + canonicalTypeSignature);
		}
		return result;
	}

	default PsiClass getType(Class<?> type) {
		PsiClass result = findType(type);
		if (result == null) {
			throw new IllegalArgumentException("Not found: type = " + type);
		}
		return result;
	}

	default PsiClass getType(String fullTypeSignature) {
		val result = findType(fullTypeSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: fullTypeSignature = " + fullTypeSignature);
		}
		return result;
	}

	PsiClass getType(PsiClassType psiClassType);

	default PsiTypeParameter getTypeParameter(String typeParameterElementSignature) {
		PsiTypeParameter result = findTypeParameter(typeParameterElementSignature);
		if (result == null) {
			throw new IllegalArgumentException("Not found: typeParameterElementSignature = " + typeParameterElementSignature);
		}
		return result;
	}
}
