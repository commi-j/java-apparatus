package tk.labyrinth.apparatus.one.model.psi;

import com.intellij.lang.java.JavaLanguage;
import com.intellij.openapi.module.Module;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiTypeParameter;
import com.intellij.psi.PsiTypeParameterListOwner;
import com.intellij.psi.impl.light.LightMethodBuilder;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.model.ElementSignature;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class DefaultPsiElementFactory implements PsiElementFactory {

	@Getter
	private final com.intellij.psi.PsiElementFactory intellijPsiElementFactory;

	@Getter
	private final Map<MethodFullSignature, PsiMethod> methodsUnderCreation = new HashMap<>();

	private final Module module;

	public DefaultPsiElementFactory(Module module) {
		this.module = module;
		intellijPsiElementFactory = com.intellij.psi.PsiElementFactory.getInstance(module.getProject());
	}

	@Nullable
	@Override
	public PsiElement findElement(ElementSignature fullElementSignature) {
		PsiElement result;
		if (fullElementSignature.matchesMethod()) {
			// FIXME: Make sig2sig conversion.
			result = findMethod(fullElementSignature.toString());
		} else {
			throw new NotImplementedException(fullElementSignature.toString());
		}
		return result;
	}

	@Nullable
	@Override
	public PsiMethod findMethod(MethodFullSignature methodFullSignature) {
		PsiMethod result;
		{
			PsiMethod foundMethod = methodsUnderCreation.get(methodFullSignature);
			if (foundMethod != null) {
				result = foundMethod;
			} else {
				PsiClass type = findType(methodFullSignature.getTypeFullSignature());
				if (type != null) {
					LightMethodBuilder methodBuilder = new LightMethodBuilder(PsiManager.getInstance(module.getProject()),
							JavaLanguage.INSTANCE, methodFullSignature.getName());
					methodFullSignature.getParameters().forEach(parameter ->
							methodBuilder.addParameter("parameter", parameter));
					result = type.findMethodBySignature(methodBuilder, true);
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	@Nullable
	@Override
	public PsiClass findType(CanonicalTypeSignature canonicalTypeSignature) {
		return JavaPsiFacade.getInstance(module.getProject()).findClass(
				canonicalTypeSignature.toString(),
				module.getModuleWithDependenciesAndLibrariesScope(false));
	}

	@Nullable
	@Override
	public PsiClass findType(Class<?> type) {
		return findType(type.getCanonicalName());
	}

	@Nullable
	@Override
	public PsiClass findType(String fullTypeSignature) {
		return findType(CanonicalTypeSignature.ofValid(fullTypeSignature));
	}

	@Override
	public PsiTypeParameter findTypeParameter(String typeParameterElementSignature) {
		PsiTypeParameter result;
		{
			Pair<String, String> parentAndName = SignatureSeparators.split(
					typeParameterElementSignature,
					SignatureSeparators.TYPE_PARAMETER);
			PsiTypeParameterListOwner typeParameterListOwner = findTypeParameterListOwner(parentAndName.getLeft());
			if (typeParameterListOwner != null) {
				result = Stream.of(typeParameterListOwner.getTypeParameters())
						.filter(psiTypeParameter -> Objects.equals(
								psiTypeParameter.getName(),
								parentAndName.getRight()))
						.findFirst()
						.orElse(null);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public PsiTypeParameterListOwner findTypeParameterListOwner(String elementFullSignature) {
		PsiTypeParameterListOwner result;
		{
			ElementSignature elementSignature = ElementSignature.ofValid(elementFullSignature);
			if (elementSignature.matchesMethod()) {
				result = findMethod(MethodFullSignature.of(elementFullSignature));
			} else if (elementSignature.matchesType()) {
				result = findType(CanonicalTypeSignature.ofValid(elementFullSignature));
			} else {
				throw new IllegalArgumentException(elementFullSignature);
			}
		}
		return result;
	}

	@Override
	public PsiClass getType(PsiClassType psiClassType) {
		return psiClassType.resolve();
	}
}
