package tk.labyrinth.apparatus.one.model.factory;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiMethod;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;
import tk.labyrinth.apparatus.one.model.psi.PsiElementFactory;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.apparatus.one.model.element.PsiMethodElementHandle;
import tk.labyrinth.apparatus.one.model.element.PsiTypeElementHandle;
import tk.labyrinth.jaap.model.ElementSignature;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.template.element.ConstructorElementTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.FormalParameterElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;
import tk.labyrinth.jaap.template.element.VariableElementTemplate;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

@RequiredArgsConstructor
public class DefaultPsiElementHandleFactory implements PsiElementHandleFactory {

	private final PsiElementFactory psiElementFactory;

	@Nullable
	@Override
	public ElementTemplate findElement(ElementSignature fullElementSignature) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public FieldElementTemplate findField(Class<?> type, String fieldSimpleName) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public FieldElementTemplate findField(String fieldFullName) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public MethodElementTemplate findMethod(MethodFullSignature methodFullSignature) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public PsiTypeElementHandle findType(CanonicalTypeSignature canonicalTypeSignature) {
		PsiClass psiClass = psiElementFactory.findType(canonicalTypeSignature);
		return psiClass != null ? new PsiTypeElementHandle(this, psiClass) : null;
	}

	@Nullable
	@Override
	public PsiTypeElementHandle findType(String fullTypeSignature) {
		PsiClass psiClass = psiElementFactory.findType(fullTypeSignature);
		return psiClass != null ? new PsiTypeElementHandle(this, psiClass) : null;
	}

	@Nullable
	@Override
	public TypeParameterElementTemplate findTypeParameter(Class<?> type, String typeParameterSimpleName) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public TypeParameterElementTemplate findTypeParameter(String typeParameterSignature) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ConstructorElementTemplate getConstructor(Element element) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ConstructorElementTemplate getConstructor(ExecutableElement executableElement) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ElementTemplate getElement(Element element) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public FieldElementTemplate getField(Element element) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public FieldElementTemplate getField(VariableElement variableElement) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public FormalParameterElementTemplate getFormalParameter(VariableElement variableElement) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public MethodElementTemplate getMethod(ExecutableElement executableElement) {
		throw new UnsupportedOperationException();
	}

	@Override
	public PsiMethodElementHandle getMethod(PsiMethod psiMethod) {
		return new PsiMethodElementHandle(this, psiMethod);
	}

	@Override
	public PsiTypeElementHandle getType(PsiClass psiClass) {
		return new PsiTypeElementHandle(this, psiClass);
	}

	@Override
	public PsiTypeElementHandle getType(PsiClassType psiClassType) {
		return getType(psiElementFactory.getType(psiClassType));
	}

	@Override
	public PsiTypeElementHandle getType(String fullTypeSignature) {
		return new PsiTypeElementHandle(this, psiElementFactory.getType(fullTypeSignature));
	}

	@Override
	public TypeElementTemplate getType(TypeElement typeElement) {
		throw new UnsupportedOperationException();
	}

	@Override
	public TypeElementTemplate getType(TypeMirror typeMirror) {
		throw new UnsupportedOperationException();
	}

	@Override
	public TypeParameterElementTemplate getTypeParameter(Class<?> type, String typeParameterSimpleName) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeParameterElementTemplate getTypeParameter(TypeParameterElement typeParameterElement) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public VariableElementTemplate getVariable(VariableElement variableElement) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
