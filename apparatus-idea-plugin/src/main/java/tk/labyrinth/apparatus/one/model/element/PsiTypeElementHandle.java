package tk.labyrinth.apparatus.one.model.element;

import com.intellij.psi.PsiClass;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.apparatus.one.model.factory.PsiElementHandleFactory;
import tk.labyrinth.jaap.annotation.OldMergedAnnotation;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.handle.OldAnnotationHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.entity.selection.EntitySelector;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.template.DeclaredTypeTemplate;
import tk.labyrinth.jaap.template.OldAnnotationTemplate;
import tk.labyrinth.jaap.template.TypeTemplate;
import tk.labyrinth.jaap.template.element.ConstructorElementTemplate;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.FieldElementTemplate;
import tk.labyrinth.jaap.template.element.MethodElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.template.element.TypeParameterElementTemplate;

import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class PsiTypeElementHandle implements TypeElementTemplate {

	private final PsiElementHandleFactory elementHandleFactory;

	@Getter
	private final PsiClass psiClass;

	@Nullable
	@Override
	public OldAnnotationHandle findOldDirectAnnotation(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public OldAnnotationHandle findOldDirectAnnotation(Class<? extends Annotation> type) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public TypeElementTemplate findSuperclass() {
		return elementHandleFactory.getType(psiClass.getSuperClass());
	}

	@Override
	public Stream<FieldElementTemplate> getAllFields() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getBinaryName() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<ConstructorElementTemplate> getConstructors() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<FieldElementTemplate> getDeclaredFields() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<MethodElementTemplate> getDeclaredMethods() {
		// FIXME: Ensure #getMethods returns only declared ones.
		return Stream.of(psiClass.getMethods())
				.map(elementHandleFactory::getMethod);
	}

	@Override
	public Stream<? extends TypeElementTemplate> getDirectSupertypes() {
		return Stream.of(psiClass.getSuperTypes()).map(elementHandleFactory::getType);
	}

	@Override
	public PsiMethodElementHandle getMethodByName(String methodName) {
		return (PsiMethodElementHandle) TypeElementTemplate.super.getMethodByName(methodName);
	}

	@Override
	public Stream<OldAnnotationHandle> getOldDirectAnnotations() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<OldAnnotationHandle> getOldDirectAnnotations(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<OldMergedAnnotation> getOldMergedAnnotations(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<OldMergedAnnotation> getOldMergedAnnotations(Class<? extends Annotation> annotationType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public PackageElementTemplate getPackage() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public String getPackageQualifiedName() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ElementTemplate getParent() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ProcessingContext getProcessingContext() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public CanonicalTypeSignature getSignature() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Name getSimpleName() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeElementTemplate getTopLevelTypeElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeElement getTypeElement() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<? extends TypeElementTemplate> getTypeHierarchy() {
		// FIXME: Method copied from annproc, probably should be optimized.
		//
		Queue<TypeElementTemplate> unprocessedTypeElements = new LinkedList<>();
		unprocessedTypeElements.add(this);
		Set<TypeElementTemplate> processedTypeElements = new HashSet<>();
		//
		List<TypeElementTemplate> result = new ArrayList<>();
		while (!unprocessedTypeElements.isEmpty()) {
			TypeElementTemplate typeElement = unprocessedTypeElements.remove();
			if (processedTypeElements.add(typeElement)) {
				result.add(typeElement);
				typeElement.getDirectSupertypes().forEach(unprocessedTypeElements::add);
			}
		}
		if (isInterface()) {
			TypeElementTemplate objectTypeElement = elementHandleFactory.getType(Object.class);
			result.add(objectTypeElement);
		}
		return result.stream();
	}

	@Override
	public TypeMirror getTypeMirror() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public int getTypeParameterCount() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Stream<TypeParameterElementTemplate> getTypeParameters() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasOldMergedAnnotation(OldAnnotationTemplate oldAnnotationTemplate) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean hasOldMergedAnnotation(Class<? extends Annotation> annotationType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isAssignableTo(TypeElementTemplate other) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public boolean isInterface() {
		return psiClass.isInterface();
	}

	@Override
	public TypeHandle resolveType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public ElementTemplate selectMember(EntitySelector selector) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public MethodElementTemplate selectMethodElement(String methodSimpleName, List<TypeTemplate> argumentTypes) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public DeclaredTypeTemplate toType() {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
