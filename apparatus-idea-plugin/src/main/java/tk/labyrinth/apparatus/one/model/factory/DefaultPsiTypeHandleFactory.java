package tk.labyrinth.apparatus.one.model.factory;

import org.jetbrains.annotations.Nullable;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.jaap.handle.base.GenericContext;
import tk.labyrinth.jaap.handle.type.DeclaredTypeHandle;
import tk.labyrinth.jaap.handle.type.PrimitiveTypeHandle;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;

public class DefaultPsiTypeHandleFactory implements PsiTypeHandleFactory {

	@Nullable
	@Override
	public TypeHandle find(TypeDescription typeDescription) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclared(String typeDescriptionString) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public DeclaredTypeHandle findDeclared(TypeDescription typeDescription) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeHandle get(GenericContext genericContext, Class<?> type) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeHandle get(GenericContext genericContext, TypeElement typeElement) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public TypeHandle get(GenericContext genericContext, TypeMirror typeMirror) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, DeclaredType declaredType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public DeclaredTypeHandle getDeclared(GenericContext genericContext, TypeMirror typeMirror) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(PrimitiveType primitiveType) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public PrimitiveTypeHandle getPrimitive(TypeMirror typeMirror) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
