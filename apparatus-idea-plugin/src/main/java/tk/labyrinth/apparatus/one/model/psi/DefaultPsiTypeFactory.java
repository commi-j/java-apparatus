package tk.labyrinth.apparatus.one.model.psi;

import com.intellij.openapi.module.Module;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiPrimitiveType;
import com.intellij.psi.PsiSubstitutor;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiTypeParameter;
import com.intellij.psi.PsiWildcardType;
import com.intellij.psi.impl.compiled.ClsJavaCodeReferenceElementImpl;
import com.intellij.psi.impl.source.PsiImmediateClassType;
import tk.labyrinth.apparatus.misc4j.java.lang.exception.NotImplementedException;
import tk.labyrinth.jaap.model.declaration.TypeDescription;

import javax.annotation.Nullable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DefaultPsiTypeFactory implements PsiTypeFactory {

	private final com.intellij.psi.PsiElementFactory intellijPsiElementFactory;

	private final Module module;

	private final PsiElementFactory psiElementFactory;

	public DefaultPsiTypeFactory(Module module, PsiElementFactory psiElementFactory) {
		this.module = module;
		this.psiElementFactory = psiElementFactory;
		intellijPsiElementFactory = com.intellij.psi.PsiElementFactory.getInstance(module.getProject());
	}

	@Nullable
	@Override
	public PsiType find(String typeDescriptionString) {
		return find(TypeDescription.of(typeDescriptionString));
	}

	@Override
	public PsiType find(TypeDescription typeDescription) {
		PsiType result;
		if (typeDescription.isArray()) {
			throw new NotImplementedException();
		} else if (typeDescription.isKeyword()) {
			result = findKeyword(typeDescription.getFullName());
		} else if (typeDescription.isWildcard()) {
			result = findWildcard(typeDescription);
		} else {
			// Parameterized or Variable or other
			//
			result = findClass(typeDescription);
		}
		return result;
	}

	@Nullable
	@Override
	public PsiClassType findClass(String typeDescriptionString) {
		return findClass(TypeDescription.of(typeDescriptionString));
	}

	@Nullable
	@Override
	public PsiClassType findClass(TypeDescription typeDescription) {
		PsiClassType result;
		if (typeDescription.isParameterized()) {
			PsiClass psiClass = psiElementFactory.findType(typeDescription.getFullName());
			if (psiClass != null) {
				if (psiClass.getTypeParameters().length == typeDescription.getParameters().size()) {
					// FIXME: Use intellijFactory.
					result = new PsiImmediateClassType(
							psiClass,
							PsiSubstitutor.createSubstitutor(IntStream.range(0, psiClass.getTypeParameters().length)
									.boxed()
									.collect(Collectors.toMap(
											index -> psiClass.getTypeParameters()[index],
											index -> find(typeDescription.getParameters().get(index))))));
				} else {
					throw new IllegalArgumentException("Inconsistent TypeParameterCount: " + typeDescription);
				}
			} else {
				result = null;
			}
		} else if (typeDescription.isVariable()) {
			result = findVariable(typeDescription);
		} else {
			if (typeDescription.isArray() || typeDescription.isKeyword() || typeDescription.isWildcard()) {
				throw new IllegalArgumentException(typeDescription.toString());
			}
			// FIXME: Check what happens if class not found.
			result = PsiType.getTypeByName(
					typeDescription.getFullName(),
					module.getProject(),
					module.getModuleWithDependenciesAndLibrariesScope(false));
		}
		return result;
	}

	@Nullable
	@Override
	public PsiPrimitiveType findKeyword(String keywordTypeName) {
		PsiPrimitiveType result;
		switch (keywordTypeName) {
			case "boolean":
				result = PsiType.BOOLEAN;
				break;
			case "byte":
				result = PsiType.BYTE;
				break;
			case "char":
				result = PsiType.CHAR;
				break;
			case "double":
				result = PsiType.DOUBLE;
				break;
			case "float":
				result = PsiType.FLOAT;
				break;
			case "int":
				result = PsiType.INT;
				break;
			case "long":
				result = PsiType.LONG;
				break;
			case "short":
				result = PsiType.SHORT;
				break;
			case "void":
				result = PsiType.VOID;
				break;
			default:
				throw new IllegalArgumentException(keywordTypeName);
		}
		return result;
	}

	@Nullable
	@Override
	public PsiClassType findVariable(String typeDescriptionString) {
		return findVariable(TypeDescription.of(typeDescriptionString));
	}

	@Nullable
	@Override
	public PsiClassType findVariable(TypeDescription typeDescription) {
		if (!typeDescription.isVariable()) {
			throw new IllegalArgumentException(typeDescription.toString());
		}
		PsiClassType result;
		{
			PsiTypeParameter psiTypeParameter = psiElementFactory.findTypeParameter(typeDescription.getFullName());
			result = psiTypeParameter != null
					// TODO In order to use this method we must make our Light builders support:
					//  #getParent() over #getOwner();
					//  #getText() over #getName();
//					? intellijPsiElementFactory.createType(psiTypeParameter)
					? intellijPsiElementFactory.createType(new ClsJavaCodeReferenceElementImpl(
					psiTypeParameter.getOwner(),
					psiTypeParameter.getName()))
					: null;
		}
		return result;
	}

	@Nullable
	@Override
	public PsiWildcardType findWildcard(TypeDescription typeDescription) {
		if (!typeDescription.isWildcard()) {
			throw new IllegalArgumentException(typeDescription.toString());
		}
		PsiWildcardType result;
		{
			PsiManager psiManager = PsiManager.getInstance(module.getProject());
			if (typeDescription.getUpperBound() != null) {
				if (typeDescription.getUpperBound().size() != 1) {
					//
					// TODO: Intersection (? extends A & B)
					throw new NotImplementedException();
				}
				PsiType extendsType = find(typeDescription.getUpperBound().get(0));
				result = extendsType != null
						? PsiWildcardType.createExtends(psiManager, extendsType)
						: null;
			} else if (typeDescription.getLowerBound() != null) {
				PsiType superType = find(typeDescription.getLowerBound());
				result = superType != null
						? PsiWildcardType.createSuper(psiManager, superType)
						: null;
			} else {
				result = PsiWildcardType.createUnbounded(psiManager);
			}
		}
		return result;
	}
}
