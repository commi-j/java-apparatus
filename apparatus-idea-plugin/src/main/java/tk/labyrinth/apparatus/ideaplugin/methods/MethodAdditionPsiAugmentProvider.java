package tk.labyrinth.apparatus.ideaplugin.methods;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.lang.java.JavaLanguage;
import com.intellij.openapi.module.Module;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiType;
import com.intellij.psi.augment.PsiAugmentProvider;
import com.intellij.psi.impl.light.LightMethodBuilder;
import com.intellij.psi.impl.light.LightTypeParameterBuilder;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import tk.labyrinth.apparatus.core.json.CoreObjectMapperFactory;
import tk.labyrinth.apparatus.core.method.MethodAdditionDescriptor;
import tk.labyrinth.apparatus.core.registry.ApparatusObjectRegistry;
import tk.labyrinth.apparatus.core.registry.PersistentObjectRegistry;
import tk.labyrinth.apparatus.ideaplugin.ConstructorAdditionPsiAugmentProvider;
import tk.labyrinth.apparatus.ideaplugin.contrib.FocusedModuleProvider;
import tk.labyrinth.apparatus.ideaplugin.tool.IdeaPluginIndexRegistry;
import tk.labyrinth.apparatus.index.IndexRegistry;
import tk.labyrinth.apparatus.misc4j.java.util.Lazy;
import tk.labyrinth.apparatus.misc4j.java.util.collectoin.ListUtils;
import tk.labyrinth.apparatus.misc4j.java.util.collectoin.StreamUtils;
import tk.labyrinth.apparatus.one.model.element.PsiMethodElementHandle;
import tk.labyrinth.apparatus.one.model.element.PsiTypeElementHandle;
import tk.labyrinth.apparatus.one.model.factory.DefaultPsiElementHandleFactory;
import tk.labyrinth.apparatus.one.model.psi.DefaultPsiElementFactory;
import tk.labyrinth.apparatus.one.model.psi.DefaultPsiTypeFactory;
import tk.labyrinth.apparatus.one.model.psi.PsiTypeFactory;
import tk.labyrinth.jaap.model.declaration.MethodDeclaration;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodAdditionPsiAugmentProvider extends PsiAugmentProvider {

	private final ConstructorAdditionPsiAugmentProvider constructorAdditionPsiAugmentProvider;

	private final ThreadLocal<Set<Pair<PsiElement, ? extends Class<?>>>> lookupsInProgress = ThreadLocal.withInitial(HashSet::new);

	private final ObjectMapper objectMapper = CoreObjectMapperFactory.create();

	{
		constructorAdditionPsiAugmentProvider = new ConstructorAdditionPsiAugmentProvider(objectMapper);
	}

	private PsiMethod createAdditionalMethod(Module module, MethodAdditionDescriptor methodAdditionDescriptor) {
		MethodDeclaration methodDeclaration = methodAdditionDescriptor.getDeclaration();
		//
		DefaultPsiElementFactory psiElementFactory = new DefaultPsiElementFactory(module);
		PsiTypeFactory psiTypeFactory = new DefaultPsiTypeFactory(module, psiElementFactory);
		//
		LightMethodBuilder methodBuilder = new LightMethodBuilder(
				PsiManager.getInstance(module.getProject()),
				JavaLanguage.INSTANCE,
				methodDeclaration.getName());
		//
		methodBuilder.setContainingClass(psiElementFactory.getType(methodAdditionDescriptor.getParent()));
		//
		methodDeclaration.getModifiers().forEach(modifier ->
				methodBuilder.addModifier(modifier.toString().toLowerCase()));
		//
		{
			List<Runnable> deferredTypeParameterBoundsResolvers = new ArrayList<>();
			StreamUtils.indexed(methodDeclaration.getTypeParameters().stream()).forEach(pair -> {
				LightTypeParameterBuilder lightTypeParameterBuilder = new LightTypeParameterBuilder(
						pair.getRight().getName(),
						methodBuilder,
						pair.getLeft());
				{
					List<TypeDescription> boundTypes = pair.getRight().getBoundTypes();
					if (boundTypes != null) {
						deferredTypeParameterBoundsResolvers.add(() -> boundTypes.forEach(boundType -> {
							lightTypeParameterBuilder.getExtendsList()
									.addReference(psiTypeFactory.findClass(boundType));
						}));
					}
				}
				methodBuilder.addTypeParameter(lightTypeParameterBuilder);
			});
			//
			// We need to apply iterative resolution for cases like this:
			// public <T,R extends T> void qwe(T t,R r)
			//
			MethodFullSignature methodFullSignature = methodDeclaration.getSignature().toFull(
					methodAdditionDescriptor.getParent().toString());
			psiElementFactory.getMethodsUnderCreation().put(methodFullSignature, methodBuilder);
			try {
				deferredTypeParameterBoundsResolvers.forEach(Runnable::run);
				//
				methodDeclaration.getFormalParameters().forEach(formalParameter ->
						methodBuilder.addParameter(
								formalParameter.getName(),
								// FIXME: Must discard method if not found.
								psiTypeFactory.find(formalParameter.getType())));
				{
					PsiType returnType = psiTypeFactory.find(methodDeclaration.getReturnType());
					if (returnType != null) {
						methodBuilder.setMethodReturnType(returnType);
					} else {
						// FIXME: Report method not created because return type was not resolved.
					}
				}
			} finally {
				psiElementFactory.getMethodsUnderCreation().remove(methodFullSignature);
			}
		}
		{
			methodBuilder.setNavigationElement(MethodAdditionPsiAugmentProviderUtils.getElement(
					psiElementFactory,
					methodAdditionDescriptor.getReference()));
		}
		return methodBuilder;
	}

	private List<PsiMethod> getExtraAugments(Module module, PsiClass element) {
//		LightMethod
//		throw new NotImplementedException();
		return List.of();
	}

	@NotNull
	@Override
	@SuppressWarnings("unchecked")
	protected <Psi extends PsiElement> List<Psi> getAugments(@NotNull PsiElement element, @NotNull Class<Psi> type) {
		List<?> result;
		//
		// Dirty solution to avoid recursions.
		// TODO: Add check that we don't have upstream dependencies (elements depending on those already in the list).
		val lookupKey = Pair.of(element, type);
		if (lookupsInProgress.get().add(lookupKey)) {
			try {
				if (type == PsiField.class) {
					result = getFieldAugments((PsiClass) element);
				} else if (type == PsiMethod.class) {
					PsiClass psiClass = (PsiClass) element;
					result = ListUtils.flatten(
							constructorAdditionPsiAugmentProvider.getConstructorAugments(psiClass),
							getMethodAugments(psiClass));
				} else {
					result = List.of();
				}
			} finally {
				lookupsInProgress.get().remove(lookupKey);
			}
		} else {
			result = List.of();
		}
		return (List<Psi>) result;
	}

	@NotNull
	protected List<PsiField> getFieldAugments(@NotNull PsiClass element) {
		return List.of();
	}

	@NotNull
	protected List<PsiMethod> getMethodAugments(@NotNull PsiClass element) {
		List<PsiMethod> result;
		{
			Module module = FocusedModuleProvider.findFocusedModule();
			if (module != null) {
				{
					// FIXME: STUDY
					DefaultPsiElementFactory psiElementFactory = new DefaultPsiElementFactory(module);
					DefaultPsiElementHandleFactory elementHandleFactory = new DefaultPsiElementHandleFactory(psiElementFactory);
					PsiTypeElementHandle streamType = elementHandleFactory.getType(Stream.class.getCanonicalName());
					PsiClass streamPsiClass = streamType.getPsiClass();
					PsiMethodElementHandle findAllMethod = streamType.getMethodByName("findAny");
					PsiMethod findAllPsiMethod = findAllMethod.getPsiMethod();
					//
					PsiMethod filterMap2 = elementHandleFactory.getType("extensionmethods.StreamExtensions.MyStream")
							.getMethodByName("filterMap2")
							.getPsiMethod();
					//
					System.out.println();
				}
				IndexRegistry indexRegistry = new IdeaPluginIndexRegistry(module, objectMapper);
				ApparatusObjectRegistry<MethodAdditionDescriptor> methodAdditionDescriptorRegistry =
						new PersistentObjectRegistry<>(indexRegistry, objectMapper, MethodAdditionDescriptor.class);
				//
				Lazy<CanonicalTypeSignature> elementSignature = Lazy.of(() ->
						CanonicalTypeSignature.ofValid(element.getQualifiedName()));
				result = methodAdditionDescriptorRegistry.get()
						.filter(methodAdditionDescriptor -> Objects.equals(
								methodAdditionDescriptor.getParent(),
								elementSignature.get()))
						.map(methodAdditionDescriptor -> createAdditionalMethod(module, methodAdditionDescriptor))
						.collect(Collectors.toList());
				result = ListUtils.flatten(result, getExtraAugments(module, element));
			} else {
				result = List.of();
			}
		}
		return result;
	}

	private static String adjustFormalParameterType(TypeDescription formalParameterType) {
		return formalParameterType.toString();
	}
}
