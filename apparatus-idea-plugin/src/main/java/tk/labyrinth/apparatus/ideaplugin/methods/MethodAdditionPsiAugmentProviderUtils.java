package tk.labyrinth.apparatus.ideaplugin.methods;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiSubstitutor;
import com.intellij.psi.PsiType;
import com.intellij.psi.impl.source.PsiExtensibleClass;
import com.intellij.psi.util.MethodSignature;
import tk.labyrinth.apparatus.one.model.psi.PsiElementFactory;
import tk.labyrinth.jaap.model.ElementSignature;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodAdditionPsiAugmentProviderUtils {

	private static String getErasureString(PsiType psiType) {
		String result;
		if (psiType instanceof PsiClassType) {
			PsiClassType psiClassReferenceType = (PsiClassType) psiType;
			result = psiClassReferenceType.rawType().getCanonicalText();
		} else {
			result = psiType.getCanonicalText();
		}
		return result;
	}

	public static PsiElement getElement(PsiElementFactory psiElementFactory, ElementSignature elementSignature) {
		// FIXME: Make this solution more robust. Solve issue with recursive method discovery.
		// FIXME: Looks like we just neet to put it into element factory.
		//
		String referenceString = elementSignature.toString();
		String referenceParentString = referenceString.substring(0, referenceString.indexOf("#"));
		String referenceMethodString = referenceString.substring(referenceString.indexOf("#") + 1);
		PsiClass reference = psiElementFactory.getType(referenceParentString);
		PsiExtensibleClass extensibleReference = (PsiExtensibleClass) reference;
		PsiMethod referenceMethod = extensibleReference.getOwnMethods().stream().filter(method -> {
			MethodSignature methodSignature = method.getSignature(PsiSubstitutor.EMPTY);
			String methodSignatureString = methodSignature.getName() + "(" +
					Stream.of(methodSignature.getParameterTypes())
							.map(MethodAdditionPsiAugmentProviderUtils::getErasureString)
							.collect(Collectors.joining(",")) + ")";
			return Objects.equals(methodSignatureString, referenceMethodString);
		}).findAny().orElse(null);
		if (referenceMethod == null) {
			throw new IllegalArgumentException("Not found: " + elementSignature);
		}
		return referenceMethod;
	}
}
