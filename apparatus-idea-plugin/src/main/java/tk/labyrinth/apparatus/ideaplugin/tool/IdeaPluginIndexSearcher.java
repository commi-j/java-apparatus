package tk.labyrinth.apparatus.ideaplugin.tool;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.vfs.VirtualFile;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.apparatus.core.tool.IndexSearcher;
import tk.labyrinth.apparatus.core.util.ResourceUtils;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.apparatus.index.OldIndexEntry;

import javax.annotation.Nullable;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class IdeaPluginIndexSearcher implements IndexSearcher {

	private final Module module;

	private final ObjectMapper objectMapper;

	@Nullable
	private OldIndexEntry parseLine(URL url, String line) {
		OldIndexEntry result;
		try {
			result = objectMapper.readValue(line, NewIndexEntry.class).toOldIndexEntry(url);
		} catch (UnrecognizedPropertyException ex) {
			// TODO: Report somehow.
			result = null;
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
		return result;
	}

	private Stream<OldIndexEntry> readIndexEntries(VirtualFile virtualFile) {
		URL url;
		try {
			// FIXME: Replace with IoUtils.unchecked.
			url = new URL(virtualFile.getUrl());
		} catch (MalformedURLException ex) {
			throw new RuntimeException(ex);
		}
		return ResourceUtils.readLines(virtualFile::getInputStream).stream()
				.map(line -> parseLine(url, line))
				.filter(Objects::nonNull);
	}

	@Override
	public Stream<OldIndexEntry> search(String key) {
		// TODO: Filter only resource roots (src..resources) (right now it also returns java and probably other languages).
		//
		String relativePath = IndexSearcher.buildPath(key);
		List<Pair<VirtualFile, VirtualFile>> rootAndFilePairs = ModuleDependencyService.getClassRoots(module)
				//
				// null when class root folder does not exist (not created or was deleted).
				.filter(Objects::nonNull)
				.map(orderEntryRoot -> Pair.of(orderEntryRoot, orderEntryRoot.findFileByRelativePath(relativePath)))
				.filter(pair -> pair.getValue() != null)
				.collect(Collectors.toList());
		return rootAndFilePairs.stream()
				.map(Pair::getValue)
				.distinct()
				.flatMap(this::readIndexEntries);
	}
}
