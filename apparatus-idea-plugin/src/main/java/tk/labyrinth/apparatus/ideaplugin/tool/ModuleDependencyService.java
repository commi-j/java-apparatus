package tk.labyrinth.apparatus.ideaplugin.tool;

import com.intellij.openapi.compiler.CompilerPaths;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.roots.LibraryOrSdkOrderEntry;
import com.intellij.openapi.roots.ModuleOrderEntry;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.roots.ModuleSourceOrderEntry;
import com.intellij.openapi.roots.OrderEntry;
import com.intellij.openapi.roots.OrderRootType;
import com.intellij.openapi.vfs.VirtualFile;
import tk.labyrinth.misc4j2.exception.ExceptionUtils;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import java.util.Objects;
import java.util.stream.Stream;

public class ModuleDependencyService {

	private static VirtualFile getModuleCompilerDestination(Module module) {
		return CompilerPaths.getModuleOutputDirectory(module, false);
	}

	private static ModuleRootManager getModuleRootManager(ModuleOrderEntry moduleOrderEntry) {
		return ModuleRootManager.getInstance(Objects.requireNonNull(moduleOrderEntry.getModule(),
				moduleOrderEntry.toString()));
	}

	/**
	 * Returns:<br>
	 * - Roots of all libraries;<br>
	 * - Roots of dependency modules;<br>
	 * - Compiler path of current module;<br>
	 *
	 * @param module non-null
	 *
	 * @return non-null
	 */
	public static Stream<VirtualFile> getClassRoots(Module module) {
		return getOrderEntries(module)
				.flatMap(orderEntry -> {
					Stream<VirtualFile> result;
					if (orderEntry instanceof LibraryOrSdkOrderEntry) {
						// Library dependencies & JDK.
						//
						result = Stream.of(orderEntry.getFiles(OrderRootType.CLASSES));
					} else if (orderEntry instanceof ModuleOrderEntry) {
						// Dependency modules.
						//
						result = Stream.of(getModuleCompilerDestination(((ModuleOrderEntry) orderEntry).getModule()));
					} else if (orderEntry instanceof ModuleSourceOrderEntry) {
						// Current module.
						//
						result = Stream.of(getModuleCompilerDestination(orderEntry.getOwnerModule()));
					} else {
						throw new NotImplementedException(ExceptionUtils.render(orderEntry));
					}
					return result;
				});
	}

	public static Stream<OrderEntry> getOrderEntries(Module module) {
		return Stream.of(ModuleRootManager.getInstance(module).getOrderEntries());
	}
}
