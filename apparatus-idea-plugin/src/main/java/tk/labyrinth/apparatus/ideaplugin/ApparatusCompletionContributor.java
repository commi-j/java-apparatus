package tk.labyrinth.apparatus.ideaplugin;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.completion.CompletionType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

// FIXME: Experimental
public class ApparatusCompletionContributor extends CompletionContributor {

	{
		extend(CompletionType.BASIC, PlatformPatterns.psiElement(), new CompletionProvider<>() {
			@Override
			protected void addCompletions(@NotNull CompletionParameters parameters, @NotNull ProcessingContext context, @NotNull CompletionResultSet result) {
				Module module = ModuleUtil.findModuleForFile(parameters.getOriginalFile());
			}
		});
	}
}
