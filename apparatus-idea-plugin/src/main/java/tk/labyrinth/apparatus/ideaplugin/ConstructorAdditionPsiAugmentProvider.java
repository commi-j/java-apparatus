package tk.labyrinth.apparatus.ideaplugin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import tk.labyrinth.apparatus.core.constructor.ConstructorAdditionDescriptor;
import tk.labyrinth.apparatus.core.registry.ApparatusObjectRegistry;
import tk.labyrinth.apparatus.core.registry.PersistentObjectRegistry;
import tk.labyrinth.apparatus.ideaplugin.model.ApparatusPsiConstructorBuilder;
import tk.labyrinth.apparatus.one.model.psi.DefaultPsiElementFactory;
import tk.labyrinth.apparatus.one.model.psi.PsiElementFactory;
import tk.labyrinth.apparatus.ideaplugin.tool.IdeaPluginIndexRegistry;
import tk.labyrinth.apparatus.index.IndexRegistry;
import tk.labyrinth.apparatus.misc4j.java.util.Lazy;
import tk.labyrinth.jaap.model.signature.CanonicalTypeSignature;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ConstructorAdditionPsiAugmentProvider {

	private final ObjectMapper objectMapper;

	@NotNull
	public List<PsiMethod> getConstructorAugments(@NotNull PsiClass element) {
		List<PsiMethod> result;
		{
			Module module = ModuleUtil.findModuleForPsiElement(element);
			if (module != null) {
				IndexRegistry indexRegistry = new IdeaPluginIndexRegistry(module, objectMapper);
				ApparatusObjectRegistry<ConstructorAdditionDescriptor> constructorAdditionDescriptorRegistry =
						new PersistentObjectRegistry<>(indexRegistry, objectMapper, ConstructorAdditionDescriptor.class);
				//
				Lazy<CanonicalTypeSignature> elementSignature = Lazy.of(() ->
						CanonicalTypeSignature.ofValid(element.getQualifiedName()));
				result = constructorAdditionDescriptorRegistry.get()
						.filter(constructorAdditionDescriptor -> Objects.equals(
								constructorAdditionDescriptor.getParent(),
								elementSignature.get()))
						.map(constructorAdditionDescriptor -> createConstructor(module, constructorAdditionDescriptor))
						.collect(Collectors.toList());
			} else {
				result = List.of();
			}
		}
		return result;
	}

	private static PsiMethod createConstructor(Module module, ConstructorAdditionDescriptor constructorAdditionDescriptor) {
		PsiElementFactory psiElementFactory = new DefaultPsiElementFactory(module);
		//
		ApparatusPsiConstructorBuilder constructorBuilder = new ApparatusPsiConstructorBuilder(
				psiElementFactory.getType(constructorAdditionDescriptor.getParent()));
		{
			constructorAdditionDescriptor.getDeclaration().getModifiers().forEach(modifier ->
					constructorBuilder.addModifier(modifier.toString().toLowerCase()));
		}
		if (constructorAdditionDescriptor.getBody() != null) {
			constructorBuilder.setBody(JavaPsiFacade.getElementFactory(constructorBuilder.getProject()).createCodeBlockFromText(
					"{" + String.join("", constructorAdditionDescriptor.getBody()) + "}",
					constructorBuilder));
		}
		{
			constructorBuilder.setMethodReturnType(constructorAdditionDescriptor.getParent().toString());
		}
		{
			// FIXME: Make this solution support any reference and not only annotations on types.
			//
			String referenceSignature = constructorAdditionDescriptor.getReference();
			String referenceParentSignature = referenceSignature.substring(0, referenceSignature.indexOf("@"));
			String referenceFullName = referenceSignature.substring(referenceSignature.indexOf("@") + 1);
			PsiClass parentType = psiElementFactory.getType(referenceParentSignature);
			PsiAnnotation reference = parentType.getAnnotation(referenceFullName);
			Objects.requireNonNull(reference, "reference");
			constructorBuilder.setNavigationElement(reference);
		}
		{
			constructorAdditionDescriptor.getDeclaration().getFormalParameters().forEach(formalParameter ->
					constructorBuilder.addParameter(formalParameter.getName(), formalParameter.getType().toString()));
		}
		return constructorBuilder;
	}
}
