package tk.labyrinth.apparatus.ideaplugin.model;

import com.intellij.lang.java.JavaLanguage;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiCodeBlock;
import com.intellij.psi.impl.light.LightMethodBuilder;
import lombok.Getter;
import lombok.Setter;

/**
 * This one allows to define a body.
 */
public class ApparatusPsiConstructorBuilder extends LightMethodBuilder {

	@Getter
	@Setter
	private PsiCodeBlock body = null;

	public ApparatusPsiConstructorBuilder(PsiClass constructedClass) {
		super(constructedClass, JavaLanguage.INSTANCE);
		setConstructor(true);
	}
}
