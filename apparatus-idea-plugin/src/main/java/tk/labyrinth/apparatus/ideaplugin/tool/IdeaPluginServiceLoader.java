package tk.labyrinth.apparatus.ideaplugin.tool;

import com.intellij.openapi.module.Module;
import lombok.Value;
import tk.labyrinth.apparatus.core.tool.ApparatusServiceLoader;
import tk.labyrinth.apparatus.core.util.ResourceUtils;
import tk.labyrinth.apparatus.misc4j.java.lang.reflect.ReflectionUtils;

import java.util.Objects;
import java.util.stream.Stream;

@Value
public class IdeaPluginServiceLoader implements ApparatusServiceLoader {

	public static final String SERVICES_PATH = "META-INF/services";

	Module module;

	@Override
	public <T> Stream<T> load(Class<T> type) {
		return ModuleDependencyService.getClassRoots(module)
				.map(orderEntryRoot -> orderEntryRoot.findFileByRelativePath(SERVICES_PATH + "/" + type.getCanonicalName()))
				.filter(Objects::nonNull)
				.flatMap(file -> ResourceUtils.readLines(file::getInputStream).stream())
				.map(ReflectionUtils::createNewInstance);
	}
}
