package tk.labyrinth.apparatus.ideaplugin.contrib;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiFile;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class FocusedModuleProvider {

	@Nullable
	public static Module findFocusedModule() {
		Module result;
		{
			Editor editor = EditorFocusService.getInstance().getFocusedEditor();
			if (editor != null) {
				Project project = editor.getProject();
				if (project != null) {
					PsiFile file = PsiDocumentManager.getInstance(editor.getProject())
							.getPsiFile(editor.getDocument());
					result = ModuleUtil.findModuleForFile(file);
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}
}
