package tk.labyrinth.apparatus.ideaplugin.tool;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.openapi.module.Module;
import tk.labyrinth.apparatus.core.tool.IndexSearcher;
import tk.labyrinth.apparatus.index.IndexRegistry;
import tk.labyrinth.apparatus.index.NewIndexEntry;
import tk.labyrinth.apparatus.index.OldIndexEntry;
import tk.labyrinth.misc4j2.exception.NotImplementedException;

import java.util.stream.Stream;

public class IdeaPluginIndexRegistry implements IndexRegistry {

	private final IndexSearcher indexSearcher;

	private final Module module;

	public IdeaPluginIndexRegistry(Module module, ObjectMapper objectMapper) {
		this.module = module;
		indexSearcher = new IdeaPluginIndexSearcher(module, objectMapper);
	}

	@Override
	public void add(NewIndexEntry newIndexEntry) {
		throw new NotImplementedException();
	}

	@Override
	public void flush() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Stream<OldIndexEntry> get(String key) {
		return indexSearcher.search(key);
	}
}
